-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 15, 2015 at 11:49 AM
-- Server version: 5.6.21
-- PHP Version: 5.5.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `tef`
--

-- --------------------------------------------------------

--
-- Table structure for table `content`
--

CREATE TABLE IF NOT EXISTS `content` (
`id` bigint(20) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Description of what you can change on this part',
  `father` bigint(20) NOT NULL,
  `deleted` tinyint(1) NOT NULL DEFAULT '0',
  `content_order` int(11) NOT NULL,
  `front_display` int(1) NOT NULL,
  `pending_approval` int(11) NOT NULL,
  `rejected` int(11) NOT NULL,
  `modify` varchar(10) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `content`
--

INSERT INTO `content` (`id`, `title`, `description`, `father`, `deleted`, `content_order`, `front_display`, `pending_approval`, `rejected`, `modify`) VALUES
(1, 'Social Media', '', -1, 0, 0, 1, 0, 0, ''),
(2, 'Home', '', -1, 0, 0, 1, 0, 0, ''),
(3, 'Projects', '', -1, 0, 0, 1, 0, 0, ''),
(4, 'Who', '', -1, 0, 0, 1, 0, 0, ''),
(5, 'People', '', 4, 0, 0, 1, 0, 0, ''),
(6, 'Firm Info', '', 4, 0, 0, 1, 0, 0, ''),
(7, 'Our Firm', '', 6, 0, 0, 1, 0, 0, ''),
(8, 'Services', '', 6, 0, 0, 1, 0, 0, ''),
(9, 'Client List', '', 6, 0, 0, 1, 0, 0, ''),
(10, 'Awards', '', 6, 0, 0, 1, 0, 0, ''),
(11, 'Press releases', '', 6, 0, 0, 1, 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `content_section`
--

CREATE TABLE IF NOT EXISTS `content_section` (
  `content_id` bigint(20) NOT NULL,
  `section_id` bigint(20) NOT NULL,
  `multiple` tinyint(1) NOT NULL,
  `image` tinyint(1) NOT NULL,
  `width` int(11) NOT NULL,
  `height` int(11) NOT NULL,
  `order` int(11) NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `autocomplete` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `content_section`
--

INSERT INTO `content_section` (`content_id`, `section_id`, `multiple`, `image`, `width`, `height`, `order`, `description`, `autocomplete`) VALUES
(1, 1, 1, 0, 0, 0, 0, 'Edit Social Media Icons.', 0),
(3, 2, 1, 0, 0, 0, 0, 'Edit the project Categories here', 0),
(3, 3, 1, 0, 0, 0, 1, 'Edit the project info here', 0),
(5, 4, 1, 0, 0, 0, 0, 'Edit the staff entries here.', 0),
(10, 5, 1, 0, 0, 0, 0, 'Edit the award list.', 0),
(11, 6, 1, 0, 0, 0, 0, 'Edit press releases.', 0);

-- --------------------------------------------------------

--
-- Table structure for table `data_row`
--

CREATE TABLE IF NOT EXISTS `data_row` (
  `id` bigint(20) unsigned NOT NULL,
  `content_id` bigint(20) NOT NULL,
  `section_id` bigint(20) NOT NULL,
  `order` int(11) NOT NULL,
  `status` int(11) NOT NULL COMMENT '1=delete,2=New, 3= update',
  `modify` datetime NOT NULL,
  `approve` int(11) NOT NULL COMMENT '1=saved,2=approved with changes,3=approved,4=decline and retain, 5=decline and revert'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `data_row`
--

INSERT INTO `data_row` (`id`, `content_id`, `section_id`, `order`, `status`, `modify`, `approve`) VALUES
(1, 1, 1, 0, 0, '0000-00-00 00:00:00', 0),
(1, 3, 2, 0, 0, '0000-00-00 00:00:00', 0),
(1, 3, 3, 0, 0, '0000-00-00 00:00:00', 0),
(1, 5, 4, 0, 0, '0000-00-00 00:00:00', 0),
(1, 10, 5, 0, 0, '0000-00-00 00:00:00', 0),
(1, 11, 6, 0, 0, '0000-00-00 00:00:00', 0),
(2, 1, 1, 0, 0, '0000-00-00 00:00:00', 0),
(2, 3, 2, 0, 0, '0000-00-00 00:00:00', 0),
(2, 3, 3, 0, 0, '0000-00-00 00:00:00', 0),
(2, 5, 4, 0, 0, '0000-00-00 00:00:00', 0),
(2, 10, 5, 0, 0, '0000-00-00 00:00:00', 0),
(2, 11, 6, 0, 0, '0000-00-00 00:00:00', 0),
(3, 1, 1, 0, 0, '0000-00-00 00:00:00', 0),
(3, 3, 2, 0, 0, '0000-00-00 00:00:00', 0),
(3, 3, 3, 0, 0, '0000-00-00 00:00:00', 0),
(3, 10, 5, 0, 0, '0000-00-00 00:00:00', 0),
(3, 11, 6, 0, 0, '0000-00-00 00:00:00', 0),
(4, 3, 3, 0, 0, '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `fields`
--

CREATE TABLE IF NOT EXISTS `fields` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(75) COLLATE utf8_unicode_ci DEFAULT NULL,
  `display_text` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `content_id` bigint(20) unsigned NOT NULL,
  `type_id` int(10) unsigned NOT NULL,
  `order` int(11) NOT NULL,
  `select_id` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `section_id` bigint(20) NOT NULL,
  `order_by` int(1) NOT NULL,
  `max_chars` bigint(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fields`
--

INSERT INTO `fields` (`id`, `name`, `display_text`, `content_id`, `type_id`, `order`, `select_id`, `section_id`, `order_by`, `max_chars`) VALUES
(1, 'name', 'Name', 1, 1, 1, '0', 1, 1, 0),
(2, 'order', 'Order', 1, 27, 2, '0', 1, 0, 0),
(3, 'link', 'Link', 1, 1, 3, '0', 1, 0, 0),
(4, 'icon', 'Icon (13w x 13t pixels)', 1, 3, 4, '0', 1, 0, 0),
(5, 'category', 'Name', 3, 1, 1, '0', 2, 1, 0),
(6, 'c_order', 'Order', 3, 27, 2, '0', 2, 0, 0),
(7, 'p_name', 'Name', 3, 1, 1, '0', 3, 1, 0),
(8, 'p_order', 'Order', 3, 27, 2, '0', 3, 0, 0),
(9, 'p_size', 'Size', 3, 1, 3, '0', 3, 0, 0),
(10, 'p_location', 'Project Location', 3, 1, 4, '0', 3, 0, 0),
(11, '', 'Images', 3, 26, 7, '0', 3, 0, 0),
(12, 'pimg1', 'Image 1(1,474w x 990t pixels)', 3, 3, 8, '0', 3, 0, 0),
(13, 'p_cat', 'Category', 3, 22, 5, '3,2', 3, 0, 0),
(14, 'featured', 'Featured?', 3, 13, 6, '0', 3, 0, 0),
(15, 'name', 'Name', 5, 1, 1, '0', 4, 1, 0),
(16, 'order', 'Order', 5, 27, 2, '0', 4, 0, 0),
(17, 'image1', 'Main Image (160w x 200t pixels)', 5, 3, 7, '0', 4, 0, 0),
(18, 'title', 'Title', 5, 1, 3, '0', 4, 0, 0),
(19, 'description', 'Description', 5, 4, 9, '0', 4, 0, 0),
(20, 'phone', 'Phone Number', 5, 1, 4, '0', 4, 0, 0),
(21, 'email', 'Email', 5, 1, 5, '0', 4, 0, 0),
(22, 'in_image', 'Inner Image (335w x 430t pixels)', 5, 3, 8, '0', 4, 0, 0),
(23, 'speech', 'Speech', 5, 4, 10, '0', 4, 0, 0),
(24, '', 'Projects', 5, 26, 11, '0', 4, 0, 0),
(25, 'project1', 'Select a Project or', 5, 15, 12, '3,3', 4, 0, 0),
(26, 'project2', 'Select a Project or', 5, 15, 13, '3,3', 4, 0, 0),
(27, 'project3', 'Select a Project or', 5, 15, 20, '3,3', 4, 0, 0),
(28, 'title1', 'Add a Title for external Project', 5, 1, 14, '0', 4, 0, 0),
(29, 'title2', 'Add a Title for external Project', 5, 1, 15, '0', 4, 0, 0),
(30, 'title3', 'Add a Title for external Project', 5, 1, 22, '0', 4, 0, 0),
(31, 'img1', 'Image (525w x 325t pixels)', 5, 3, 16, '0', 4, 0, 0),
(32, 'img2', 'Image (525w x 325t pixels)', 5, 3, 17, '0', 4, 0, 0),
(33, 'img3', 'Image (525w x 325t pixels)', 5, 3, 24, '0', 4, 0, 0),
(34, 'desc1', 'Project description', 5, 4, 18, '0', 4, 0, 0),
(35, 'desc2', 'Project description', 5, 4, 19, '0', 4, 0, 0),
(36, 'desc3', 'Project description', 5, 4, 26, '0', 4, 0, 0),
(37, 'project4', 'Select a Project or', 5, 15, 21, '3,3', 4, 0, 0),
(38, 'title4', 'Add a Title for external Project', 5, 1, 23, '0', 4, 0, 0),
(39, 'desc4', 'Project description', 5, 4, 27, '0', 4, 0, 0),
(40, 'img4', 'Image (525w x 325t pixels)', 5, 3, 25, '0', 4, 0, 0),
(41, 'leader', 'Leader?', 5, 13, 6, '0', 4, 0, 0),
(42, 'a_name', 'Award Name', 10, 1, 1, '0', 5, 1, 0),
(43, 'a_order', 'Order', 10, 27, 2, '0', 5, 0, 0),
(44, 'a_project', 'Project', 10, 15, 3, '3,3', 5, 0, 0),
(45, 'a_peep', 'Staff involved', 10, 22, 4, '5,4', 5, 0, 0),
(46, 'p_title', 'Press title', 11, 1, 1, '0', 6, 1, 0),
(47, 'p_order', 'Order', 11, 27, 2, '0', 6, 0, 0),
(48, 'p_project', 'Project', 11, 15, 3, '5,4', 6, 0, 0),
(49, 'p_staff', 'Staff involved', 11, 22, 4, '5,4', 6, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `field_data`
--

CREATE TABLE IF NOT EXISTS `field_data` (
`id` bigint(20) unsigned NOT NULL,
  `fields_id` int(10) unsigned NOT NULL,
  `content_live` text COLLATE utf8_unicode_ci,
  `content_draft` text COLLATE utf8_unicode_ci NOT NULL,
  `row_id` bigint(20) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=360 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `field_data`
--

INSERT INTO `field_data` (`id`, `fields_id`, `content_live`, `content_draft`, `row_id`) VALUES
(1, 1, 'Linked In', '', 1),
(2, 2, '1', '', 1),
(3, 3, 'http://linkedin.com', '', 1),
(4, 4, 'lin.png', '', 1),
(5, 1, 'Facebook', '', 2),
(6, 2, '3', '', 2),
(7, 3, 'facebook.com', '', 2),
(8, 4, 'fb.png', '', 2),
(9, 1, 'Twitter', '', 3),
(10, 2, '2', '', 3),
(11, 3, 'twitter.com', '', 3),
(12, 4, 'tw.png', '', 3),
(13, 5, 'Wellness', '', 1),
(14, 6, '1', '', 1),
(15, 5, 'Preservation', '', 2),
(16, 6, '2', '', 2),
(17, 5, 'Category 1', '', 3),
(18, 6, '3', '', 3),
(19, 7, 'Project Name Goes Here Orrovit a del eos doles', '', 1),
(20, 8, '1', '', 1),
(21, 9, '1000 sqft', '', 1),
(22, 10, 'San Francisco', '', 1),
(23, 13, '2', '', 1),
(24, 14, 'on', '', 1),
(25, 12, 'p1.png', '', 1),
(33, 7, 'Project name #3', '', 3),
(34, 8, '3', '', 3),
(35, 9, '1000 sqft', '', 3),
(36, 10, 'Vallejo', '', 3),
(37, 13, '3,1', '', 3),
(38, 14, 'on', '', 3),
(39, 12, 'p3.png', '', 3),
(47, 7, 'Project name 2', '', 2),
(48, 8, '2', '', 2),
(49, 9, '10,000 sqft', '', 2),
(50, 10, 'Oakland', '', 2),
(51, 13, '3,1', '', 2),
(52, 14, 'on', '', 2),
(53, 12, 'p2.png', '', 2),
(54, 7, 'Project name 4', '', 4),
(55, 8, '4', '', 4),
(56, 9, '50 sqft', '', 4),
(57, 10, 'San Mateo', '', 4),
(58, 13, '2,1', '', 4),
(59, 14, 'on', '', 4),
(60, 12, 'p4.png', '', 4),
(209, 15, 'NAME GOES HERE2', '', 2),
(210, 16, '1', '', 2),
(211, 18, 'Title Goes Here', '', 2),
(212, 20, '510.XXX.XXXX', '', 2),
(213, 21, 'name@TEFarch.com', '', 2),
(214, 41, '0', '', 2),
(215, 17, 'peep2.jpg', '', 2),
(216, 22, 'int2.jpg', '', 2),
(217, 19, 'Ibus aut re volupta sitiorrorepe dolenia turepero etur suntemp oriorion eatusae necero eic tota dolorunto blatiat emporum earchit fuga. Ilitaspere vellaut qui rem andam estia nus idem nempore periosti inveligeni aspel il incti consect iaspici issera voluptaspiet aut officipsum vendici aecuscient.<br /><br />Ro derum sunt et ad que eicipient. Pa soluptatibus dignatemquas nem que endundis deligen ducilig enditius quia dolo modi res et ipsam faccusapit experest, qui andaeptatiis sim eaqui antectaqui ut fugit, que quamet re estore doluptata volorem. Nequunt perum, omnimporpos dolores cipitib usdamusam dolorectia vernatesci dolo.<br /><br />Daectem sita duciissi non eium quunt es ditaepr eperum, corunte mquibus magnien isinvel ignatur sit volupienis destiumquis nihiliq uassinctatur acerspedi. ipsam faccusapit experest, qui andaeptatiis sim eaqui antectaqui ut fugw ipsam faccusapit experest, qui anantectaqui ut fu<br /><br />Ibus aut re volupta sitiorrorepe dolenia turepero etur suntemp oriorion eatusae necero eic tota dolorunto blatiat emporum earchit fuga. Ilitaspere vellaut qui rem andam estia nus idem nempore periosti inveligeni aspel il incti consect iaspici issera voluptaspiet aut officipsum vendici aecuscient.<br /><br />Ro derum sunt et ad que eicipient. Pa soluptatibus dignatemquas nem que endundis deligen ducilig enditius quia dolo modi res et ipsam faccusapit experest, qui andaeptatiis sim eaqui antectaqui ut fugit, que quamet re estore doluptata volorem. Nequunt perum, omnimporpos dolores cipitib usdamusam dolorectia vernatesci dolo.<br /><br />Daectem sita duciissi non eium quunt es ditaepr eperum, corunte mquibus magnien isinvel ignatur sit volupienis destiumquis nihiliq uassinctatur acerspedi. ipsam faccusapit experest, qui andaeptatiis sim eaqui antectaqui ut fugw ipsam faccusapit experest, qui anantectaqui ut fu', '', 2),
(218, 23, 'My tablet is always attached to me. is mnimole seceris dolupta.et harupta dolupta serepud igenime ipsusanis nempore essitati sundae por moloratquiam quod quo blam resto bea aut ut expe sus volum ad que offica.', '', 2),
(219, 25, '-1', '', 2),
(220, 26, '3', '', 2),
(221, 28, 'Project title Goes Here', '', 2),
(222, 29, '', '', 2),
(223, 31, 'proj.jpg', '', 2),
(224, 32, '', '', 2),
(225, 34, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc eu mi tortor. Praesent sit amet mauris a nibh eleifend porttitor. Nulla consectetur, metus id euismod eleifend, nisi felis pretium eros, nec semper justo metus eget nisi. Integer efficitur sit amet nunc et placerat. Praesent ornare tincidunt nunc, non varius urna mollis ac. Aliquam ultrices justo et diam commodo porta. Integer nec pulvinar nunc.', '', 2),
(226, 35, '', '', 2),
(227, 27, '4', '', 2),
(228, 37, '2', '', 2),
(229, 30, '', '', 2),
(230, 38, '', '', 2),
(231, 33, '', '', 2),
(232, 40, '', '', 2),
(233, 36, '', '', 2),
(234, 39, '', '', 2),
(244, 42, 'Award name 3', '', 3),
(245, 43, '3', '', 3),
(246, 44, '1', '', 3),
(247, 45, '1', '', 3),
(248, 42, 'Award Name Goes Here', '', 1),
(249, 43, '1', '', 1),
(250, 44, '3', '', 1),
(251, 45, '2', '', 1),
(252, 42, 'Award Name Goes Here ia nonsereperum ute di ut erate esciissuntor si con et qui do Officimet ut laccum que sit, qui quidem fugia corepta turestias ', '', 2),
(253, 43, '2', '', 2),
(254, 44, '4', '', 2),
(255, 45, '1,2', '', 2),
(294, 15, 'NAME GOES HERE', '', 1),
(295, 16, '1', '', 1),
(296, 18, 'Title Goes Here', '', 1),
(297, 20, '510.XXX.XXXX', '', 1),
(298, 21, 'name@TEFarch.com', '', 1),
(299, 41, 'on', '', 1),
(300, 17, 'peep1.jpg', '', 1),
(301, 22, 'int1.jpg', '', 1),
(302, 19, 'Ibus aut re volupta sitiorrorepe dolenia turepero etur suntemp oriorion eatusae necero eic tota dolorunto blatiat emporum earchit fuga. Ilitaspere vellaut qui rem andam estia nus idem nempore periosti inveligeni aspel il incti consect iaspici issera voluptaspiet aut officipsum vendici aecuscient.<br /><br />Ro derum sunt et ad que eicipient. Pa soluptatibus dignatemquas nem que endundis deligen ducilig enditius quia dolo modi res et ipsam faccusapit experest, qui andaeptatiis sim eaqui antectaqui ut fugit, que quamet re estore doluptata volorem. Nequunt perum, omnimporpos dolores cipitib usdamusam dolorectia vernatesci dolo.<br /><br />Daectem sita duciissi non eium quunt es ditaepr eperum, corunte mquibus magnien isinvel ignatur sit volupienis destiumquis nihiliq uassinctatur acerspedi. ipsam faccusapit experest, qui andaeptatiis sim eaqui antectaqui ut fugw ipsam faccusapit experest, qui anantectaqui ut fu<br /><br />Ibus aut re volupta sitiorrorepe dolenia turepero etur suntemp oriorion eatusae necero eic tota dolorunto blatiat emporum earchit fuga. Ilitaspere vellaut qui rem andam estia nus idem nempore periosti inveligeni aspel il incti consect iaspici issera voluptaspiet aut officipsum vendici aecuscient.<br /><br />Ro derum sunt et ad que eicipient. Pa soluptatibus dignatemquas nem que endundis deligen ducilig enditius quia dolo modi res et ipsam faccusapit experest, qui andaeptatiis sim eaqui antectaqui ut fugit, que quamet re estore doluptata volorem. Nequunt perum, omnimporpos dolores cipitib usdamusam dolorectia vernatesci dolo.<br /><br />Daectem sita duciissi non eium quunt es ditaepr eperum, corunte mquibus magnien isinvel ignatur sit volupienis destiumquis nihiliq uassinctatur acerspedi. ipsam faccusapit experest, qui andaeptatiis sim eaqui antectaqui ut fugw ipsam faccusapit experest, qui anantectaqui ut fu', '', 1),
(303, 23, 'My tablet is always attached to me. is mnimole seceris dolupta.et harupta dolupta serepud igenime ipsusanis nempore essitati sundae por moloratquiam quod quo blam resto bea aut ut expe sus volum ad que offica.', '', 1),
(304, 25, '3', '', 1),
(305, 26, '-1', '', 1),
(306, 28, '', '', 1),
(307, 29, 'Project title Goes Here', '', 1),
(308, 31, '', '', 1),
(309, 32, 'p3.png', '', 1),
(310, 34, '', '', 1),
(311, 35, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras molestie et nulla malesuada elementum. Pellentesque et facilisis risus. Quisque sagittis libero id posuere imperdiet. Pellentesque non porta ex. Morbi molestie euismod est vitae sollicitudin. Sed ornare massa quis tincidunt dignissim. Sed ut orci ut purus luctus gravida sed quis turpis. Vestibulum id porta magna. In nisi tellus, porttitor vitae ex ut, finibus placerat neque.<br /><br />Etiam egestas ipsum neque, ut egestas magna sodales a. Ut a lorem varius, efficitur ante non, luctus magna. Phasellus mattis interdum tincidunt. Etiam finibus purus lectus, at scelerisque urna faucibus eu. Aenean vehicula facilisis leo, quis egestas sem sollicitudin a. Sed tempor lacus sit amet est imperdiet mollis. In hac habitasse platea dictumst.<br /><br />Suspendisse pharetra leo ac vehicula aliquam. Nunc fermentum malesuada ligula ac dictum. Suspendisse a pharetra nisi. Nunc ut orci dictum, bibendum risus a, consectetur justo. Vivamus euismod dui at diam luctus, non pretium tortor semper. Nunc vel felis felis. Proin eu aliquam risus. Vestibulum tempus vitae quam at tempus. Ut non accumsan mauris, eget suscipit nisi. Donec efficitur vulputate facilisis. Aliquam erat volutpat. Vestibulum ut ipsum in libero mollis convallis. Duis eget nisi vitae tellus vestibulum tincidunt.', '', 1),
(312, 27, '4', '', 1),
(313, 37, '1', '', 1),
(314, 30, '', '', 1),
(315, 38, '', '', 1),
(316, 33, '', '', 1),
(317, 40, '', '', 1),
(318, 36, '', '', 1),
(319, 39, '', '', 1),
(348, 46, 'Press entry number one', '', 1),
(349, 47, '1', '', 1),
(350, 48, '1', '', 1),
(351, 49, '1', '', 1),
(352, 46, 'Press title here Press title here Press title here', '', 2),
(353, 47, '2', '', 2),
(354, 48, '2', '', 2),
(355, 49, '1,2', '', 2),
(356, 46, 'voleste mpossimi, sam fugiandebis expla qui rehenim volupti tem exceatu reptibeaqui repudi dolupta pore d', '', 3),
(357, 47, '3', '', 3),
(358, 48, '1', '', 3),
(359, 49, '1,2', '', 3);

-- --------------------------------------------------------

--
-- Table structure for table `field_section`
--

CREATE TABLE IF NOT EXISTS `field_section` (
`id` bigint(20) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `order` int(11) NOT NULL,
  `icon` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='How we divide the inputs visualy on the cms';

--
-- Dumping data for table `field_section`
--

INSERT INTO `field_section` (`id`, `name`, `description`, `order`, `icon`) VALUES
(1, 'Social Media Icons', 'Social Media', 1, 11),
(2, 'Project Categories', '', 0, 16),
(3, 'Projects', '', 0, 15),
(4, 'People', '', 0, 13),
(5, 'Award list', '', 0, 4),
(6, 'Press Releases', '', 0, 6);

-- --------------------------------------------------------

--
-- Table structure for table `field_type`
--

CREATE TABLE IF NOT EXISTS `field_type` (
`id` int(10) unsigned NOT NULL,
  `type` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `field_type`
--

INSERT INTO `field_type` (`id`, `type`) VALUES
(1, 'input_small'),
(2, 'textarea_small'),
(3, 'file_small'),
(4, 'text_editor_small'),
(5, 'input_big'),
(6, 'textarea_big'),
(7, 'file_big'),
(8, 'text_editor_big'),
(9, 'input_small_big'),
(10, 'textarea_small_big'),
(11, 'file_small_big'),
(12, 'text_editor_small_big'),
(13, 'switch_small'),
(14, 'switch_small_big'),
(15, 'select_small'),
(16, 'select_big'),
(17, 'select_small_big'),
(20, 'date_small'),
(21, 'date_small_big'),
(22, 'select_multiple_small'),
(23, 'select_multiple_small_big'),
(24, 'time_small'),
(25, 'time_small_big'),
(26, 'separator'),
(27, 'order_'),
(28, 'files');

-- --------------------------------------------------------

--
-- Table structure for table `file`
--

CREATE TABLE IF NOT EXISTS `file` (
`id` bigint(20) unsigned NOT NULL,
  `content_id` bigint(20) NOT NULL,
  `section_id` bigint(20) NOT NULL,
  `field_id` bigint(20) NOT NULL,
  `row_id` bigint(20) NOT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `caption` text COLLATE utf8_unicode_ci NOT NULL,
  `caption_d` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image_order` int(11) NOT NULL,
  `image_order_d` int(11) NOT NULL,
  `status` int(11) NOT NULL COMMENT '0=live,1=delete draft,2=new draft'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `image`
--

CREATE TABLE IF NOT EXISTS `image` (
`id` bigint(20) unsigned NOT NULL,
  `content_id` bigint(20) NOT NULL,
  `section_id` bigint(20) NOT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `caption` text COLLATE utf8_unicode_ci NOT NULL,
  `image_order` int(11) NOT NULL,
  `status` int(11) NOT NULL COMMENT '0=live,1=delete draft,2=new draft'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `log`
--

CREATE TABLE IF NOT EXISTS `log` (
  `date` datetime NOT NULL,
  `user` bigint(20) NOT NULL,
  `operation` int(11) NOT NULL,
  `reason` text COLLATE utf8_unicode_ci NOT NULL,
  `content_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `meta_data`
--

CREATE TABLE IF NOT EXISTS `meta_data` (
  `content_id` bigint(20) NOT NULL,
  `meta_title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_keywords` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `section_icon`
--

CREATE TABLE IF NOT EXISTS `section_icon` (
`id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `file` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `section_icon`
--

INSERT INTO `section_icon` (`id`, `name`, `file`) VALUES
(2, 'Images', 'images.png'),
(3, 'Quicklinks', 'quicklinks.png'),
(4, 'Typewriter', 'typewriter.png'),
(5, 'Side Bar', 'sidebar.png'),
(6, 'News', 'news.png'),
(7, 'Calendar', 'calendar.png'),
(8, 'Map', 'map.png'),
(9, 'Phone', 'phone.png'),
(10, 'microphone', 'microphone.png'),
(11, 'Social Media', 'social_media.png'),
(12, 'Job Posting', 'job_posting.png'),
(13, 'Bios and Background', 'bios_and_background.png'),
(14, 'FAQs', 'question.png'),
(15, 'Projects', 'projects.png'),
(16, 'Categories', 'categories.png'),
(17, 'Alert', 'alert.png'),
(18, 'Download', 'download.png'),
(19, 'Lock', 'lock.png'),
(20, 'car', 'car.png');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
`id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `who` bigint(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `user`, `password`, `type`, `email`, `created`, `modified`, `who`) VALUES
(1, '', 'test', 'D4ZNNoqGclNCUcdebnFSRe14Z4VUFXz/IwyD06JNKwzL5fvJupLARF+RcwKyCs6UNfZm3+OJJoP+oCT2YkrUKg==', 1, 'approver@approver.com', '2014-04-28 09:17:10', '2014-05-01 12:26:32', 1),
(2, '', 'admin', 'Tl4ckpgYMtzgu8d/3VxuR8qFPKYF8QRfpl/GgJ2b3/AbOf1bH6OK7AzNeEsPvPOBpH//GAem8OeEnnneMooNcg==', 2, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_type`
--

CREATE TABLE IF NOT EXISTS `user_type` (
`id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_type`
--

INSERT INTO `user_type` (`id`, `name`) VALUES
(3, 'Editor'),
(4, 'Approver');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `content`
--
ALTER TABLE `content`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `content_section`
--
ALTER TABLE `content_section`
 ADD PRIMARY KEY (`content_id`,`section_id`);

--
-- Indexes for table `data_row`
--
ALTER TABLE `data_row`
 ADD PRIMARY KEY (`id`,`content_id`,`section_id`), ADD KEY `content_id` (`content_id`), ADD KEY `section_id` (`section_id`), ADD KEY `fields_id_2` (`content_id`,`section_id`);

--
-- Indexes for table `fields`
--
ALTER TABLE `fields`
 ADD PRIMARY KEY (`id`), ADD KEY `select` (`select_id`);

--
-- Indexes for table `field_data`
--
ALTER TABLE `field_data`
 ADD PRIMARY KEY (`id`), ADD KEY `fields_id` (`fields_id`,`row_id`), ADD KEY `fields_id_2` (`fields_id`), ADD KEY `content_id` (`row_id`);

--
-- Indexes for table `field_section`
--
ALTER TABLE `field_section`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `field_type`
--
ALTER TABLE `field_type`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `file`
--
ALTER TABLE `file`
 ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `image`
--
ALTER TABLE `image`
 ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `log`
--
ALTER TABLE `log`
 ADD PRIMARY KEY (`date`,`user`);

--
-- Indexes for table `meta_data`
--
ALTER TABLE `meta_data`
 ADD PRIMARY KEY (`content_id`);

--
-- Indexes for table `section_icon`
--
ALTER TABLE `section_icon`
 ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_type`
--
ALTER TABLE `user_type`
 ADD UNIQUE KEY `id` (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `content`
--
ALTER TABLE `content`
MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `fields`
--
ALTER TABLE `fields`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT for table `field_data`
--
ALTER TABLE `field_data`
MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=360;
--
-- AUTO_INCREMENT for table `field_section`
--
ALTER TABLE `field_section`
MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `field_type`
--
ALTER TABLE `field_type`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `file`
--
ALTER TABLE `file`
MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `image`
--
ALTER TABLE `image`
MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `section_icon`
--
ALTER TABLE `section_icon`
MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `user_type`
--
ALTER TABLE `user_type`
MODIFY `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
