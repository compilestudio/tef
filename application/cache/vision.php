<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Vision extends MY_Controller {

#**************************************************************************************************
	#Display the vision page
	public function index($error=0){
		$this->load->model("backend/Content_model","content");
	
		$data['info']= $this->content->get_data(2);
			
		$this->load_header_front(NULL);
			
		$this->load->view('vision',$data);
		$this->load_footer_front();
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */