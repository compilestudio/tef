<?php

	/*
	 * Fecha de Creación: 16-mar-2012
	 * Autor: Pedro Izaguirre
	 * Fecha Última Modificación: 
	 * Modificado por: 
	 * Descripción: Modelo con funciones para el Home
	 * 
	 */ 

class Home_model extends MY_Model{
	function __construct(){
        parent::__construct();
		$this->set_table("home");
    }
#********************************************************************************************************
	public function home_info(){
		$this->lang->load('home', $this->idioma);

		$array_end['entrada_btn'] = $this->lang->line('entrada_btn');
		$array_end['salida_alm_btn'] = $this->lang->line('salida_alm_btn');
		$array_end['regrese_alm_btn'] = $this->lang->line('regrese_alm_btn');
		$array_end['salida_btn'] = $this->lang->line('salida_btn');
		$array_end['proyecto_title'] = $this->lang->line('proyecto_title');
		$array_end['dueno_title'] = $this->lang->line('dueno_title');
		$array_end['fecha_entrega'] = $this->lang->line('fecha_entrega');
		$array_end['trabajar_btn'] = $this->lang->line('trabajar_btn');
		$array_end['trabajando_btn'] = $this->lang->line('trabajando_btn');
		$array_end['accion_btn'] = $this->lang->line('accion_btn');
		$array_end['proyecto_titulo'] = $this->lang->line('proyecto_titulo');
		$array_end['dejar_btn'] = $this->lang->line('dejar_btn');
		
		return $array_end;	
		
	}
}

?>