<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class People extends MY_Controller {

#**************************************************************************************************
	#Display the people list
	public function index($id=0){
		$this->load_header_front(NULL);

		$this->load->model("backend/content_model","content");

		$peeps = $this->content->get_section_multiple(5,4);
		usort($peeps,array($this,'sortPeeps'));
		$data['peeps'] = $peeps;
		$data['id'] = $id;


		$this->load->view('people',$data);
		$this->load_footer_front(null);
	}
	public function sortPeeps($a, $b){
		if ($a['order'] == $b['order']) {
			return 0;
		}
		return ($a['order'] < $b['order']) ? -1 : 1;
	}
	public function sortPeepsD($a, $b){
		if ($a['order'] == $b['order']) {
			return 0;
		}
		return ($a['order'] > $b['order']) ? -1 : 1;
	}
#**************************************************************************************************
	#Display the detail of the people page
	public function detail($id=0){
		$this->load_header_front(NULL);

		$this->load->model("backend/content_model","content");

        $team = $this->content->get_section_multiple(5,4);

		$t = NULL;
		foreach($team as $te):
			if($te['leader']=="on"):
				$t[]=$te;
			endif;
		endforeach;
		usort($t,array($this,'sortPeeps'));
		$data['team'] = $t;

		usort($t,array($this,'sortPeepsD'));
		$data['team2'] = $t;


		$peep = $this->content->get_section_info(5,4,$id);

		$peep['pr1'] = $this->content->get_section_info(3,3,$peep['project1']);
		$peep['pr2'] = $this->content->get_section_info(3,3,$peep['project2']);
		$peep['pr3'] = $this->content->get_section_info(3,3,$peep['project3']);
		$peep['pr4'] = $this->content->get_section_info(3,3,$peep['project4']);

        $data['peep'] = $peep;

        $data['projects'] = $this->content->get_section_multiple(5,3);
        foreach($data['projects'] as &$project):
            $people = explode(",", $project["people"]);
            if(in_array($id, $people)){
                $project['project_info'] = $this->content->get_section_info(3,3,$project['project']);
            }
        endforeach;


		$press = $this->content->get_info_on_select(49,$id);
		if($press){
			foreach($press as &$aw):
				$aw->fields['project_info'] = $this->content->get_section_info(3,3,$aw->fields['p_project']);
			endforeach;
			$data['press'] = $press;
		}else{
			$data['press'] = array();
		}



		$awards = $this->content->get_info_on_select(45,$id);
		if($awards){
			foreach($awards as &$aw):
				$aw->fields['project_info'] = $this->content->get_section_info(3,3,$aw->fields['a_project']);
			endforeach;
			$data['awards'] = $awards;
		}else{
			$data['awards'] = array();
		}

		$this->load->view('peep',$data);
		$this->load_footer_front(null);
	}


}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */ 