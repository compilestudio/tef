<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class About extends MY_Controller {

    public function index(){
        $this->load_header_front(NULL);
        $this->load->model("backend/content_model","content");
        $data["info"] = $this->content->get_section_info(6,7);

        $this->load->view('about',$data);
        $this->load_footer_front(null);
    }
}