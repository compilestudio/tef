<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Projects extends MY_Controller {

    public function index($cat="all"){
        $this->load_header_front(NULL);
        $this->load->model("backend/content_model","content");
        $data['category'] = $cat;
        $cat_ = 0;
        if($cat == "featured"){
            $featured = $this->content->get_info_on_select(14,"on");
            $data['projects'] = $featured;
        }elseif($cat == "all"){
            $data["all_projects"] = $this->content->get_section_multiple(3, 3);
            $data['type'] =1;
        }else{
            $categories = $this->content->get_section_multiple(3,2);
            foreach($categories as &$category):
                if(strtoupper($category["category"]) == strtoupper($cat)){
                    #$cat_ = $category["row"];
                    #$projects = $this->content->get_info_on_select(13,$category['row']);
                    $cat_ = $category["row"];
                    break;
                }
            endforeach;

            $projects = $this->content->get_section_multiple(3, 3);
            $p_ = NULL;
            foreach($projects as $p):
                $cats = explode(",",$p['p_cat']);
                if(in_array($cat_,$cats))
                    $p_[] = $p;
            endforeach;
            $data['projects'] = $p_;

            $data['categories'] = $categories;
            $data['type'] =2;
        }
        $this->load->view('projects',$data);
        $this->load_footer_front(null);
    }
#**************************************************************************************************

	public function detail($id=0){
        $this->load->model("backend/content_model","content");
        $project = $this->content->get_section_info(3, 3, $id);
        $project['client_info'] = $this->content->get_section_info(14,11,$project['client']);
        $data["project"] = $project;
        $this->load_header_front($data);

        $data["images"] = $this->content->get_files_result(3, 3, 57, $id);

        $data["id"] = $id;
        //team
        $data["team"] = $this->content->get_section_multiple(5, 4);

        //awards
        $data["awards"] = $this->content->get_section_multiple(10, 5    );

        //press
        $data["press"] = $this->content->get_section_multiple(11, 6);

        //related
        $data["projects"] = $this->content->get_section_multiple(3, 3);

        $data['team_projects'] = $this->content->get_section_multiple(5,3);


        foreach($data['team_projects'] as &$project):
            $people = explode(",", $project["people"]); //team in project
            if($project["row"] == $id){
                foreach($data["team"] as &$item){
                    if(in_array($item["row"], $people)){
                        $item["in_project"] = true;
                    }
                }
                break;
            }
        endforeach;


        $this->load->view('project_detail',$data);
        $this->load_footer_front(null);
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */ 