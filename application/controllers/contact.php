<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contact extends MY_Controller {

    public function index(){
        $this->load_header_front(NULL);
        $this->load->model("backend/content_model","content");
        $data["info"] = $this->content->get_section_info(16,7);

        $this->load->view('contact',$data);
        $this->load_footer_front(null);
    }
}