<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Error extends MY_Controller
{
    private $data = array();

    function __construct()
    {
        parent::__construct();
        $this->load->helper('html');
    }

    function error_404()
    {
        $this->load_header_front(NULL);
        $this->load->view('error');
        $this->load_footer_front(null);


    }
}
/* End of file errors.php */
/* Location: ./application/controllers/errors.php */