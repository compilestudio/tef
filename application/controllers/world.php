<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class World extends MY_Controller {

    public function index($id=0){
        $this->load_header_front(NULL);
        $this->load->model("backend/content_model","content");

        $data['id'] = $id;

        $cats = $this->content->get_section_multiple(13,14);
        $cat_ = 0;
        foreach($cats as $c):
            if($c['name']==strtoupper($id)):
                $cat_ = $c["row"];
                break;
            endif;
        endforeach;

        $info = $this->content->get_section_multiple(13,13);
        $inf_ = null;
        foreach($info as &$in):
            if(!empty($in['date'])):
                $date = explode("-",$in['date']);
                $date = $date[2]."-".$date[0]."-".$date[1];
                $in['date'] = $date;
            endif;

            if(isset($in['category'])):
                $cats = explode(",",$in['category']);
            else:
                $cats = array();
            endif;
            if($id===0 || in_array($cat_,$cats)):
                $inf_[] = $in;
            endif;
        endforeach;
        $data["info"] = $inf_;

/*
        $feeds = $this->content->get_section_info(13,14);

        $this->load->library('rssparser', array($this, 'parseFile')); // parseFile method of current class
        $this->rssparser->set_feed_url('https://tefarch.wordpress.com/feed/');
        $this->rssparser->set_cache_life(30);
        $rss = $this->rssparser->getFeed(30);
        $data['rss'] = $rss;

        $url = "https://graph.facebook.com/v2.2/333318470099728/posts?access_token=CAAUU1uqIiRQBAIVnu9iNqNeeV80O8yvqurQ5AFJJhS5xbh8xSwEXxw1javZCEJB7ZC620alxSklCZAx8yuycZAJ98Ic2TPH4Fg9bJRh2yL57wiwXPOjFAqxZCF83TBhGFMrZBO8vrrEIhKSaYJUuxIbGESeP5S5c2FEpbQQiR5uHpY396DqRb9iVRCVxuTkPCVZCuUXMjstEwZDZD";
        //  Initiate curl
        $ch = curl_init();
// Disable SSL verification
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
// Will return the response, if false it print the response
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
// Set the url
        curl_setopt($ch, CURLOPT_URL,$url);
// Execute
        $result=curl_exec($ch);
// Closing
        curl_close($ch);

// Will dump a beauty json :3
        $fb = json_decode($result, true);
        $data['fb'] = $fb;
        #echo $info['data'][0]['name'];
        #echo $info['data'][0]['description'];
        #echo $info['data'][0]['picture'];

        if(($id===0 || $id=="dialog")):
            foreach($rss as $r):
                $array = array(
                    'title' => $r['title'],
                    'image' => '',
                    'link' =>$r['link'],
                    'description' => $r['description'],
                    'row' => 'blog',
                    'date'=> $r['pubDate']
                );
                $data['info'][] = $array;
            endforeach;
        endif;

        if(($id===0 || $id=="other")):
            foreach($fb['data'] as $r):
                $name = "";
                if(!empty($r['name']))
                    $name = $r['name'];
                $array = array(
                    'title' => $name,
                    'image' => '',
                    'link' =>"https://www.facebook.com/".$r['id'],
                    'description' => @$r['story'],
                    'row' => 'fb',
                    'date' => $r['created_time']
                );
                $data['info'][] = $array;
            endforeach;
        endif;
*/

        $this->load->view('world',$data);
        $this->load_footer_front(null);
    }
    function parseFile($data, $item) {
        #$data['summary'] = (string)$item->summary;
        return $data;
    }
}