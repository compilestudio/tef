<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Clients extends MY_Controller {

    public function index($id=0){
        $this->load_header_front(NULL);
        $this->load->model("backend/content_model","content");
        $info = $this->content->get_section_multiple(14,11);
        foreach($info as &$in):
            $in['projects'] = $this->content->get_info_on_select(74,$in['row']);
            if(!empty($in['client_cat'])):
                $in['client_category'] =$this->content->get_section_info(14,15,$in['client_cat']);
            else:
                $in['client_category'] = array("category"=>"NO CATEGORY","order"=>999999);
            endif;
        endforeach;

        if(!empty($info))
            usort($info,array($this,'sortClients'));

        $data["info"] = $info;
        $data['id'] = $id;

        $this->load->view('clients',$data);
        $this->load_footer_front(null);
    }
    private function sortClients($a,$b){
        if ($a['client_category']['order'] == $b['client_category']['order']) {
            return 0;
        }
        return ($a['client_category']['order'] < $b['client_category']['order']) ? -1 : 1;
    }
}