<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
 * Fecha Creación: 2011-dic-21
 * Autor: Víctor López
 * Fecha última modificación: 2012-mar-13
 * Último en modificarla:  Pedro Izaguirre
 * 
 * Descripción: 
 * */

class Session extends MY_Controller {
	
	public function index() {
		
		redirect('backend/session/start');
		
	}
#We start the session	
	public function start() {
		
		$email = $this->input->post('user_name');
		$password = $this->input->post('password');
		
		$this->lang->load('backend', $this->idioma);
		
		#si recibimos datos via post
		if ($email && $password):
		
			#cargamos el modelo de sesión
			$this->load->model('backend/session_model');
			
			#si el usuario está autorizado 
			if ($this->session_model->authorized($email, $password)) :
				
				#si el usuario pidió que se recordara su sesión
				if ($this->input->post('remember')) :
					$this->session_model->remember();
				endif;

				redirect('backend/home');
			endif;
			
		endif;
		redirect('backend/landing/1');
				
	}
	
	public function display() {
	
		echo '<pre>' . print_r($this->session->all_userdata(), true) . '</pre>';
		
	}
	#End the session
	public function end() {
		$this->session->sess_destroy();
		redirect('backend/landing');
	}
	
} 