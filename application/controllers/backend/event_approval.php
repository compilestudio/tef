<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Event_approval extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
/*	public function index()
	{
		$this->load_header(":::Backend:::");
		$this->load->model("backend/content_model","content");
		$data = $this->content->content_info(1,0);
		
		$sql = "SELECT 	* 
				FROM 	event
				where	b=0";
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0):
			$data['rows'] = $query->result(); 
		else:
			$data['rows'] = NULL;
		endif;
		
		$this->load->view('backend/event_approval',$data);
		$this->load_footer();
	}*/
	#********************************************************
	public function index($id=0,$org=0){
		$this->load_header(":::Backend:::");
		$this->load->model("backend/content_model","content");
	
		$data = $this->content->content_info(1,0);
		$this->content->set_table("calendar");
		$sql = "SELECT c.id,c.event_name,max(date) max_,date_format(min(date),'%M %d %Y') min_ 
				FROM calendar c, calendar_dates cd
				where cd.calendar_id=c.id
				and approved=0 ";
		// if($org!=0)
			// $sql .= " and org_id=".$org;
		// else 
			// $sql .= " and org_id=0";
		if($id!=0)
			$sql .=" or c.id=".$id;
		$sql .= " group by c.id
				order by event_name";
		$query =  $this->db->query($sql);
		$data['events'] = $query->result();
		
		$data['description'] = $this->content->get_data(108);
		
		#We get the locations	
		$data['location'] = $this->content->get_sections(19);
		
		
		$this->content->set_table("organization");
		$data['organizations'] = $this->content->list_rows('*');
		
		if($id!=0):
			$this->content->set_table("calendar");
			$data['info'] = $this->content->get($id);
			
			$this->content->set_table("calendar_dates");
			$data['dates'] = $this->content->list_rows('*','calendar_id='.$id);
			
			$this->content->set_table("calendar_category");
			$categories = $this->content->list_rows('*','calendar_id='.$id);
			$data['categories_'] = NULL;
			if($categories)
				foreach($categories as $cat):
					$data['categories_'][$cat->category] = 1;
				endforeach;	

			$this->content->set_table("organization");								
			if($org==0):
				$org = $data['info']->org_id;
			endif;
			$data['orga'] = $this->content->get($org);		
		endif;
		
		$this->content->set_table("content_category");
		$data['categories'] = $this->content->list_rows('*','content_id=6');
		
		$this->content->set_table("content");
		$data['display_type'] = $this->content->list_rows('*','father=8');
		
		$this->content->set_table("content_category");
		$data['venue_category'] = $this->content->list_rows('*','content_id=19','category_order asc');
		
		$this->load->view('backend/event_detail3',$data);
		
		$this->load_footer();
	}
#********************************************************
	public function detail($id){
		$this->load_header(":::Backend:::");
		$this->load->model("backend/content_model","content");
		$data = $this->content->content_info(1,0);
		
		$data['info'] = $this->content->get_data($id);
		
		$this->content->set_table("content_category");
		$data['categories'] = $this->content->list_rows('*','content_id=6');
		
		$this->content->set_table("content");
		$data['display_type'] = $this->content->list_rows('*','father=8');
		
		$this->content->set_table("event");
		$data['info'] = $this->content->get($id);
		
		$this->content->set_table("event_category");
		$categories = $this->content->list_rows('*','event_id='.$id);
		$data['categories_'] = NULL;
		if($categories)
			foreach($categories as $cat):
				$data['categories_'][$cat->category_id] = 1;
			endforeach;
		
		$data['event_id'] = $id;
		
		$this->load->view('backend/event_detail',$data);
		
		$this->load_footer();
	}
	#********************************************************
	public function create($id=0,$org=0,$based=0){
		$this->load_header(":::Backend:::");
		$this->load->model("backend/content_model","content");
		$data = $this->content->content_info(1,0);
		
		#We get the locations
		$data['location'] = $this->content->get_sections(19);
		
		$this->content->set_table("calendar");
		$sql = "SELECT 	c.id,c.event_name,max(date) max_,date_format(min(date),'%M %d %Y') min_ 
				FROM 	calendar c
				left 	join calendar_dates cd
				on 		cd.calendar_id=c.id
				where 	approved=1";
		if($org!=0)
			$sql .= " and org_id=".$org;
		else 
			$sql .= " and org_id=0";
		if($id!=0)
			$sql .=" or c.id=".$id;
		$sql .= " group by c.id
				order by event_name asc";
		$query =  $this->db->query($sql);
		$data['events'] = $query->result();
		$data['description'] = $this->content->get_data(108);
		
		if($org!=0):
			$sql = "select	count(1) quatity
					from	calendar
					where	approved=1
					and		org_id=".$org;
			$query =  $this->db->query($sql);
			$data['ev_org'] = $query->result();
		endif;
		
		#We get the locations	
		$data['location'] = $this->content->get_sections(19);
		
		$this->content->set_table("organization");
		$data['organizations'] = $this->content->list_rows('*',NULL,'organization asc');
		
		if($org!=0):
			$data['orga'] = $this->content->get($org);
		endif;
		
		if($id==0 && $based!=0):
			$id = $based;
		endif;
		
		if($id!=0):
			$this->content->set_table("calendar");
			$data['info'] = $this->content->get($id);
			
			$this->content->set_table("calendar_dates");
			$data['dates'] = $this->content->list_rows('*','calendar_id='.$id);
			
			$this->content->set_table("calendar_category");
			$categories = $this->content->list_rows('*','calendar_id='.$id);
			$data['categories_'] = NULL;
			if($categories)
				foreach($categories as $cat):
					$data['categories_'][$cat->category] = 1;
				endforeach;			
		endif;
		
		$this->content->set_table("content_category");
		$data['categories'] = $this->content->list_rows('*','content_id=6','name asc');
		
		$this->content->set_table("content");
		$data['display_type'] = $this->content->list_rows('*','father=8','title asc');
		
		$this->content->set_table("content_category");
		$data['venue_category'] = $this->content->list_rows('*','content_id=19','name asc');
		
		$this->content->set_table("repeating_dates");
		$data['repeating_dates'] = $this->content->list_rows('*','calendar_id='.$id);
		
		
		$this->load->view('backend/event_detail2',$data);
		
		$this->load_footer();
	}
	#********************************************************
	public function create2($id=0,$org=0,$based=0){
		$this->load_header(":::Backend:::");
		$this->load->model("backend/content_model","content");
		$data = $this->content->content_info(1,0);
		
		#We get the locations
		$data['location'] = $this->content->get_sections(19);
		
		$this->content->set_table("calendar");
		$sql = "SELECT 	c.id,c.event_name,max(date) max_,date_format(min(date),'%M %d %Y') min_ 
				FROM 	calendar c
				left 	join calendar_dates cd
				on 		cd.calendar_id=c.id
				where 	approved=1";
		if($org!=0)
			$sql .= " and org_id=".$org;
		else 
			$sql .= " and org_id=0";
		if($id!=0)
			$sql .=" or c.id=".$id;
		$sql .= " group by c.id
				order by event_name asc";
		$query =  $this->db->query($sql);
		$data['events'] = $query->result();
		$data['description'] = $this->content->get_data(108);
		
		if($org!=0):
			$sql = "select	count(1) quatity
					from	calendar
					where	approved=1
					and		org_id=".$org;
			$query =  $this->db->query($sql);
			$data['ev_org'] = $query->result();
		endif;
		
		#We get the locations	
		$data['location'] = $this->content->get_sections(19);
		
		$this->content->set_table("organization");
		$data['organizations'] = $this->content->list_rows('*',NULL,'organization asc');
		
		if($org!=0):
			$data['orga'] = $this->content->get($org);
		endif;
		
		if($id==0 && $based!=0):
			$id = $based;
		endif;
		
		if($id!=0):
			$this->content->set_table("calendar");
			$data['info'] = $this->content->get($id);
			
			$this->content->set_table("calendar_dates");
			$data['dates'] = $this->content->list_rows('*','calendar_id='.$id);
			
			$this->content->set_table("calendar_category");
			$categories = $this->content->list_rows('*','calendar_id='.$id);
			$data['categories_'] = NULL;
			if($categories)
				foreach($categories as $cat):
					$data['categories_'][$cat->category] = 1;
				endforeach;			
		endif;
		
		$this->content->set_table("content_category");
		$data['categories'] = $this->content->list_rows('*','content_id=6','name asc');
		
		$this->content->set_table("content");
		$data['display_type'] = $this->content->list_rows('*','father=8','title asc');
		
		$this->content->set_table("content_category");
		$data['venue_category'] = $this->content->list_rows('*','content_id=19','name asc');
		
		
		$this->load->view('backend/event_detail2',$data);
		
		$this->load_footer();
	}
#********************************************************
	public function add(){
		$this->load->model("backend/content_model","content");
		$recurance = $this->input->post('recurance');
		$approved =$this->input->post('approved');
		
		#revisamos si viene lorganization
		$org = $this->input->post("select_organization");
		$this->content->set_table("organization");
		$data = array(
			'organization'=> $this->input->post('organization'),
			'name'=> $this->input->post('name'),
			'phone'=> $this->input->post('phone'),
			'email'=> $this->input->post('email'),
			'information'=> $this->input->post('host')
		);
		#Si es nueva
		if($org==0):
			$org = $this->content->insert($data);
		else:
			$this->content->update($data,$org);
		endif;
		
		#revisamos si viene Venue
		$venue = $this->input->post('location');
		$this->content->set_table('content');
		#Armamos la información para el contenido
		$data = array(
			'title' => $this->input->post("other_location"),
			'father' => 19,
			'parent_category'=>13
		);
		if($venue==0):
			$content_id = -1;
		else:
			$content_id= $venue;
		endif;
		$venue_cat = $this->input->post('venue_category');
		if($venue_cat)
			$data['parent_category'] = $venue_cat;
		$content_id = $this->content->save_content($data,$content_id);
		$street = $this->input->post("venue_street");
		$zip = $this->input->post("zip_code");
		#address
		$address = $street.", San Francisco, CA, United States";
		$result_ = $this->decodeAddress($address);
		$result_ = explode(",",$result_);
	
		#insertamos los campos
		$this->content->insert_field_value(71,$street,"input_big",$content_id);
		$this->content->insert_field_value(132,$zip,"input_small",$content_id);
		$this->content->insert_field_value(119,$result_[0],"input_small",$content_id);
		$this->content->insert_field_value(120,$result_[1],"input_small",$content_id);
		$venue = $content_id;
		
		#Insertamos el calendario
		$data = array (
			'name' => $this->input->post('name'),
			'organization' => $this->input->post('organization'),
			'phone' => $this->input->post('phone'),
			'email' => $this->input->post('email'),
			'event_name' => $this->input->post('section_title'),
			'location' => $venue,
			'event_website' => $this->input->post('website'),
			'main_category' => $this->input->post('parent_category_'),
			'event_image' => $this->input->post('image1'),
			'caption1' => $this->input->post('credit1'),
			'big_image' => $this->input->post('big_image'),
			'caption2' => $this->input->post('credit2'),
			'event_information' => $this->input->post('information'),
			'sponsors' => $this->input->post('host'),
			'display_type' => $this->input->post('display_type'),
			'recurance' => $recurance,
			'recurrence_type' => $this->input->post('recurrence_type'),
			'end_date' => 0,
			'approved' => $approved,
			'based_on' => $this->input->post('base_on'),
			'extra_directions' => $this->input->post('extra_directions'),
			'org_id' => $org
		);
		$this->content->set_table("calendar");
		
		$event = $this->input->post('event');
		if($event==-1 || $event==0):
			$id = $this->content->insert($data);
		else:
			$id = $event;
			$this->content->update($data,$id);
		endif;
		
		$categories = $this->input->post('category_select');
		if($event!=-1):
			#When we update
			$this->db->delete('calendar_category', array('calendar_id' => $id));
		endif; 
		if(count($categories)>0):
			$this->content->set_table("calendar_category");
			if($categories)
			foreach($categories as $cat):
				$param = array(
							'calendar_id' => $id,
							'category' => $cat
						 );
				$this->content->insert($param);
			endforeach;
		endif;
		
		$this->content->set_table("calendar_dates");
		$dates = $this->input->post('event_date');
		if($event!=-1):
			#When we update
			$this->db->delete('calendar_dates', array('calendar_id' => $id));
		endif;
		$start = $this->input->post('start_time');
		$end = $this->input->post('end_time');
		if($recurance==1 || $recurance==3):
			$empieza = 0;
			$terminar = 1;
		else:
			$empieza = 1;
			$terminar = count($dates);
		endif;
		
		$start_date_mul = $this->input->post('start_date_mul');
		$end_date_mul = $this->input->post('end_date_mul');
		#Consecutive events
		if($start_date_mul!="" && $end_date_mul!=""):
			$times = $this->datediff('d', $start_date_mul, $end_date_mul, false);
			$date = $start_date_mul;
			for($i=0;$i<=$times;$i++):
				$param = array(
					'calendar_id' => $id,
					'date' => $date,
					'start_time' => $this->input->post('start_mul'),
					'end_time' => $this->input->post('end_mul'),
					'non_consec' => 0
				 );
				 $this->content->insert($param);
				 $date = date("Y-m-d",strtotime(date("Y-m-d", strtotime($date)) . " +1 day"));
			endfor;
		else:
			#Non consecutive Dates
			for($i=$empieza;$i<$terminar;$i++):
				if($dates[$i]!=""):
					$param = array(
						'calendar_id' => $id,
						'date' => $dates[$i],
						'start_time' => $start[$i],
						'end_time' => $end[$i],
						'non_consec' => 1
					 );
					 $this->content->insert($param);
				endif;
			endfor;
		endif;
		
		#Eliminamos las fechas que se repiten
		$this->content->set_table("calendar_repeat");
		$this->db->delete('calendar_repeat', array('calendar_id' => $id));
		#Eliminamos las fechas de la tabla 
		$this->content->set_table("repeating_dates");
		$this->db->delete('repeating_dates', array('calendar_id' => $id)); 
		#Insertamos las fechas de las repeticiones
		$recurrence_type=$this->input->post('recurrence_type');
		$dates_ = $dates[0];
		$end_date = $this->input->post('end_date');
		if($recurance==3):
			$start_dates = $this->input->post("start_date");
			$end_dates = $this->input->post("end_date");
			$start_times = $this->input->post("start_time_m");
			$end_times = $this->input->post("end_time_m");
			for($k=0;$k<count($start_dates);$k++):
				$dates_ = $start_dates[$k];
				$end_date = $end_dates[$k];
				$start_time = $start_times[$k];
				$end_time = $end_times[$k];
				#We insert the dates to the database
				$this->content->set_table("repeating_dates");
				$data_ = array(
						'start_date'=>$dates_,
						'end_date'=> $end_date,
						'start_time'=>$start_time,
						'end_time' => $end_time,
						'calendar_id' =>$id
				);
				$this->content->insert($data_);
				$this->content->set_table("calendar_repeat");
				#weekly
				if($recurrence_type==1):
					$times = $this->datediff('ww', $dates_, $end_date, false); 
					for($i=0;$i<=$times;$i++):
						$_date = date("Y-m-d",strtotime(date("Y-m-d", strtotime($dates_)) . " +".$i." week"));
						$data = array (
							'name' => $this->input->post('name'),
							'organization' => $this->input->post('organization'),
							'phone' => $this->input->post('phone'),
							'email' => $this->input->post('email'),
							'event_name' => $this->input->post('section_title'),
							'location' => $venue,
							'event_website' => $this->input->post('website'),
							'main_category' => $this->input->post('parent_category_'),
							'event_image' => $this->input->post('image1'),
							'caption1' => $this->input->post('credit1'),
							'big_image' => $this->input->post('big_image'),
							'caption2' => $this->input->post('credit2'),
							'event_information' => $this->input->post('information'),
							'sponsors' => $this->input->post('host'),
							'display_type' => $this->input->post('display_type'),
							'recurance' => $recurance,
							'recurrence_type' => $recurrence_type,
							'date_' => $_date,
							'start_time' => $start_time,
							'end_time' => $end_time,
							'approved' => $approved,
							'based_on' => $this->input->post('base_on'),
							'org_id' => $org,	
							'calendar_id' => $id
						);
						$this->content->insert($data);
					endfor;
				endif;
				#Biweekly
				if($recurrence_type==2):
					$times = $this->datediff('ww', $dates_, $end_date, false);
					for($i=0;$i<$times;$i+=2):
						$_date = date("Y-m-d",strtotime(date("Y-m-d", strtotime($dates_)) . " +".$i." week"));
						$data = array (
							'name' => $this->input->post('name'),
							'organization' => $this->input->post('organization'),
							'phone' => $this->input->post('phone'),
							'email' => $this->input->post('email'),
							'event_name' => $this->input->post('section_title'),
							'location' => $venue,
							'event_website' => $this->input->post('website'),
							'main_category' => $this->input->post('parent_category_'),
							'event_image' => $this->input->post('image1'),
							'caption1' => $this->input->post('credit1'),
							'big_image' => $this->input->post('big_image'),
							'caption2' => $this->input->post('credit2'),
							'event_information' => $this->input->post('information'),
							'sponsors' => $this->input->post('host'),
							'display_type' => $this->input->post('display_type'),
							'recurance' => $recurance,
							'recurrence_type' => $recurrence_type,
							'date_' => $_date,
							'start_time' => $start[0],
							'end_time' => $end[0],
							'approved' => $approved,
							'based_on' => $this->input->post('base_on'),
							'org_id' => $org,	
							'extra_directions' => $this->input->post('extra_directions'),
							'calendar_id' => $id	
						);
						$this->content->insert($data);
					endfor;
				endif;
				#Month same day
				if($recurrence_type==3):
					$times = $this-> datediff('m', $dates_, $end_date, false);
					$day =  date("w",strtotime($dates_));
					$week_month = $this->week_number($dates_);
					for($i=0;$i<$times;$i++):
						$month = date("n",strtotime(date("Y-m-d", strtotime($dates_)) . " +".$i." month"));
						$year = date("Y",strtotime(date("Y-m-d", strtotime($dates_)) . " +".$i." month"));
						$day_ = $this->get_Xth_DOW($day,$week_month,intval($month),intval($year));
						if(strlen($day_)<2)
							$day_ = "0".$day_;
						if(strlen($month)<2)
							$month = "0".$month;
						$_date = $year."-".$month."-".$day_;
						$data = array (
							'name' => $this->input->post('name'),
							'organization' => $this->input->post('organization'),
							'phone' => $this->input->post('phone'),
							'email' => $this->input->post('email'),
							'event_name' => $this->input->post('section_title'),
							'location' => $venue,
							'event_website' => $this->input->post('website'),
							'main_category' => $this->input->post('parent_category_'),
							'event_image' => $this->input->post('image1'),
							'caption1' => $this->input->post('credit1'),
							'big_image' => $this->input->post('big_image'),
							'caption2' => $this->input->post('credit2'),
							'event_information' => $this->input->post('information'),
							'sponsors' => $this->input->post('host'),
							'display_type' => $this->input->post('display_type'),
							'recurance' => $recurance,
							'recurrence_type' => $recurrence_type,
							'date_' => $_date,
							'start_time' => $start[0],
							'end_time' => $end[0],
							'approved' => $approved,
							'based_on' => $this->input->post('base_on'),
							'org_id' => $org,	
							'extra_directions' => $this->input->post('extra_directions'),
							'calendar_id' => $id	
						);
						$this->content->insert($data);
					endfor;
				endif;
				#month same date
				if($recurrence_type==4):
					$times = $this->datediff('m', $dates_, $end_date, false);
					for($i=0;$i<$times;$i++):
						$_date = date("Y-m-d",strtotime(date("Y-m-d", strtotime($dates_)) . " +".$i." month"));
						$data = array (
							'name' => $this->input->post('name'),
							'organization' => $this->input->post('organization'),
							'phone' => $this->input->post('phone'),
							'email' => $this->input->post('email'),
							'event_name' => $this->input->post('section_title'),
							'location' => $venue,
							'event_website' => $this->input->post('website'),
							'main_category' => $this->input->post('parent_category_'),
							'event_image' => $this->input->post('image1'),
							'caption1' => $this->input->post('credit1'),
							'big_image' => $this->input->post('big_image'),
							'caption2' => $this->input->post('credit2'),
							'event_information' => $this->input->post('information'),
							'sponsors' => $this->input->post('host'),
							'display_type' => $this->input->post('display_type'),
							'recurance' => $recurance,
							'recurrence_type' => $recurrence_type,
							'date_' => $_date,
							'start_time' => $start[0],
							'end_time' => $end[0],
							'approved' => $approved,
							'based_on' => $this->input->post('base_on'),
							'org_id' => $org,	
							'extra_directions' => $this->input->post('extra_directions'),
							'calendar_id' => $id
						);
						$this->content->insert($data);
					endfor;
				endif;
			endfor;
		endif;		
		$this->content->set_table("content");
		$approval = $this->input->post("approval");
		if($approval):
			redirect("backend/event_approval/0/0/1");
		elseif($approved==0):
			redirect("add_event/0/0/1");
		else:
			redirect("backend/event_approval/create/".$id."/".$org."/1");
		endif;
	}
#********************************************************
	public function delete_event(){
		$id = $this->input->post("id");
		$this->load->model("backend/content_model","content");
		$this->content->set_table("calendar");
		$this->content->delete($id);
		$this->db->delete('calendar_repeat', array('calendar_id' => $id));
	}
	public function datediff($interval, $datefrom, $dateto, $using_timestamps = false) {
	    /*
	    $interval can be:
	    yyyy - Number of full years
	    q - Number of full quarters
	    m - Number of full months
	    y - Difference between day numbers
	        (eg 1st Jan 2004 is "1", the first day. 2nd Feb 2003 is "33". The datediff is "-32".)
	    d - Number of full days
	    w - Number of full weekdays
	    ww - Number of full weeks
	    h - Number of full hours
	    n - Number of full minutes
	    s - Number of full seconds (default)
	    */
	    
	    if (!$using_timestamps) {
	        $datefrom = strtotime($datefrom, 0);
	        $dateto = strtotime($dateto, 0);
	    }
	    $difference = $dateto - $datefrom; // Difference in seconds
	     
	    switch($interval) {
	     
	    case 'yyyy': // Number of full years
	
	        $years_difference = floor($difference / 31536000);
	        if (mktime(date("H", $datefrom), date("i", $datefrom), date("s", $datefrom), date("n", $datefrom), date("j", $datefrom), date("Y", $datefrom)+$years_difference) > $dateto) {
	            $years_difference--;
	        }
	        if (mktime(date("H", $dateto), date("i", $dateto), date("s", $dateto), date("n", $dateto), date("j", $dateto), date("Y", $dateto)-($years_difference+1)) > $datefrom) {
	            $years_difference++;
	        }
	        $datediff = $years_difference;
	        break;
	
	    case "q": // Number of full quarters
	
	        $quarters_difference = floor($difference / 8035200);
	        while (mktime(date("H", $datefrom), date("i", $datefrom), date("s", $datefrom), date("n", $datefrom)+($quarters_difference*3), date("j", $dateto), date("Y", $datefrom)) < $dateto) {
	            $months_difference++;
	        }
	        $quarters_difference--;
	        $datediff = $quarters_difference;
	        break;
	
	    case "m": // Number of full months
	
	        $months_difference = floor($difference / 2678400);
	        while (mktime(date("H", $datefrom), date("i", $datefrom), date("s", $datefrom), date("n", $datefrom)+($months_difference), date("j", $dateto), date("Y", $datefrom)) < $dateto) {
	            $months_difference++;
	        }
	        #$months_difference--;
	        $datediff = $months_difference;
	        break;
	
	    case 'y': // Difference between day numbers
	
	        $datediff = date("z", $dateto) - date("z", $datefrom);
	        break;
	
	    case "d": // Number of full days
	
	        $datediff = floor($difference / 86400);
	        break;
	
	    case "w": // Number of full weekdays
	
	        $days_difference = floor($difference / 86400);
	        $weeks_difference = floor($days_difference / 7); // Complete weeks
	        $first_day = date("w", $datefrom);
	        $days_remainder = floor($days_difference % 7);
	        $odd_days = $first_day + $days_remainder; // Do we have a Saturday or Sunday in the remainder?
	        if ($odd_days > 7) { // Sunday
	            $days_remainder--;
	        }
	        if ($odd_days > 6) { // Saturday
	            $days_remainder--;
	        }
	        $datediff = ($weeks_difference * 5) + $days_remainder;
	        break;
	
	    case "ww": // Number of full weeks
	
	        $datediff = floor($difference / 604800);
	        break;
	
	    case "h": // Number of full hours
	
	        $datediff = floor($difference / 3600);
	        break;
	
	    case "n": // Number of full minutes
	
	        $datediff = floor($difference / 60);
	        break;
	
	    default: // Number of full seconds (default)
	
	        $datediff = $difference;
	        break;
	    }    
	
	    return $datediff;
	
	}
	public function get_Xth_DOW($DOW,$X,$M,$Y) {
		$numDays = date('t',mktime(0,0,0,$M,1,$Y));
		$add = 7*($X - 1);
		$firstDOW = date('w',mktime(0,0,0,$M,1,$Y));
		$diff = $firstDOW - $DOW;
		$d = 1;
		if($diff > 0) { $d += (7 - $diff); }
		elseif ($diff < 0) {
			$d += -1*$diff; 
		}
		$d = $d + $add;
		while($d > $numDays) { $d -= 7; }
		return $d;
	}
	public function week_number($date){
	    return ceil(substr($date, -2) / 7);
	}
#************************************************************************************************************************
	#Decodificamos la direccion a lat y long
	function decodeAddress($address){
		define("MAPS_HOST", "maps.google.com");
		define("KEY", "ABQIAAAAhs_VtsoXFrrSgFgu9vIyzhROQ65eSPjAHv4W4yl7sTA8ZOdUvRTJvBTExxkJz8BVXJ1NQU7AhYZoig");
		$base_url = "http://" . MAPS_HOST . "/maps/geo?output=xml" . "&key=" . KEY;
		$address = str_replace(array("\r", "\r\n", "\n"), '', $address);
		$request_url = $base_url . "&q=" . urlencode($address);
	    $xml = simplexml_load_file($request_url) or die("url not loading");
	
	    $status = $xml->Response->Status->code;
		if (strcmp($status, "200") == 0) {
	      // Successful geocode
	      $geocode_pending = false;
	      $coordinates = $xml->Response->Placemark->Point->coordinates;
	      $coordinatesSplit = split(",", $coordinates);
	      // Format: Longitude, Latitude, Altitude
	      $lat = $coordinatesSplit[1];
	      $lng = $coordinatesSplit[0];
			return $lat.",".$lng;
	    }else{
	    	return "0,0";
	    }
	}
#************************************************************************************************************************
	#we update the org information
	function update_org(){
		$this->load->model("backend/content_model","content");
		$this->content->set_table("organization");
		$data = array(
			'organization'=>$this->input->post("organization"),
			'name'=>$this->input->post("name"),
			'phone'=>$this->input->post("phone"),
			'email'=>$this->input->post("email")
		);
		
		$this->content->update($data,$this->input->post("id"));
	}
#************************************************************************************************************************
	#We delete an organization
	function delete_org(){
		$this->load->model("backend/content_model","content");
		$this->content->set_table("organization");
		
		$this->content->delete($this->input->post("id"));
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */

