<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Landing extends MY_Controller {

#**************************************************************************************************
	#Display the landing page
	public function index($error=0){
		$this->load_header_front(NULL);
        $this->load->model("backend/content_model","content");
        $data["main"] = $this->content->get_section_info(2,7);
        $images = $this->content->get_section_multiple(2,8);
		$data["images"] = $images;

        //featured projects
        $data["projects"] = $this->content->get_section_multiple(3,3);
		$lower_s = $this->content->get_section_multiple(2,10);
		foreach($lower_s as &$l):
			if($l['project']!=0)
				$l['pinfo'] = $this->content->get_section_info(3,3,$l['project']);
		endforeach;
		$data["lower_s"] = $lower_s;
		$this->load->view('landing',$data);
		$this->load_footer_front(null);
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */ 