<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Landing extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index($error=0)
	{
		#If logged redirect
		$user=$this->session->userdata('super_user_id');
		if($user):
			redirect("super/home");
		endif;
		
		$this->load_header_super(":::Superuser Log-in:::");
		$this->load->model("super/Landing_model","landing");
		$landing_data = $this->landing->landing_info();
		
		#Mandamos el parametro de error
		$landing_data['error']=$error;
		$this->load->view('super/landing_page',$landing_data);
		$this->load_footer_super();
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */