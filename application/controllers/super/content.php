<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Content extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index($id,$father=0)
	{
		$this->load_header_super(":::Super admin:::");
		$this->load->model("super/content_model","content");
		$data = $this->content->content_info($id,$father);
		$this->load->view('super/content_view',$data);
		$this->load_footer_super();
	}
#******************************************************************************************************************************************************************
	#Funcion para guardar la información del contenido
	function save_content(){
		$content_id = $this->input->post("id");
		$title = $this->input->post("title");		
		$order = $this->input->post("order");
		$description = $this->input->post("description");
		$father = $this->input->post("father");
		if(!$father)	
			$father = -1;
		$deleted = $this->input->post("deleted");
		if(!$deleted)	
			$deleted = 0;
		$front_display = $this->input->post("front_display");
		if(!$front_display)	
			$front_display = 0;
		$sub_sections = $this->input->post("sub_sections");
		if(!$sub_sections)	
			$sub_sections = 0;		
		
		$info = array(
				'title' => $title,
				'description'=> $description,
				'father'=>$father,
				'deleted'=>$deleted,
				'content_order'=>$order,
				'has_sub_section' => $sub_sections,
				'front_display' => $front_display
		);
		
		$this->load->model('super/content_model','content');
		if($content_id==-1):
			$content_id = $this->content->insert($info);
			$this->add_meta_fields($content_id);
		else:
			$this->content->update($info,$content_id);
		endif;
		
		redirect("super/content/".$content_id);
	}
#******************************************************************************************************************************************************************
	#Funcion para guardar la información de la sección
	function save_section(){
		$content_id = $this->input->post("content_id");
		$section_id = $this->input->post("section_id");
		$multiple = $this->input->post("multiple");
		$image = $this->input->post("image");
		$width = $this->input->post("width");
		$height = $this->input->post("height");
		$order = $this->input->post("order");
		$description = $this->input->post("description");
		
		$data = array(
					'multiple' => $multiple,
					'image' =>$image,
					'width' => $width,
					'height' => $height,
					'order' => $order,
					'description' => $description
				);	
		$where = array(
					'content_id'=>$content_id,
					'section_id' => $section_id
				);
		
		$this->db->where($where);
		$this->db->update('content_section', $data); 
	}
#******************************************************************************************************************************************************************
	#Funcion para eliminar la sección
	function delete_section(){
		$section_id= $this->input->post("section_id");
		$content_id= $this->input->post("content_id");
		$where = array(
					'content_id'=>$content_id,
					'section_id' => $section_id
				);
		
		$this->db->where($where);
		$this->db->delete('content_section'); 
	}
#******************************************************************************************************************************************************************
	#Funcion para guardar un nuevo campo
	function add_field(){
		$content_id = $this->input->post("content_id");
		$section_id = $this->input->post("section_id");
		$name = $this->input->post("name");
		$display_text = $this->input->post("display_text");
		$type = $this->input->post("type");
		$select_id = $this->input->post("section");
		$order = $this->input->post("order");
		$order_by = $this->input->post("order_by");
		
		
		$data = array(
					'content_id'=>$content_id,
					'section_id' => $section_id,
					'name' => $name,
					'display_text' =>$display_text,
					'type_id' => $type,
					'order' => $order,
					'select_id' => $select_id,
					'order_by' => $order_by
				);	
		$this->load->model('super/content_model','content');
		$this->content->set_table('fields');
		$this->content->insert($data);
	}
#******************************************************************************************************************************************************************
	#Funcion para guardar la información del campo
	function update_field(){
		$content_id = $this->input->post("content_id");
		$section_id = $this->input->post("section_id");
		$name = $this->input->post("name");
		$display_text = $this->input->post("display_text");
		$type = $this->input->post("type");
		$select_id = $this->input->post("section");
		//$order = $this->input->post("order");
		$order_by = $this->input->post("order_by");
		$max_chars = $this->input->post("max_chars");
		$field_id = $this->input->post("id");
		
		$data = array(
					'name' => $name,
					'display_text' =>$display_text,
					'type_id' => $type,
					'select_id' => $select_id,
					'max_chars' => $max_chars,
					'order_by' => $order_by
				);	
		$where = array(
					'content_id'=>$content_id,
					'section_id' => $section_id,
					'id' => $field_id
				);
		
		$this->db->where($where);
		$this->db->update('fields', $data); 
	}		
#******************************************************************************************************************************************************************
	#Funcion para eliminar el field
	function delete_field(){
		$id = $this->input->post("id");
		#Eliminamos el contenido
		$this->db->delete('field_data', array('fields_id' => $id));
		
		#Eliminamos el campo
		$this->db->delete('fields', array('id' => $id));
		 		
	}		
#******************************************************************************************************************************************************************
	#Funcion para agregar contenido
	function add_content(){
		$name = $this->input->post("title");
		$father = $this->input->post("father");
		
		$array = array(
					'title'=>$name,
					'father'=>$father,
					'deleted' => 0,
					'content_order' => 0,
					'front_display' => 1
				);
		$this->load->model('super/content_model','content');
		$id = $this->content->insert($array);
		
		echo $id;
	}		
#******************************************************************************************************************************************************************
	#Funcion para eliminar el contenido
	function delete_content(){
		
		$id= $this->input->post("id");
		
		$this->load->model('super/content_model','content');
		$this->content->delete($id);
	}
#******************************************************************************************************************************************************************
	#Funcion para agregar una sección al contenido
	function add_content_section(){
		$content_id = $this->input->post("content_id");
		$section_id = $this->input->post("section_id");
		
		$array = array(
					'content_id'=>$content_id,
					'section_id'=>$section_id
				);
		$this->load->model('super/content_model','content');
		$this->content->set_table('content_section');
		$this->content->insert($array);
	}	
	
	
	
	
		
		
		
		
#******************************************************************************************************************************************************************
	#Funcion para agregar los campos del meta tags
	function add_meta_fields($content_id){
		$this->load->model('super/content_model','content');
		$this->content->set_table('fields');
		
		#Page title
		$data = array(
			'name' => "meta_title",
			'display_text' => "Page title",
			'content_id' => $content_id,
			'type_id' => 5,
			'order' => 1,
			'field_section' => 1,
			'select_id' => 0
		);
		
		$this->content->insert($data);
		
		#Page description
		$data = array(
			'name' => "meta_description",
			'display_text' => "Page description",
			'content_id' => $content_id,
			'type_id' => 2,
			'order' => 2,
			'field_section' => 1,
			'select_id' => 0
		);
		
		$this->content->insert($data);
		
		#Page description
		$data = array(
			'name' => "meta_keywords",
			'display_text' => "Keywords",
			'content_id' => $content_id,
			'type_id' => 2,
			'order' => 3,
			'field_section' => 1,
			'select_id' => 0
		);
		
		$this->content->insert($data);
	}
#******************************************************************************************************************************************************************
	#Funcion para agregar nuevos campos
	function add_fields(){
		$field_name = $this->input->post('field_name');
		$field_order = $this->input->post('field_order');
		$field_display_text = $this->input->post('field_display_text');
		$field_type = $this->input->post('field_type');
		$field_section = $this->input->post('field_section');
		$content_select_id = $this->input->post('content_select_id');
		$content_id = $this->input->post('content_id');
		
		if($field_name!='' && $field_order!='' && $field_display_text!=''):
			$data = array(
				'name' => $field_name,
				'display_text' => $field_display_text,
				'content_id' => $content_id,
				'type_id' => $field_type,
				'order' => $field_order,
				'field_section' => $field_section,
				'select_id' => $content_select_id
			);
			$this->load->model('super/content_model','content');
			$this->content->set_table('fields');
			$this->content->insert($data);
		endif;
		
		redirect('super/content/'.$content_id);
	}
#******************************************************************************************************************************************************************
	#Funcion para actualizar el field
	function update_field_(){
		$field_name = $this->input->post('field_name');
		$field_order = $this->input->post('field_order');
		$field_display_text = $this->input->post('display_text');
		$field_type = $this->input->post('field_type');
		$field_section = $this->input->post('field_section');
		$content_select_id = $this->input->post('content_select_id');
		$content_id = $this->input->post('content_id');
		
		$field_id = $this->input->post('id');
		
		if($field_name!='' && $field_order!='' && $field_display_text!=''):
			$data = array(
				'name' => $field_name,
				'display_text' => $field_display_text,
				'content_id' => $content_id,
				'type_id' => $field_type,
				'order' => $field_order,
				'field_section' => $field_section,
				'select_id' => $content_select_id
			);
			$this->load->model('super/content_model','content');
			$this->content->set_table('fields');
			$this->content->update($data,$field_id);
		endif;
		
		redirect('super/content/'.$content_id);
	}
#******************************************************************************************************************************************************************
	#Funcion para eliminar el field
	function delete_field_(){
		$id = $this->input->post("id");
		
		$this->load->model('super/content_model','content');
		
		#We delete the content of the fields if any
		$this->db->delete('field_content_long', array('fields_id' => $id)); 		
		$this->db->delete('field_content_small', array('fields_id' => $id));
		
		#we delete the field
		$this->db->delete('fields', array('id' => $id));
		
	}
#******************************************************************************************************************************************************************
	#Funcion para eliminar el contenido
	function delete(){
		$id = $this->input->post("id");
		
		$this->load->model('super/content_model','content');
		
		#We delete the content of the fields if any
		$this->db->delete('field_content_long', array('content_id' => $id)); 		
		$this->db->delete('field_content_small', array('content_id' => $id));
		
		#we delete the field
		$this->db->delete('fields', array('content_id' => $id));
		
		$this->content->delete($id);
		
	}
#******************************************************************************************************************************************************************
	#Funcion para actualizar el orden de los fields
	public function update_field_order(){
		$fields = $this->input->post("field");
		$this->load->model("super/content_model","content");
		$this->content->update_field_order($fields);
	}	
#********************************************************************************************************
	#Actualizamos el section
	function change_section(){
		$current = $this->input->post("current");
		$new = $this->input->post("new");
		$content_id = $this->input->post("content_id");
		
		$where = array (
				'content_id'=>$content_id,
				'section_id'=> $current
			);
		
		#Actualizamos la content_section para asignar la seccion al contenido
		$data = array(
			'section_id'=>$new
		);
		$this->db->where($where);
		$this->db->update('content_section',$data);

		#Actualizamos la data_row para ver todos los múltiples
		$this->db->where($where);
		$this->db->update('data_row',$data);

		#Actualizamos la fields para todos los campos
		$this->db->where($where);
		$this->db->update('fields',$data);

	}
}

