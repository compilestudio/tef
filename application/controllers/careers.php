<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Careers extends MY_Controller {

    public function index(){
        $this->load->helper('url');
        $this->load_header_front(NULL);
        $this->load->model("backend/content_model","content");
        $info = $this->content->get_section_multiple(17,12);

        $data["info"] = $info;

        $this->load->view('careers',$data);
        $this->load_footer_front(null);
    }
}