<link rel="stylesheet" href="<?=site_url("_css/front/superslides.css")?>">
<style>
    html{
        overflow-y: auto !important;
    }
</style>

<div class="fullscreen">
    <div class="fullscreen-header">
        <h1 class="projects-title">
            <?=$info["about_title"]?>
        </h1>
    </div>
    <div class="our-firm">
        <div class="text">
            <?=$info["text"]?>
        </div>
        <div class="image">
            <img src="<?=base_url("uploads/".$info["image"])?>" alt="image" style="width: 100%">
        </div>
    </div>
</div>


<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="<?=base_url("_js/superslides/jquery.easing.1.3.js")?>"></script>
<script src="<?=base_url("_js/superslides/jquery.animate-enhanced.min.js")?>"></script>
<script src="<?=base_url("_js/superslides/hammer.min.js")?>"></script>
<script src="<?=base_url("_js/superslides/jquery.superslides.js")?>" type="text/javascript" charset="utf-8"></script>
<script>

    $(function() {
        $('img').on('dragstart', function(event) { event.preventDefault(); });

        $(".more-btn").click(function() {
            $('html, body').animate({
                scrollTop: $(".our-firm").offset().top
            }, 1000);
        });
    });
</script>
