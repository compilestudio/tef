<link rel="stylesheet" href="<?=site_url("_css/front/superslides.css")?>">
<style>
    .content{
        background: none;
        padding: 0;
        padding-left: 40px;
    }
    html{
        overflow-y: hidden !important;
    }
</style>
<div class="fullscreen" style="margin-top:0 !important;">
    <div class="slider-content">
        <div id="slides">
            <div class="slides-container">
                <?php
                function order_slider($a, $b){
                    if ($a['order'] == $b['order']) {
                        return 0;
                    }
                    return ($a['order'] < $b['order']) ? -1 : 1;
                }
                if(isset($images)){
                    usort($images,"order_slider");
                    foreach($images as $image){
                        $project = "";
                        $pointer = "";
                        if(isset($image["project"]) && !empty($image["project"])){
                            $project = $image["project"];
                            $pointer = "pointer";
                        }
                        ?>
                        <img src="<?=base_url("uploads/".$image["image"])?>" alt="image" project="<?=$project?>" class="<?=$pointer?>">
                    <?php }
                }
                ?>
            </div>
            <nav class="slides-navigation">
                <a href="#" class="next">
                    <img src="<?=base_url("_img/related_next.png")?>" alt="next"/>
                </a> 
                <a href="#" class="prev">
                    <img src="<?=base_url("_img/related_prev.png")?>" alt="prev"/>
                </a>
            </nav>
        </div>
        <div class="more">
            <div class="more-btn">
                <img src="<?=site_url("_img/close.png")?>" alt="more"    >
                <?php if(!empty($main["more"])){?>
                    <span class="image-more">
                        <?=$main["more"]?>
                    </span>
                <?php }?>
                <?php if(!empty($main["more2"])):?>
                    <span class="image-only">
                        <?=$main["more2"]?>
                    </span>
                <?php endif;?>
            </div>
        </div>
    </div>
    <div class="owl-content" >
        <div id="owl-carousel" >
            <?php
                if(isset($lower_s)):
                    usort($lower_s,'s_cmp');
                    foreach($lower_s as $project):
                        if($project['project']==0):
                            $target = "_blank";
                            $caption = $project['scaption'];
                            $img = base_url()."uploads/".$project['simage'];
                            $link = addhttp($project['slink']);
                        else:
                            $target = "";
                            $caption = $project['pinfo']['p_name'];
                            $img = base_url()."uploads/".$project['pinfo']['pimg1'];;
                            $link = site_url("projects/detail/".$project["project"]);
                        endif;
                    ?>
                        <div class="item item-thumbnail">
                            <a href="<?=$link;?>" target="<?=$target;?>">
                                <span class="item-title">
                                    <?=$caption;?>
                                </span>
                                <img class="img-responsive" src="<?=$img;?>" alt="TEF - Featured project">
                            </a>
                        </div>
                <?php
                    endforeach;
                endif;
            ?>
        </div>
    </div>
</div>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="<?=base_url("_js/superslides/jquery.easing.1.3.js")?>"></script>
<script src="<?=base_url("_js/superslides/jquery.animate-enhanced.min.js")?>"></script>
<script src="<?=base_url("_js/superslides/hammer.min.js")?>"></script>
<script src="<?=base_url("_js/superslides/jquery.superslides.js")?>" type="text/javascript" charset="utf-8"></script>
<script>
    var hidden = 1;
    $(function() {
        $('img').on('dragstart', function(event) { event.preventDefault(); });
        var $slides = $('#slides');
        Hammer($slides[0]).on("swipeleft", function(e) {
            $slides.data('superslides').animate('next');
        });
        Hammer($slides[0]).on("swiperight", function(e) {
            $slides.data('superslides').animate('prev');
        });
        $slides.superslides({
            <?php
                if(empty($main["timelapse"])){
                    $main["timelapse"] = 0;
                }
            ?>
            play:<?=$main["timelapse"]*1000?>
        });
        $("#owl-carousel").owlCarousel({
            autoPlay : false,
            items : 4,
            itemsDesktop : [1199,4],
            itemsDesktopSmall : [979,3],
            itemsTablet: [768,3],
            itemsMobile: [480,2],
            navigation : true,
            pagination: false
        });
        $(".owl-buttons").empty();
        $(".owl-buttons").append('<div class="owl-prev"></div><div style="position:absolute; z-index:-9999"></div><div class="owl-next"></div>');
        /*$(".owl-prev").text("");
        $(".owl-next").text("");*/
        $(".slides-pagination a").text("");

        //$(".more img").hide();
        $(".more span.image-only").hide();

        //hide related projects
        $(".slides-container img").click(function() {

            //if is open
            if($(".slider-content").css("margin-top").replace('px', '') < 0){
                related_projects("hide");
                //$(".more span").hide();

            }else{
                var project = $(this).attr("project");
                if(project != ""){
                    location.href = "<?=site_url("projects/detail")?>/"+project;
                }

            }

        });
        $(".more-btn").click(function() {
            related_projects();
        });
    });
    $( window ).load( function () {
        //related_projects("show");
        if(isMobile){
            $('.owl-content .item .item-title').show();
        }
    });
    $( window ).resize( debouncer( function ( e ) {
        if(hidden==0) {
            hidden=1;
            related_projects();
        }
    }));
    function debouncer( func , timeout ) {
        var timeoutID , timeout = timeout || 400;
        return function () {
            var scope = this , args = arguments;
            clearTimeout( timeoutID );
            timeoutID = setTimeout( function () {
                func.apply( scope , Array.prototype.slice.call( args ) );
            } , timeout );
        }
    }
    function related_projects(){
        //$(".more-btn span").hide();
        if(hidden==1){
            $( ".owl-content" ).animate({
                bottom: "+="+$('.owl-content .owl-carousel .owl-item').height(),
                position: "absolute"
            }, 500);
            $( ".more" ).animate({
                bottom: "+="+$('.owl-content .owl-carousel .owl-item').height()
            }, 500);
            var src = $(".more-btn img").attr("src");
            $(".more-btn img").attr("src", src.replace("close.png", "more.png"));

            hidden=0;
            $('.image-more').hide();
            $('.image-only').show();
            $('.slider-content').css('opacity',0.6);
        }else{
            var src = $(".more-btn img").attr("src");
            $(".more-btn img").attr("src", src.replace("more.png", "close.png"));
            $( ".owl-content" ).animate({
                bottom: "-="+$('.owl-content .owl-carousel .owl-item').height(),
                position: "relative"
            }, 500);

            /*
            $( ".more" ).animate({
                bottom: "20px"+($('.owl-content .owl-carousel .owl-item').height()-300)
            }, 500);*/
            $('.more').css("bottom","10px");

            $('.image-only').hide();
            $('.image-more').show();
            $('.slider-content').css('opacity',1);
            hidden=1;
        }
        /*
        var top = 0;
        if(state == "show"){
            top = "-"+$("#owl-carousel").height()+"px";
            var src = $(".more-btn img").attr("src");
            $(".more-btn img").attr("src", src.replace("more.png", "close.png"));

        }else{
            var src = $(".more-btn img").attr("src");
            $(".more-btn img").attr("src", src.replace("close.png", "more.png"));
        }
        $(".slider-content").animate({
                marginTop:top
            },
            250
        );
        */
    }
</script>
<?php
function s_cmp($a, $b)
{
    if ($a['sorder'] == $b['sorder']) {
        return 0;
    }
    return ($a['sorder'] < $b['sorder']) ? -1 : 1;
}
?>