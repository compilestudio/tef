<form method="post" action="<?=site_url("super/session/start");?>">
	<fieldset class="login_fieldset pull-right">
		<legend>Log In</legend>
		<div class="control-group">
			<label for="user_name" class="control-label"><?=$user_login;?></label>
			<input type="text" id="user_name" name="user_name" class="span5">
			<label for="password" class="control-label"><?=$user_pass;?></label>
			<input type="password" id="password" name="password" class="span5">
			<div class="span5 button">
				<button type="submit" class="btn btn-primary pull-right">
					<?=$login_button;?>
					<i class="icon-chevron-right icon-white"></i>
				</button>
			</div>
			<?php if($error!=0):?>
			<br><br>
			<div class="alert alert-error">
				<?=$login_error;?>
			</div>
			<?php endif;?>
		</div>
	</fieldset>
</form>