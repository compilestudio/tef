<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
	<head>
		<title><?=$page_title;?></title>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js" type="text/javascript"></script>
		<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
		<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
		<?php echo link_tag('_css/super/super.css');?>
		<?php echo link_tag('_css/backend/jquery.pnotify.default.css');?>
		<script language="javascript" src="<?=base_url();?>_js/backend/tiny_mce/jquery.tinymce.js"></script>
		<script language="javascript" src="<?=base_url();?>_js/backend/bootstrap-modal.js"></script>
		<script src="<?=base_url();?>_js/backend/jquery.pnotify.min.js" type="text/javascript"></script>
		<script language="javascript">
		$(document).ready(function(){
			//Aplicamos el tiny mce al corto
			$('textarea.text_editor4').tinymce({
				// Location of TinyMCE script
				script_url : '<?=base_url();?>_js/tiny_mce/tiny_mce.js',
	
				// General options
				theme : "advanced",
				plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",
	
				// Theme options
				theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,pastetext,pasteword",
				theme_advanced_buttons2 : "formatselect,fontselect,fontsizeselect",
				theme_advanced_buttons3 : "tablecontrols",
				theme_advanced_buttons4 : "bullist,numlist,|,code,|,sub,sup,|,forecolor,backcolor,link,unlink",
				theme_advanced_toolbar_location : "top",
				theme_advanced_toolbar_align : "left",
				theme_advanced_statusbar_location : "bottom",
				theme_advanced_resizing : true,
				force_p_newlines : false,
				force_br_newlines : true,
				forced_root_block : '',
				height:300
			});
			//Aplicamos el tynimce al largo
			$('textarea.text_editor8').tinymce({
				// Location of TinyMCE script
				script_url : '<?=base_url();?>_js/tiny_mce/tiny_mce.js',
	
				// General options
				theme : "advanced",
				plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",
	
				// Theme options
				theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,pastetext,pasteword,|,bullist,numlist,|,code,|,sub,sup,|,forecolor,backcolor",
				theme_advanced_buttons2 : "formatselect,fontselect,fontsizeselect,|,tablecontrols,link,unlink",
				theme_advanced_buttons3 : "",
				theme_advanced_buttons4 : "",
				theme_advanced_toolbar_location : "top",
				theme_advanced_toolbar_align : "left",
				theme_advanced_statusbar_location : "bottom",
				theme_advanced_resizing : true,
				force_p_newlines : false,
				force_br_newlines : true,
				forced_root_block : ''
			});
			//Aplicamos el upload para los files
			$('.file_upload').each(function(index){
				var current = $(this);
				$(this).uploadify({
					'uploader'  : '<?=base_url();?>swf/uploadify.swf',
					'script'    : '<?=site_url("backend/content/upload_file");?>',
					'cancelImg' : '<?=base_url();?>img/cancel.png',
					'folder'    : 'uploads/',
					'auto'      : true,
					'buttonImg' : '<?=base_url();?>img/browse.jpg',
					'height'	: 18,
					'width'		: 60,
					'onComplete': function(event, ID, fileObj, response, data) {
					  	$('.file_text',$(current).parent()).val(response);
					}
				  });
			});
		});
		</script>
	</head>
	<body>
		<div class="navbar navbar-fixed-top">
		<div class="navbar-inner">
			<div class="container">
				<a class="brand" href="#">
					Super administrador
				</a>	
				<ul class="nav pull-right">
					<?php $user=$this->session->userdata('super_user_id');
					if($user):?>
						<li>
							<a href="<?=site_url('super/session/end');?>" class="log_off">
								Sign out
							</a>
						</li>
					<?php endif;?>
				</ul>
			</div>
		</div>
		</div>
		<div class="container main_container">