<script language="javascript">
$(document).ready(function(){
	$('.menu li a .text').on('click',function(){
		var object = $('ul',$(this).parent().parent())[0];
		$(object).slideToggle();
	});
	$('.menu li a .icon-edit').on('click',function(){
		location.href="<?=site_url("super/content/");?>/"+$(this).attr("data-id");
	});
	$('.menu li a .add_new').on('click',function(){
		$('#father').val($(this).attr("data-id"));
		$('#add_new').modal();
	});
	$('.add_new_top').on('click',function(){
		$('#father').val(-1);
	});
	display_menu($('#men<?=$this->uri->segment(3);?>'));
	$('#men<?=$this->uri->segment(3);?>').children('ul').show();
	
	//Agregamos el nuevo contenido
	$('.add_new').on('click',function(){
		if($('#new_name').val()!=''){
			$.post('<?=site_url("super/content/add_content");?>',
				{
					title:$('#new_name').val(),
					father: $('#father').val()
				},
				function(data){
					$('#new_name').val('');
					location.href = "<?=site_url('super/content/');?>/"+data;
				}
			);
		}
	});
});
function display_menu(obj){
	if(obj.length > 0){
		if($(obj).parent('ul').length > 0){
			$(obj).parent('ul').show();
			display_menu($(obj).parent('ul').parent());
		}
	}
}
</script>
<div class="span3">
	<ul class="nav nav-list menu">
		<li>
			<a href="#add_new" role="button" data-toggle="modal" class="add_new_top">
				<?=$add_section;?>
				<i class="icon-plus pull-right add_top"></i>
			</a>
		</li>
	</ul>
	<?=$menu;?>
</div>
<div class="modal hide fade" id="add_new">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    		<h3>Agregar un contenido</h3>
	</div>
	<div class="modal-body">
		<label for="new_name">
			Nombre del contenido
		</label>
    		<input type="text" class="span5" id="new_name" name="new_name" >	
    		<input type="hidden" id="father" name="father" value="-1">
	</div>
	<div class="modal-footer">
		<a href="#" class="btn btn-primary add_new">Agregar</a>
	</div>
</div>