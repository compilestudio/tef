<script language="javascript">
$(document).ready(function(){
	//Cuando actualizamos la información de la sección
	$('.update_section').on('click',function(){
		$.post('<?=site_url("super/content/save_section");?>',
			{
				content_id:<?=$content_id;?>,
				section_id:$('#section_id',$(this).parent().parent()).val(),
				multiple: $('#multiple',$(this).parent().parent()).is(':checked') ? 1 : 0,
				image: $('#image',$(this).parent().parent()).is(':checked') ? 1 : 0,
				width: $('#width',$(this).parent().parent()).val(),
				height: $('#height',$(this).parent().parent()).val(),
				order: $('#order',$(this).parent().parent()).val(),
				description: $('#description',$(this).parent().parent()).val()
			},
			function(){
				$.pnotify({
					text: "La sección ha sido actualizada",
					delay: 3000,
					type: 'success'
				});
		});
	});
	//Eliminamos la sección
	$('.delete_section').on('click',function(){
		if(confirm('Esta seguro que desea eliminar esta sección?')){
			$.post('<?=site_url("super/content/delete_section");?>',{content_id:<?=$content_id;?>,section_id:$('#section_id',$(this).parent().parent()).val(),},function(){
				location.reload();
			});
		}
	});
	//Actualizamos el campo
	$('.update_field').on('click',function(){
		$.post('<?=site_url("super/content/update_field");?>',
			{
				content_id:<?=$content_id;?>,
				section_id:$('#section_id',$(this).parent().parent()).val(),
				name: $('#name',$(this).parent().parent()).val(),
				display_text: $('#display_text',$(this).parent().parent()).val(),
				order_by: $('#order_by',$(this).parent().parent()).is(':checked') ? 1 : 0,
				type: $('#type',$(this).parent().parent()).val(),
				section: $('#section',$(this).parent().parent()).val(),
				max_chars: $('#max_chars',$(this).parent().parent()).val(),
				id: $('#field_id',$(this).parent().parent()).val()
			},
			function(){
				$.pnotify({
					text: "El campo ha sido actualizado",
					delay: 3000,
					type: 'success'
				});
			}
		);
	});
	//Eliminamos el campo
	$('.delete_field').on('click',function(){
		var obj =$(this);
		if(confirm('Esta seguro que quiere eliminar este campo?')){
			$.post('<?=site_url("super/content/delete_field");?>',
				{
					id:$('#field_id',$(this).parent().parent()).val()
					
				},
				function(){
					$.pnotify({
						text: "El campo ha sido eliminado",
						delay: 3000,
						type: 'success'
					});
					$(obj).parent().parent().fadeOut();
				}
			);
		}
	});
	//Eliminamos el contenido
	$('.delete_content').on('click',function(){
		if(confirm('Esta seguro que desea eliminar este contenido?')){
			$.post('<?=site_url("super/content/delete_content");?>',
				{
					id: <?=$content_id;?>
				},
				function(){
					location.href = "<?=site_url("super/home");?>";
				}
			);
		}
	});
	//Agregamos una nueva sección
	$('.add_new_section_').on('click',function(){
		$.post('<?=site_url("super/content/add_content_section");?>',
			{
				content_id:<?=$content_id;?>,
				section_id:$('#section_new').val()
			},function(){
				window.location.reload();
			}
		);
	});
	//Cambiamos el id de la sección para agregar el nuevo campo
	$('.add_field').on('click',function(){
		$('#section_id_').val($(this).attr("data-id"));
	});
	//Agregamos el nuevo campo
	$('.add_new_field_').on('click',function(){
		$.post('<?=site_url("super/content/add_field");?>',
			{
				content_id:<?=$content_id;?>,
				section_id:$('#section_id_',$(this).parent().parent()).val(),
				name: $('#field_name',$(this).parent().parent()).val(),
				display_text: $('#field_description',$(this).parent().parent()).val(),
				order_by: $('#order_by',$(this).parent().parent()).is(':checked') ? 1 : 0,
				type: $('#type',$(this).parent().parent()).val(),
				section: $('#section',$(this).parent().parent()).val(),
				order: $('#order',$(this).parent().parent()).val()
			},function(){
				window.location.reload();
			}
		);
	});
	//Armamos el drag and drop
	$('.field_list').each(function(){
		var obj = $(this);
		$(this).sortable({
			placeholder: "ui-state-highlight",
			handle: ".drag",
			cancel: ".field_list input",
			stop: function( event, ui ) {
				$(".field_list").find("input,select")
					.bind('mousedown.ui-disableSelection selectstart.ui-disableSelection', function(e) {
						e.stopImmediatePropagation();
					});
				$.post('<?=site_url("super/content/update_field_order/".$content_id);?>/'+$(obj).attr("data-sec_id"),$(ui.item).parent().sortable( "serialize" ),function(data){
					
				});
			}
		}).disableSelection();
		$(".field_list").find("input,select")
			.bind('mousedown.ui-disableSelection selectstart.ui-disableSelection', function(e) {
				e.stopImmediatePropagation();
			});
	});
	//Guardamos el cambio de seccion
	$('.section_changer').on('change',function(){
		$.post('<?=site_url("super/content/change_section");?>',{current:$(this).attr("data-current"),new:$(this).val(),content_id:<?=$content_id;?>},function(){
			location.href = "<?=site_url("super/content/".$content_id);?>";
		});
	});
});
</script>
<div class="row content_container">
	<?=$menu;?>
	<div class="span9">
		<h3 class="page span9">
			<?=$content_info->title;?>
			<button type="button" class="btn btn-danger btn-mini pull-right delete_content">
				<i class="icon-trash icon-white"></i>
				Content
			</button>
			<br>
			<a href="#add_section" role="button" data-toggle="modal" class="btn btn-primary btn-mini pull-right add_new_section">
				<i class="icon-plus-sign icon-white"></i>
				Section
			</a>
		</h3>
		<div class="content_description span9" style="border:none;">
			<?=$content_description;?>
		</div>
		<?php if($section_data) 
		foreach($section_data as $sd):?>
			<div class="span9" style="margin: 15px; padding-top: 10px; border-top: 3px groove rgb(0, 0, 0);background:#EEE;">
				<div class="span3">
					<span class="label label-warning">
						<?=$sd->section_id;?>
					</span><br>
					<select id="section" name="section" class="span3 section_changer" data-current="<?=$sd->section_id;?>">
						<?php foreach($sections as $sec):?>
							<option value="<?=$sec->section_id;?>" <?php if($sd->section_id== $sec->section_id) echo "selected";?>>
								<?=$sec->name;?>
							</option>
						<?php endforeach;?>
					</select>
					<input type="hidden" id="section_id" name="section_id" value="<?=$sd->section_id;?>" class="span1" />
				</div>
				<div class="span1">
					<label for="multiple">
						Multiple
					</label>
					<input type="checkbox" id="multiple" name="multiple" <?php if($sd->multiple!=0) echo 'checked="checked"';?> />
				</div>
				<div class="span1">
					<label for="image">
						Image
					</label>
					<input type="checkbox" id="image" name="image" <?php if($sd->image!=0) echo 'checked="checked"';?> />
				</div>
				<div class="span1">
					<label for="width">
						Width
					</label>
					<input type="text" id="width" name="width" value="<?=$sd->width;?>" class="span1" />
				</div>
				<div class="span1">
					<label for="height">
						Height
					</label>
					<input type="text" id="height" name="height" value="<?=$sd->height;?>" class="span1" />
				</div>
				<div class="span1">
					<label for="order">
						Order
					</label>
					<input type="text" id="order" name="order" value="<?=$sd->order;?>" class="span1" />
				</div>
				<div class="span9">
					<label for="order">
						Description
					</label>
					<textarea id="description" name="description" class="span8"><?=$sd->description;?></textarea>
				</div>
				<div class="span3 pull-right" style="padding-top: 15px;">
					<button type="button" class="btn btn-primary update_section">
						Update
					</button>
					<button type="button" class="btn btn-warning delete_section">
						<i class="icon-trash icon-white"></i>
					</button>
					<a href="#add_field" role="button" data-toggle="modal" class="btn btn-info add_field" style="margin-top:5px" data-id="<?=$sd->section_id;?>">
						<i class="icon-trash icon-plus-sign icon-white"></i>
						Field
					</a>
				</div>
			</div>
			<form>
			<div class="field_list" data-sec_id="<?=$sd->section_id;?>">
				<?php foreach($sd->fields as $f):?>
					<div id="field_<?=$f->id;?>" class="span10" style="float: left; border-top: 1px dotted rgb(0, 0, 0); margin: 5px 18px; padding-top: 10px; width: 725px;">
						<div class="span1">
							<label>
								Name
							</label>
							<input type="text" id="name" name="name" value="<?=$f->name;?>" class="span1">
							<span class="label label-warning">
								<?=$f->id;?>
							</span>
							<input type="hidden" id="field_id" name="field_id" value="<?=$f->id;?>" class="span1" />
							<input type="hidden" id="section_id" name="section_id" value="<?=$sd->section_id;?>" class="span1" />
						</div>
						<div class="span2">
							<label>
								Display
							</label>
							<input type="text" id="display_text" name="display_text" value="<?=$f->display_text;?>" class="span2">
						</div>
						<div class="span1">
							<label>
								Order By
							</label>
							<input type="checkbox" id="order_by" name="order_by" <?php if($f->order_by!=0) echo 'checked="checked"';?> >
						</div>
						<div class="span2">
							<label>
								Type
							</label>
							<select id="type" name="type" class="span2">
								<?php foreach($field_types as $ft):?>
									<option value="<?=$ft->id;?>" <?php if($ft->id==$f->type_id) echo "selected='selected'";?>>
										<?=$ft->type;?>
									</option>
								<?php endforeach;?>
							</select>
						</div>
						<div class="span1">
							<label>
								Section
							</label>
							<select id="section" name="section" class="span1">
								<option value="0">None</option>
								<?php foreach($multiple_data as $md):?>
									<option value="<?=$md->id;?>" <?php if($md->id==$f->select_id) echo "selected='selected'";?>>
										<?=$md->value;?>
									</option>
								<?php endforeach;?>
							</select>
						</div>
						<div class="span1">
							<label>
								Max C
							</label>
							<input type="text" id="max_chars" name="max_chars" value="<?=$f->max_chars;?>" class="span1">
						</div>
						<div class="span1" style="margin-top: 5px;">
							<button type="button" class="btn btn-mini btn-success update_field">
								<i class="icon-file icon-white"></i>
							</button>
							<br>
							<button type="button" class="btn btn-mini btn-danger delete_field" style="margin-top:5px">
								<i class="icon-trash icon-white"></i>
							</button>
							<a href='javascript:void(0);' class='drag' data-id='<?=$images->id;?>'>
								<img src='<?=base_url();?>_img/backend/drag.png'>
							</a>
						</div>
					</div>
				<?php endforeach;?>
			</div>
			</form>
		<?php endforeach;?>
		
	</div>
</div>
<div class="modal hide fade" id="add_section">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    		<h3>Agregar una sección</h3>
	</div>
	<div class="modal-body">
		<label for="new_name">
			Sección
		</label>
    		<select id="section_new" name="section_new">
    			<?php if($section_list)
			foreach($section_list as $section):?>
    				<option value="<?=$section->id;?>">
    					<?=$section->name;?>
    				</option>
    			<?php endforeach;?>
    		</select>
	</div>
	<div class="modal-footer">
		<a href="#" class="btn btn-primary add_new_section_">Agregar</a>
	</div>
</div>
<div class="modal hide fade" id="add_field">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    		<h3>Agregar un campo</h3>
	</div>
	<div class="modal-body">
		<label for="new_name" class="span2">
			Nombre
		</label>
		<label for="new_name" class="span3">
			Descripción
		</label>
		<label class="span1">
			Order By
		</label>
		<input type="hidden" id="section_id_" name="section_id_" class="span2">
    		<input type="text" id="field_name" name="field_name" class="span2">
    		<input type="text" id="field_description" name="field_description" class="span3">
    		<div class="pull-right" style="text-align: center; width: 160px;">
			<input type="checkbox" id="order_by" name="order_by" >
		</div>	
		<label class="span2">
			Type
		</label>
		<label class="span3">
			Section
		</label>	
		<label class="span1">
			Order
		</label>
		<select id="type" name="type" class="span2">
			<?php foreach($field_types as $ft):?>
				<option value="<?=$ft->id;?>">
					<?=$ft->type;?>
				</option>
			<?php endforeach;?>
		</select>
		<select id="section" name="section" class="span3">
			<option value="0">None</option>
			<?php foreach($multiple_data as $md):?>
				<option value="<?=$md->id;?>">
					<?=$md->value;?>
				</option>
			<?php endforeach;?>
		</select>
		<input type="text" id="order" name="order" value="" class="span1" style="margin-left: 45px;">			
	</div>
	<div class="modal-footer">
		<a href="#" class="btn btn-primary add_new_field_">Agregar</a>
	</div>
</div>