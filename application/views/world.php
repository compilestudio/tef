<?php $noCustom = 0;;?>
<link rel="stylesheet" href="<?=site_url("_css/front/superslides.css")?>">
<style>
    html{
        overflow-y: auto !important;
    }
</style>
<div class="fullscreen">
    <div class="list">
        <?php
        if(!empty($info))
            usort($info,'cmp_date');
        $g = 0;
        $o = 0;
        if(!empty($info))
        foreach($info as $f):
            $class="back";
            $class2 = "";
            if(!is_numeric($f['row'])):
                $class = $f['row'];
            endif;
            if($class=="fb"):
                $g = 0;
                $o++;
                if($o==4):
                    $o=1;
                endif;
                $class2= $class."_".$o;
            endif;
            if($class=="blog"):
                $o = 0;
                $g++;
                if($g==4):
                    $g=1;
                endif;
                $class2= $class."_".$g;
            endif;
            if(($id==="other" || $id==="dialog") && is_numeric($f['row'])):
                $class2="hide_";
                $noCustom = 1;
            endif;
            if(!(empty($f['title']) && empty($f['description']))):
            ?>
            <?php
            $class3 = "";
            if(!empty($f['image'])):
                $class3 = "hasImage";
            endif;
            ?>
            <div class="item col-xs-6 col-sm-6 col-md-4 col-lg-3 <?=$class3;?>">
                <a href="<?=addhttp($f['link']);?>" target="_blank" title="View  detail">
                    <?php if(!empty($f['image'])):?>
                        <div class="image">
                            <img src="<?=base_url()."uploads/".$f['image'];?>" alt=""/>
                        </div>
                    <?php endif;?>
                    <div class="name" style="height:100%;padding:5%;">
                        <div class="title">
                            <?=strtoupper($f['title']);?>
                        </div>
                        <div class="description">
                            <?php
                            $text = strip_tags($f['description']);
                            if(strlen($text)>255)
                                echo substr($text,0,255)."...";
                            else
                                echo $text;
                            ?>
                        </div>
                    </div>
                </a>
            </div>
        <?php endif;
        endforeach;?>
    </div>
</div>


<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="<?=base_url("_js/superslides/jquery.easing.1.3.js")?>"></script>
<script src="<?=base_url("_js/superslides/jquery.animate-enhanced.min.js")?>"></script>
<script src="<?=base_url("_js/superslides/hammer.min.js")?>"></script>
<script src="<?=base_url("_js/superslides/jquery.superslides.js")?>" type="text/javascript" charset="utf-8"></script>
<script>
var load = 0;
$(function() {
    $('img').on('dragstart', function(event) { event.preventDefault(); });

    $(".more-btn").click(function() {
        $('html, body').animate({
            scrollTop: $(".our-firm").offset().top
        }, 1000);
    });
});
//When the images have finished loading
$(window).load(function() {
    var h = $('.hasImage:first').outerHeight();
    $('.item').outerHeight(h);
    load = 1;
    <?php if($noCustom == 1):?>
    $('.hide_').addClass("hidden");
    <?php endif;?>
});
$( window ).resize(function() {
    if(load==1) {
        $('.hasImage:first').removeAttr("style").removeClass("hidden");
        var h = $('.hasImage:first').outerHeight();
        $('.item').outerHeight(h);
        <?php if($noCustom == 1):?>
        $('.hasImage:first').addClass("hidden");
        <?php endif;?>
    }
});
</script>
<?php
function cmp_date($a, $b){
    if (strtotime($a['date']) == strtotime($b['date'])) {
        return 0;
    }
    return (strtotime($a['date']) < strtotime($b['date'])) ? 1 : -1;
}
?>

