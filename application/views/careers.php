<link rel="stylesheet" href="<?=site_url("_css/front/superslides.css")?>">
<style>
    html{
        overflow-y: auto !important;
    }
</style>

<div class="fullscreen">
    <div class="fullscreen-header">
        <h1 class="projects-title">
            CAREERS
        </h1>
    </div>
    <div class="our-firm">
        <ul class="careers">
            <?php if(!empty($info))
            foreach($info as $in):?>
                <li>
                    <div class="position">
                        <?=$in['position'];?>
                    </div>
                    <div class="description">
                        <?=$in['job_description'];?>
                    </div>
                    <?php
                    if(!empty($in['pdf'])):?>
                        <a href="<?=site_url("careers/download/".$in['pdf']);?>" class="link">
                            Download Job Description
                        </a>
                    <?php endif;?>
                    <?php
                    if(!empty($in['contact'])):
                        echo safe_mailto($in['contact'], 'Contact');
                    endif;?>
                </li>
            <?php endforeach;?>
        </ul>
    </div>
</div>
