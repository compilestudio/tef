<?php echo link_tag('_css/multi-select.css');?>
<script language="javascript" src="<?=base_url();?>_js/jquery.multi-select.js"></script>
<script language="JavaScript">
$(document).ready(function(){
	$('#category_select').multiSelect();
	$('.datepicker').datepicker()
	.on('changeDate', function(ev){
		$(this).datepicker('hide');
	});
	//Time picker
	$('.timepicker').timepicker({
		timeFormat: "g:ia"
	});
	$('textarea.text_editor_').tinymce({
		// Location of TinyMCE script
		script_url : '<?=base_url();?>/_js/tiny_mce/tiny_mce.js',

		// General options
		theme : "advanced",
		plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",

		// Theme options
		theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,pastetext,pasteword,|,bullist,numlist,|,code,|,sub,sup,|,forecolor,backcolor",
		theme_advanced_buttons2 : "formatselect,fontselect,fontsizeselect,|,tablecontrols,link,unlink",
		theme_advanced_buttons3 : "",
		theme_advanced_buttons4 : "",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : true,
		force_p_newlines : false,
		force_br_newlines : true,
		forced_root_block : '',
		width: "700",
		height : "280",
		setup : function(ed) {
			 //peform this action every time a key is pressed
			 ed.onKeyUp.add(function(ed, e) {
			 	//define local variables
			 	var tinylen, htmlcount;
				//grabbing the length of the curent editors content
			 	tinylen = ed.getContent().length;
				//setting up the text string that will display in the path area
			 	htmlcount = "HTML Character Count: " + tinylen;
			 	tinymce.DOM.setHTML(tinymce.DOM.get(tinyMCE.activeEditor.id + '_path_row'), htmlcount);  
			 });
		}
	});
	$('.reject').on('click',function(){
		if(confirm('Are you sure that you want to reject this event?')){
			$.post('<?=site_url("backend/content/event_reject");?>',{event_id:$('#event_id').val()},function(data){
				location.href = '<?=site_url("backend/event_approval/2");?>';
			});
		}
	});
	$('.view_image').on('click',function(){
		if($(this).attr("data-img")!=""){
			$.colorbox({href:"http://www.epicenter-sf.org/uploads/"+$(this).attr("data-img")});
		}
	});
});
</script>
<style type="text/css">
.ms-container ul.ms-list {
    height: 210px;
    overflow-y: auto;
    padding: 0;
    width: 128px;
}
h1{
	margin: 20px 0;
}
.button_section {
    margin-bottom: 10px;
    margin-top: 10px;
    width: 680px;
}
</style>
<div class="container content_container">
<form action="<?=site_url("backend/content/content_save_event");?>" method="post">
	<?=$menu;?>
	<div class="span9">
		<h1 class="span8">Event Info</h1>
		<div class="span4">
			<label for="display_type">
				Date
			</label>
			<div class="input-append date datepicker" data-date="<?php if($info->event_date!=' ') echo $info->event_date; else echo date("Y-m-d");?>" data-date-format="yyyy-mm-dd">
				<input style="margin-bottom: 5px; margin-left: -4px; width: 259px;" readonly="readonly" id="event_date" name="event_date" class="span3" size="16" type="text" value="<?php if($info->event_date!=' ') echo $info->event_date; else echo date("Y-m-d");?>">
				<span class="add-on"><i class="icon-th"></i></span>
			</div>
			<input type="hidden" value="57" name="event_date_id" id="event_date_id">
		</div>
		<div class="span4 pull-right">
			<label for="parent_category_">
				Main category 						
			</label>
			<select class="span4" id="parent_category_" name="parent_category_">
				<option value="-1">Select a parent category </option>
				<?php foreach($categories as $cat):?>
					<option value="<?=$cat->id;?>" <?php if($info->category==$cat->id) echo "selected";?>><?=$cat->name;?></option>
				<?php endforeach;?>
			</select>
		</div>
		<div class="span4">
			<label for="parent_category_">
				 Name of Event  						
			</label>
			<input type="text" class="span4" value="<?=$info->name;?>" name="section_title" id="section_title">
			<label for="display_type">
				 Location of Event 
			</label>
			<input type="text" value="<?=$info->location;?>" class="span4" name="location" id="location">
			<input type="hidden" value="60" name="location_id" id="location_id">
			<div class="row">
				<!-- Ponemos el label-->
				<label for="start_time" class="span2">
					Start Time
				</label>
				<!-- Ponemos el label-->
				<label for="start_time" class="span2">
					End Time
				</label>
			</div>
				<!-- Vemos que tipo de input es-->
				<input class="timepicker span2" type="text" id="start_time" name="start_time" readonly="readonly" value="<?=$info->start_time;?>">
				<input type="hidden" value="58" name="start_time_id" id="start_time_id">
			<div class="pull-right">
				<!-- Vemos que tipo de input es-->
				<input class="timepicker span2" type="text" id="end_time" name="end_time" readonly="readonly" value="<?=$info->end_time;?>">
				<input type="hidden" value="59" name="end_time_id" id="end_time_id">
			</div>
			<div class="row">
				<label for="website" class="span4">
					Event Website																	
				</label>
			</div>
			<input type="text" value="<?=$info->website;?>" class="span4" name="website" id="website">
			<input type="hidden" value="62" name="website_id" id="website_id">	
		</div>
		<div class="detail_title span4 pull-right">Secondary Categories</div>
		<div class="span4 pull-right">
			<select multiple="multiple" class="span4" name="category_select[]" id="category_select">
				<?php if($categories) 
						foreach($categories as $parent):?>
						<option <?php if(isset($categories_[$parent->id])) echo "selected";?> value="<?=$parent->id;?>"><?=$parent->name;?></option>
				<?php endforeach;?>
			</select>
			<input type="hidden" id="v4" name="v4" value="1" />
		</div>
		<h1 class="span8">
			Photos  
		</h1>
		<div class="span8">
			<label for="image1">
				<i style="cursor:pointer;" data-img="<?=$info->image;?>" class="icon-picture view_image"></i>
				Event Image (186w x181t Thumbnail) 																	
			</label>
			<input type="text" id="image1" name="image1" class="span3 file_text" value="<?=$info->image;?>" readonly>
			<input type="file" id="image1_file" name="image1_file" class="file_upload">
			<div class="clear_file" data-id="image1">
				<i class="icon-remove"></i>
			</div>
			<input type="hidden" value="66" name="image1_id" id="image1_id">
		</div>
		<div class="span8">
			<label for="image3">
				<i style="cursor:pointer;" data-img="<?=$info->image3;?>" class="icon-picture view_image"></i>
				Large Event Image (770 pixels wide maximum)											
			</label>
			<input type="text" readonly="" value="<?=$info->image3;?>" class="span3 file_text" name="big_image" id="big_image">
			<input width="75" type="file" height="29" class="file_upload" name="big_image_file" id="big_image_file">
			<input type="hidden" value="85" name="big_image_id" id="big_image_id">
		</div>
		<h1 class="span8">
			Description  
		</h1>
		<div class="span8" style="width: 680px;">
			<label for="where">
				Event Information																	
			</label>
			<textarea class="span8 text_editor_" name="information" id="information"><?=$info->information;?></textarea>
			<input type="hidden" value="63" name="information_id" id="information_id">
		</div>	
		<div class="span8" style="width: 680px;">
			<label for="what">
				  Sponsor/Organization Information 																
			</label>
			<textarea class="span8 text_editor_" name="host" id="host"><?=$info->host;?></textarea>
			<input type="hidden" value="61" name="host_id" id="host_id">
		</div>	
				
		<input type="hidden" value="20" name="father_id" id="father_id">
		<input type="hidden" value="6" name="id" id="id">				
		<div class="span8 button_section">
			<button type="submit" class="btn btn-primary pull-right">
				Approve
				<i class="icon-chevron-right icon-white"></i>
			</button>
			<div class="pull-right">&nbsp;&nbsp;</div>
			<button type="button" class="btn btn-danger pull-right reject">
				Reject
				<i class="icon-remove icon-white"></i>
			</button>
		</div>							
		<input type="hidden" class="span6" value="Dates" name="content_title_label" id="content_title_label">
		<input type="hidden" class="span2" value="1" name="content_order" id="content_order">
		<input type="hidden" class="span2" value="-1" name="section_list" id="section_list">
		<input type="hidden" class="span2" value="<?=$event_id;?>" name="event_id" id="event_id">		
	</div>
</form>
</div>