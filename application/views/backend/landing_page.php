<form method="post" action="<?=site_url("backend/session/start");?>">
	<div class="row_">
		<div class="left_column">
			&nbsp;
		</div>
		<div class="right_column">
			<fieldset class="login_fieldset pull-right span9">
				<legend><?=$login_button;?></legend>
				<div class="control-group">
					<label for="user_name" class="control-label"><?=$user_login;?></label>
					<input type="text" id="user_name" name="user_name" class="span5">
					<label for="password" class="control-label"><?=$user_pass;?></label>
					<input type="password" id="password" name="password" class="span5">
					<div class="button">
						<button type="submit" class="btn btn-primary pull-right">
							<?=$login_button;?>
						</button>
					</div>
					<?php if($error!=0):?>
					<br><br>
					<div class="alert alert-error">
						<?=$login_error;?>
					</div>
					<?php endif;?>
				</div>
			</fieldset>
		</div>
	</div>
</form>