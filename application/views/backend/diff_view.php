<div class="container">
	<?php foreach($fields as $field):?>
		<div class="row">
			<h3 class="span12 alert alert-info" style="padding: 5px; width: 920px;">
				<?=$field->display_text;?>
			</h3>
		</div>
		<div class="row">
			<div class="span6" style="border-right: 1px solid rgb(204, 204, 204); padding-right: 5px; width: 454px;">
				<?php 
				if(in_array($field->type_id,array(1,2,4,5,6,8,9,10,12,20,21,24,25,27))):
					echo $field->content_live;
				elseif(in_array($field->type_id,array(3,7,11))):
					if($field->content_live!=''):
						$ext = pathinfo($field->content_live, PATHINFO_EXTENSION);
						if(in_array($ext, array('jpg','gif','png'))):
							echo "<img src='".base_url()."uploads/".$field->content_live."'>";
						else:
							echo "<a href='".base_url()."uploads/".$field->content_live."' target='_blank'>
									<img src='".base_url()."_img/backend/document.png'>
								</a>";
						endif;
					endif;
				elseif($field->type_id==28):
					echo "<ul class='files'>";
					if($field->files_live)
					foreach($field->files_live as $file):
						$ext = pathinfo($file->img, PATHINFO_EXTENSION);
						echo "<li>";
							if(in_array($ext, array('jpg','gif','png'))):
								echo "<img src='".base_url()."uploads/".$file->img."'>";
							else:
								echo "<a href='".base_url()."uploads/".$file->img."' target='_blank'>
										<img src='".base_url()."_img/backend/document.png'>
									</a>";
							endif;
							echo $file->caption;
						echo "</li>";
					endforeach;
					echo "</ul>";
				endif;
				?>
			</div>
			<div class="span6">
				<?php 
				if(in_array($field->type_id,array(1,2,4,5,6,8,9,10,12,20,21,24,25,27))):
					echo $field->content_draft;
				elseif(in_array($field->type_id,array(3,7,11))):
					if($field->content_draft!=''):
						$ext = pathinfo($field->content_draft, PATHINFO_EXTENSION);
						if(in_array($ext, array('jpg','gif','png'))):
							echo "<img src='".base_url()."uploads/".$field->content_draft."'>";
						else:
							echo "<a href='".base_url()."uploads/".$field->content_draft."' target='_blank'>
									<img src='".base_url()."_img/backend/document.png'>
								</a>";
						endif;
					endif;
				elseif($field->type_id==28):
					echo "<ul class='files'>";
					if($field->files_draft)
					foreach($field->files_draft as $file):
						$ext = pathinfo($file->img, PATHINFO_EXTENSION);
						echo "<li>";
							if(in_array($ext, array('jpg','gif','png'))):
								echo "<img src='".base_url()."uploads/".$file->img."'>";
							else:
								echo "<a href='".base_url()."uploads/".$file->img."' target='_blank'>
										<img src='".base_url()."_img/backend/document.png'>
									</a>";
							endif;
							echo $file->caption_d;
						echo "</li>";
					endforeach;
					echo "</ul>";
				endif;
				?>
			</div>
		</div>
	<?php endforeach;?>
</div>