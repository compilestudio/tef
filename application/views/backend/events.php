<script src="<?=base_url();?>_js/backend/bootstrap-transition.js" type="text/javascript"></script>
<script language="JavaScript">
	$(document).ready(function(){
		$('.a_approve_modal').on('click',function(){
			$.post('<?=site_url("news_resources/get_detail_event/");?>',{id:$(this).attr("data-id")},function(data){
				if(data!=null){
					$("#event_id").val(data.id);
					$("#title").val(data.title);
					$("#location_address").val(data.location_address);
					$("#registration_url").val(data.registration_url);
					$("#date").val(data.date);
					$("#start_time").val(data.start_time);
					$("#end_time").val(data.end_time);
					$("#message").val(data.message);
				}else{
					$("#event_id").val("");
					$("#title").val("");
					$("#location_address").val("");
					$("#registration_url").val("");
					$("#date").val("");
					$("#start_time").val("");
					$("#end_time").val("");
					$("#message").val("");					
				}
			});
		});
		$('#approve_').on('click',function(){				
			$.post('<?=site_url("news_resources/approve_event/");?>',{id:$('#event_id').val()},function(data){			
				$("#row_"+$('#event_id').val()).fadeOut().remove();
				$.pnotify({
					text: "Approved event successfully",
					delay: <?=$success_notification_delay;?>,
					type: 'success'
				});		
				$("#approve_modal").modal('hide');
			});
		});

	});
	function delete_row(row){
	    $(row).parent().parent().parent().fadeOut().remove();
	}
</script>
<div class="row_">
	<div class="left_column">
		<?=$menu;?>
	</div>
	<div class="right_column">
<!--Titulo del contenido-->
		<div class="headline">
			<?=$content_info->title;?>
			<a href="javascript:void(0);" class="collapse_">
				<?=$btn_collapse;?>
			</a>
			<a href="javascript:void(0);" class="expand">
				<?=$btn_expand;?>
			</a>
		</div>

		<table class="table table-striped">
			<thead>
				<tr>
					<th>Created</th>
					<th>Email</th>
					<th>Title</th>
					<th>Date</th>
					<th>Attachment</th>
					<th>
						Options 
					</th>
				</tr>
			</thead>

			<tbody>
				<?php if(isset($events) && !empty($events)): 
				foreach($events as $row): 
					if($row->approved == "N"):?>
					<tr id="row_<?=$row->id;?>">
						<td>
							<?=$row->created;?>
						</td>
						<td>
							<?=$row->email;?>
						</td>
						<td>
							<?=$row->title;?>
						</td>
						<td>
							<?=$row->date;?>
						</td>																	
						<td>
							<a href="<?=base_url("uploads/".$row->attachment);?>" target="_blank" download="<?=base_url("uploads/".$row->attachment);?>" class="btn btn-mini">
								<i class="icon icon-download-alt"></i>
							</a>
						</td>
						<td>
							<a href="#approve_modal"  role="button" data-toggle="modal" class="btn btn-mini a_approve_modal" data-id="<?=$row->id;?>">
								<i class="icon icon-ok"></i> Approve
							</a>
						</td>						
					</tr>
				<?php endif; endforeach; endif;?>
			</tbody>
		</table>

	</div>
</div>



<!--------------------------------------Modal para el rechazo de los cambios------------------------------>
<div class="modal hide fade" id="approve_modal">
	<input type="hidden" id="event_id" value="0">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		Approve event
  	</div>
  	<div class="modal-body">
		
		<form class="form-horizontal">
		  <div class="control-group">
		    <label class="control-label" for="title">Title</label>
		    <div class="controls">
		      <input type="text" readonly="readonly" id="title" placeholder="Title">
		    </div>
		  </div>
		  <div class="control-group">
		    <label class="control-label" for="location_address">Location Address</label>
		    <div class="controls">
		      <input type="text" readonly="readonly" id="location_address" placeholder="Location Address">
		    </div>
		  </div>
		  <div class="control-group">
		    <label class="control-label" for="registration_url">Registration URL</label>
		    <div class="controls">
		      <input type="text" readonly="readonly" id="registration_url" placeholder="Registration URL">
		    </div>
		  </div>
		  <div class="control-group">
		    <label class="control-label" for="date">Date</label>
		    <div class="controls">
		      <input type="text" readonly="readonly" id="date" placeholder="Date">
		    </div>
		  </div>
		  <div class="control-group">
		    <label class="control-label" for="start_time">Start Time</label>
		    <div class="controls">
		      <input type="text" readonly="readonly" id="start_time" placeholder="Start Time">
		    </div>
		  </div>
		  <div class="control-group">
		    <label class="control-label" for="end_time">End Time</label>
		    <div class="controls">
		      <input type="text" readonly="readonly" id="end_time" placeholder="End Time">
		    </div>
		  </div>
		  <div class="control-group">
		    <label class="control-label" for="message">Message</label>
		    <div class="controls">
		    	<textarea rows="3"  readonly="readonly" id="message" ></textarea>
		    </div>
		  </div>			  	  
		</form>

	</div>
	<div class="modal-footer">
		<button class="btn btn-success btn-primary pull-right" type="button" id="approve_" name="Approve">
			Approve
		</button>
	</div>
</div>

<div class="modal hide fade" id="reject_modal">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
		Reject event
  	</div>
  	<div class="modal-body">

	</div>
	<div class="modal-footer">
		<button class="btn btn-primary btn-danger pull-right" type="button" id="reject_" name="reject_">
			<?=$reject_btn;?>
		</button>
	</div>
</div>