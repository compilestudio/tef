<div class="row_">
	<div class="left_column">
		<?=$menu;?>
	</div>
	<div class="right_column">
        <h4>
            You can delete the stories that are offensive or are spam here.
        </h4>
        <div class="row">
            <div class="span4">
                <input type="text" class="span4" placeholder="Search" id="search">
            </div>
        </div>
        <style>
            th,td{
                font-size: 12px;
            }
        </style>
        <table class="table table-striped table-bordered" width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <th>
                    Name
                </th>
                <th>
                    Email
                </th>
                <th>
                    City
                </th>
                <th>
                    County
                </th>
                <th>
                    Story
                </th>
                <th>
                    Type
                </th>
                <th>
                    &nbsp;
                </th>
            </tr>
            <tbody id="fbody">
            <?php if(!empty($stories))
            foreach($stories as $s):?>
                <tr id="row_<?=$s->id;?>">
                    <td>
                        <?=$s->name;?>
                    </td>
                    <td>
                        <?=$s->email;?>
                    </td>
                    <td>
                        <?=$s->city;?>
                    </td>
                    <td>
                        <?=$s->county;?>
                    </td>
                    <td>
                        <?=$s->story;?>
                    </td>
                    <td>
                        <?php
                        switch($s->type):
                            case 1:
                                echo "Live";
                                break;
                            case 2:
                                echo "Work";
                                break;
                            case 3:
                                echo "Play";
                                break;
                        endswitch;?>
                    </td>
                    <td>
                        <button class="btn btn-danger delete" id="<?=$s->id;?>">
                            <i class="icon-trash icon-white"></i>
                        </button>
                    </td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
	</div>
</div>
<script>
$.expr[":"].contains = $.expr.createPseudo(function(arg) {
    return function( elem ) {
        return $(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
    };
});
$(document).ready(function(){
    $('.delete').on('click',function(){
        if(confirm("Do you want to delete this story?")){
            $.post('<?=site_url("backend/content/delete_story");?>',{id:$(this).attr("id")},function(data){
                $('#row_'+data).fadeOut();
            });
        }
    });
    $("#search").keyup(function () {
        //split the current value of searchInput
        var data = this.value.split(" ");
        //create a jquery object of the rows
        var jo = $("#fbody").find("tr");
        if (this.value == "") {
            jo.show();
            return;
        }
        //hide all the rows
        jo.hide();

        //Recusively filter the jquery object to get results.
        jo.filter(function (i, v) {
            var $t = $(this);
            for (var d = 0; d < data.length; ++d) {
                if ($t.is(":contains('" + data[d] + "')")) {
                    return true;
                }
            }
            return false;
        })
            //show the rows that match.
            .show();
    });
});
</script>