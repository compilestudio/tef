<?php 
function cmp($a, $b){
    if (strtolower($a->title) == strtolower($b->title)) {
        return 0;
    }
    return (strtolower($a->title) < strtolower($b->title)) ? -1 : 1;
}
usort($location,'cmp');
?>
<?php echo link_tag('_css/multi-select.css');?>
<?php echo link_tag('_css/fonts.css');?>
<script language="javascript" src="<?=base_url();?>_js/jquery.multi-select.js"></script>
<script language="JavaScript">
$(document).ready(function(){
	$('#form1').validationEngine({validationEventTrigger:'submit'});
	$('.delete').on('click',function(){
		if(confirm('Do you want to delete this Event?')){
			$.post('<?=site_url("backend/event_approval/delete_event");?>',{id:$('#event').val()},function(){
				location.href='<?=site_url("backend/event_approval/create");?>';
			});
		}
	});
	$('#category_select').multiSelect();
	$('.datepicker').datepicker()
	.on('changeDate', function(ev){
		$(this).datepicker('hide');
	});
	//Time picker
	$('.timepicker').timepicker({
		timeFormat: "g:ia"
	});
	$('textarea.text_editor_').tinymce({
		// Location of TinyMCE script
		script_url : '<?=base_url();?>/_js/tiny_mce/tiny_mce.js',

		// General options
		theme : "advanced",
		plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",

		// Theme options
		theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,pastetext,pasteword,|,bullist,numlist,|,code,|,sub,sup,|,forecolor,backcolor",
		theme_advanced_buttons2 : "formatselect,fontselect,fontsizeselect,|,tablecontrols,link,unlink",
		theme_advanced_buttons3 : "",
		theme_advanced_buttons4 : "",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : true,
		force_p_newlines : false,
		force_br_newlines : true,
		forced_root_block : '',
		width: "700",
		height : "280",
		setup : function(ed) {
			 //peform this action every time a key is pressed
			 ed.onKeyUp.add(function(ed, e) {
			 	//define local variables
			 	var tinylen, htmlcount;
				//grabbing the length of the curent editors content
			 	tinylen = ed.getContent().length;
				//setting up the text string that will display in the path area
			 	htmlcount = "HTML Character Count: " + tinylen;
			 	tinymce.DOM.setHTML(tinymce.DOM.get(tinyMCE.activeEditor.id + '_path_row'), htmlcount);  
			 });
		}
	});
	$('textarea.text_editor_small').tinymce({
		// Location of TinyMCE script
		script_url : '<?=base_url();?>/_js/tiny_mce/tiny_mce.js',

		// General options
		theme : "advanced",
		plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",

		// Theme options
		theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,bullist,numlist,backcolor",
		theme_advanced_buttons2 : "code,|,sub,supformatselect,fontselect,fontsizeselect,|,forecolor",
		theme_advanced_buttons3 : "",
		theme_advanced_buttons4 : "",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : true,
		force_p_newlines : false,
		force_br_newlines : true,
		forced_root_block : '',
		width: "300",
		height : "300",
		setup : function(ed) {
			 //peform this action every time a key is pressed
			 ed.onKeyUp.add(function(ed, e) {
			 	//define local variables
			 	var tinylen, htmlcount;
				//grabbing the length of the curent editors content
			 	tinylen = ed.getContent().length;
				//setting up the text string that will display in the path area
			 	htmlcount = "HTML Character Count: " + tinylen;
			 	tinymce.DOM.setHTML(tinymce.DOM.get(tinyMCE.activeEditor.id + '_path_row'), htmlcount);  
			 });
		}
	});
	$('.view_image').on('click',function(){
		if($(this).attr("data-img")!=""){
			$.colorbox({href:"http://www.epicenter-sf.org/uploads/"+$(this).attr("data-img")});
		}
	});
	$("#add_date").on('click',function(){
		$('#dates').clone().appendTo('#date_space');
		$('.timepicker').timepicker({
			timeFormat: "g:ia"
		});
		$('.datepicker').datepicker();
	});
	$(".remove_row").live('click',function(){
		if($("#date_space #dates").length!=1){
			$(this).parent().remove(); 
		}else{
			alert("You can't remove anymore dates!");
		}
	});
	$('#event').on('change',function(){
		if($(this).val()!=-1){
			window.location.href='<?=site_url("backend/event_approval/create/");?>/'+$(this).val();
		}else{
			window.location.href='<?=site_url("backend/event_approval/create/");?>/';
		}
	});
	$('.clear').on('click',function(){
		window.location.reload();
	});
	$('.radio_').on('click',function(){
		var val = $(this).val();
		if (val ==1){
			$('#main_title').html("Single Date Event");
			$('.op2,.op3').hide();
			$('.op1').fadeIn();
		}
		if (val ==2){
			$('#main_title').html("Multiple Date Event");
			$('.op1,.op3').hide();
			$('.op2').fadeIn();
		}
		if (val ==3){
			$('#main_title').html("Repeating Event");
			$('.op2,.op1').hide();
			$('.op3').fadeIn();
		}
	});
	$('.radio_:checked').click();
	$('.clear_file').on('click',function(){
		if(confirm('Do you want to delte this image?')){
			$('.file_text',$(this).parent()).val('');
		}
	});
	//Preview
	$('.preview').on('click',function(){
		if($('#recurance:checked').val() ==2){
			if($('#start_date_mul').val()!=''){
				date = $('#start_date_mul').val();
				date2 = $('#end_date_mul').val();
			}else{
				date = $('#dates #event_date:eq(0)').val();
				date2 = "";
			}
		}
		else if($('#recurance:checked').val() ==3){
			date = $('#event_date').val();
			date2 = $('#end_date').val();
		}else{
			date = $('#event_date').val();
			date2 = "";
		}
		
		$.post('<?=site_url("calendar/convert_date");?>',{date:date,date2:date2},function(data){
			//Fechas Inicio y fin
			$('#date_').html(data);
			
			if($('#recurance:checked').val() ==2){
				if($('.start_time:eq(1)').val()!=""){
					$('.description').html($('.start_time:eq(1)').val()+"-"+$('.end_time:eq(1)').val());	
				}else{
					$('.description').html($('.start_time:eq(2)').val()+"-"+$('.end_time:eq(2)').val());
				}
				
			}else{
				$('.description').html($('#start_time').val()+"-"+$('#end_time').val());
			}
			
			//Event name
			$('.hours').html($('#section_title').val());
			
			//Encabezado de la categorías
			$('#headline').html($('#parent_category_ option:selected').text());
			
			//Organization
			$('.organization').html($('#organization').val());
			//Website
			$('#website_').html($('#website').val());
			$('#website_').attr('href',$('#website').val());
			//What information
			$('#what').html($('#information').val());
			//Who Iinformation
			$('#who').html($('#host').val());
			
			//Where INformation 
			$('#where').html($('#other_location').val()+"<br>"+$('#venue_street').val()+"<br>San Francisco, CA "+$('#zip_code').val()+"<br><br>"+$('#extra_directions').val());
			$('#big_image_').attr('src',"<?=base_url();?>uploads/"+$('#big_image').val());
			
			//Ponemos el color
			if($('#parent_category_').val()==1){
				$('.down_arrow,.detail').removeClass("blue pink red orange brown green");
				$('.down_arrow,.detail').addClass("blue");				
			}
			if($('#parent_category_').val()==2){
				$('.down_arrow,.detail').removeClass("blue pink red orange brown green");
				$('.down_arrow,.detail').addClass("pink");				
			}
			if($('#parent_category_').val()==3){
				$('.down_arrow,.detail').removeClass("blue pink red orange brown green");
				$('.down_arrow,.detail').addClass("orange");				
			}
			if($('#parent_category_').val()==4){
				$('.down_arrow,.detail').removeClass("blue pink red orange brown green");
				$('.down_arrow,.detail').addClass("red");				
			}
			if($('#parent_category_').val()==5){
				$('.down_arrow,.detail').removeClass("blue pink red orange brown green");
				$('.down_arrow,.detail').addClass("brown");				
			}
			if($('#parent_category_').val()==6){
				$('.down_arrow,.detail').removeClass("blue pink red orange brown green");
				$('.down_arrow,.detail').addClass("green");				
			}
			$('.modal2').modal();
		});
	});
	//New options
	$('#select_organization').on('change',function(){
		window.location.href = '<?=site_url('backend/event_approval/create');?>/'+$('#event').val()+"/"+$(this).val();
	});
	$('#event').on('change',function(){
		window.location.href = '<?=site_url('backend/event_approval/create');?>/'+$(this).val()+'/'+$('#select_organization').val();
	});
	$('#based_on').on('change',function(){
		window.location.href = '<?=site_url('backend/event_approval/create');?>/'+$('#event').val()+'/'+$('#select_organization').val()+"/"+$(this).val();
	});
	$('#location').on('change',function(){
		$.post('<?=site_url("add_event/venue");?>',{id:$(this).val()},function(data){
			if(data==0){
				$('#other_location').val("");
				$('#venue_street').val("");
				$('#zip_code').val("");
			}else{
				$('#other_location').val(data.name);
				$('#venue_street').val(data.street);
				$('#zip_code').val(data.zip);
				$('#venue_category option[value="'+data.category+'"]').attr('selected', 'selected');
			}
		});
	});
	$('#location').change();
	//Guardamos la organizacion
	$('.save_org').on('click',function(){
		$.post('<?=site_url("backend/event_approval/update_org");?>',{organization:$('#organization').val(),name:$('#name').val(),phone:$('#phone').val(),email:$('#email').val(),id:$('#select_organization').val()},function(data){
			$('.org_saved').fadeIn();
		});
	});
	$('.delete_org').on('click',function(){
			<?php if(isset($ev_org) && $ev_org[0]->quatity==0):?>
			if(confirm('Are you sure that you want to delete this organization?')){
				$.post('<?=site_url("backend/event_approval/delete_org");?>',{id:$('#select_organization').val()},function(){
					location.href="<?=site_url("backend/event_approval/create");?>";
				});
			}
			<?php else:?>
			alert("There are events assocaited with this organization. Only organizations that do not have events tagged to them can be deleted");
			<?php endif;?>
	});
	//Multiple rows
	$('#add_repeating_date').on('click',function(){
		$('#repeating_date').clone().appendTo('#repeating_dates');
		$('.timepicker').timepicker({
			timeFormat: "g:ia"
		});
		$('.datepicker').datepicker();
	});
	$(".remove_row_multiple").live('click',function(){
		if($("#repeating_dates #repeating_date").length!=1){
			$(this).parent().remove(); 
		}else{
			alert("You can't remove anymore dates!");
		}
	});
});
</script>
<style type="text/css">
.modal{
	font-family:Share-regular;
}
.ms-container ul.ms-list {
    height: 210px;
    overflow-y: auto;
    padding: 0;
    width: 128px;
}
.button_section {
    margin-bottom: 10px;
    margin-top: 10px;
    width: 680px;
}
h1 {
    border-bottom: medium none;
    border-top: 5px solid #000000;
    font-size: 24px;
    font-weight: normal;
    line-height: 36px;
    margin-top: 50px;
    padding-top: 5px;
}
h3 {
    border-top: 1px solid #000000;
    font-size: 16px;
    font-weight: bold;
    margin-bottom: 20px;
    margin-left: 0 !important;
    margin-top: 30px !important;
    padding-bottom: 15px;
    padding-top: 10px;
    text-transform: capitalize;
    width: 700px;
}
.pink, label{
	color:#FF00FF;
}
.pink {
    margin-left: 10px;
}
ul.recurrence {
    list-style: none outside none;
    margin: 0 0 0 -20px;
    padding: 0;
}
ul.recurrence li {
    font-size: 20px;
    margin-bottom: 15px;
}
.btn-info,.btn-info:hover {
    background-color: #fe01fa;
    background-image: none;
    border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
}
.btn-success,.btn-success:hover {
    background-color: #8a7a6b;
    background-image: none;
    border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
}
.op2,.op3,.op1{
	display:none;
}
.op3,.op2 {
	float:left;
    margin-top: 30px;
}
.description_text {
    color: #9C8D7E;
    float: right;
    font-family: Share-regular;
    font-size: 14px;
    width: 360px;
}
/*Preview*/
.container .calendar_text_container {
    float: left;
    width: 100%;
    min-height:700px;
}
.container .upcoming_events {
    background: none repeat scroll 0 0 #000000;
    color: #FFFFFF;
    float: left;
    height: 38px;
    margin-bottom: 10px;
    margin-top: 18px;
    padding: 7px 10px 12px;
    position: relative;
    width: 964px;
}
.container .calendar_text_container .left_column {
    float: left;
    width: 190px;
}
.container .calendar_text_container .left_column .down_arrow {
    color: #FFFFFF;
    float: left;
    height: 181px;
    position: relative;
    width: 186px;
    z-index: 99;
}
.container .calendar_text_container .left_column .down_arrow .time {
    float: left;
    font-size: 20px;
    margin-left: 15px;
    margin-top: 15px;
    width: 150px;
}
.hours {
    float: left;
    font-size: 16px;
    margin-top: 10px;
    width: 100%;
}
.organization {
    float: left;
    font-size: 12px;
    margin-top: 10px;
    width: 100%;
}
.container .calendar_text_container .left_column .down_arrow .description {
	border:none;
	padding:0;
    float: left;
    font-size: 14px;
    margin-top: 5px;
    width: 140px;
}
.container .upcoming_events .top {
    float: left;
    font-size: 47px;
    margin-top: 13px;
}
.container .calendar_text_container .right_column.detail {
    float: left;
    margin-left: 20px;
    width: 770px;
}
.right_column .detail_title {
    color: #21BFF0;
    float: left;
    font-size: 18px;
    margin-bottom: 5px;
    width: 100%;
}
.container .calendar_text_container .right_column .text {
    color: #262626;
    float: left;
    font-family: arial;
    font-size: 12px;
    line-height: 15px;
    margin-bottom: 25px;
}
.down_arrow.blue {
    background:#21bff0;
  }
  .down_arrow.red {
    background:#b5121b;
  }
  .down_arrow.pink {
    background:#ff00ff;
  }
  .down_arrow.orange {
    background:#f47735;
  }
  .down_arrow.brown {
    background:#3e7c94;
  }
  .down_arrow.green {
    background:#c1cd23;
  }
 .right_column.blue > .detail_title {
      color:#21bff0;
    }
    .right_column.pink > .detail_title {
      color:#ff00ff;
    }
    .right_column.red > .detail_title {
      color:#b5121b;
    }
    .right_column.orange > .detail_title {
      color:#f47735;
    }
    .right_column.brown > .detail_title {
      color:#3e7c94;
    }
    .right_column.green > .detail_title {
      color:#c1cd23;
    }
    .right_column.detail {
      width:770px;
    }
select.org{
	width:695px;
}
.instructions {
	color:#000;
    margin-bottom: 15px;
}
.radio_container{
    float: left;
    width: 15px;
}
</style>
<div class="container content_container">
<form action="<?=site_url("backend/event_approval/add");?>" method="post" id="form1">
	<?=$menu;?>
	<div class="span9 event_detail_">
		<?php $saved = $this->uri->segment(6);
		if($saved):
		?>
		<div class="span6 alert alert-success">
			<?=$description['event_saved'];?>
		</div>
		<?php endif;?>
		<h3 class="span9">
			Contact Info
		</h3>
		<div class="span9 instructions">
			Please enter the contact information for the person we can reach if there are any issues or concerns with your event posting.
		</div>
		<div class="row row2">
			<div class="span8">
				<label for="select_organization">
					Organization/Company
				</label>
				<select id="select_organization" name="select_organization" class="org">
					<option value="0">Not listed</option>
					<?php foreach($organizations as $org):?>
						<option value="<?=$org->id;?>" <?php if(isset($orga) && $orga->id==$org->id) echo "selected";?>>
							<?=$org->organization;?>
						</option>
					<?php endforeach;?>
				</select>
			</div>
			<div class="span4">
				<label for="parent_category_">
					 Organization/Company (60 char. max)
				</label>
				<input type="text" class="span4 validate[required]" value="<?php if(isset($orga)) echo $orga->organization;?>" name="organization" id="organization">
				<label for="parent_category_">
					 Name
				</label>
				<input type="text" class="span4 validate[required]" value="<?php if(isset($orga)) echo $orga->name;?>" name="name" id="name">
			</div>
			<div class="span4 pull-right">
				<label for="parent_category_">
					 Phone
				</label>
				<input type="text" class="span4 validate[required]" value="<?php if(isset($orga)) echo $orga->phone;?>" name="phone" id="phone">
				<label for="parent_category_">
					 Email
				</label>
				<input type="text" class="span4 validate[required]" value="<?php if(isset($orga)) echo $orga->email;?>" name="email" id="email">
			</div>
			<?php if(isset($orga)):?>
				<div class="span9" style="margin-top: 15px; margin-left: 20px;">
					<button type="button" class="btn btn-info pull-right save_org">
						Update
						<i class="icon-chevron-right icon-white"></i>
					</button>
					<button type="button" class="btn btn-warning pull-right delete_org" style="margin-right: 10px;">
						<i class="icon-remove-sign icon-white"></i>
						Delete
					</button>
				</div>
			<?php endif;?>
			<div class="span alert alert-success hide org_saved">
			  	<button type="button" class="close" data-dismiss="alert">&times;</button>
				<strong>The organization information it's updated</strong>
			</div>
		</div>
<!-----------------------Event General info-->
		<h3 class="span9">
			Event General Info
		</h3>
		<div class="span9 instructions">
			<?=$description['add_general'];?>
		</div>
		<div class="row row2">
			<div class="span8">
				<label for="event">
					 Select an Event  						
				</label>
				<select id="event" name="event" class="span9">
					<option value="0">Create New Event</option>
					<?php foreach($events as $ev):
					?>
						<option value="<?=$ev->id;?>" <?php if(isset($info) && $info->id==$ev->id) echo "selected";?>>
							<?=$ev->event_name;?>
						</option>	
					<?php endforeach;?>
				</select>
			</div>
			<div class="span4 pull-right">
				<label for="parent_category_">
					Main category 						
				</label>
				<select class="span4 validate[required]" id="parent_category_" name="parent_category_">
					<option value="">Select a parent category </option>
					<?php foreach($categories as $cat):?>
						<option value="<?=$cat->id;?>" <?php if(isset($info) && $info->main_category==$cat->id) echo "selected";?>><?=$cat->name;?></option>
					<?php endforeach;?>
				</select>
			</div>
			<div class="span4">
				<label for="parent_category_">
					 Name of Event  (70 char. max)						
				</label>
				<input type="text" class="span4 validate[required]" value="<?php if(isset($info)) echo $info->event_name;?>" name="section_title" id="section_title">
				<label for="based_on">
					 Based On  						
				</label>
				<select id="based_on" name="based_on" class="span4">
					<option value="0">New Event</option>
					<?php foreach($events as $ev):?>
						<option value="<?=$ev->id;?>" <?php if(isset($info) && $info->id==$ev->id) echo "selected";?>>
							<?=$ev->event_name;?>
						</option>	
					<?php endforeach;?>
				</select>
				<label for="display_type">
				 	Existing Venue (in database) 
				</label>
				<select id="location" name="location" class="span4">
					<option value="0">Not listed</option>
					<?php foreach($location as $l):?>
						<option value="<?=$l->id;?>" <?php if(isset($info) && $info->location==$l->id) echo "selected";?>>
							<?=$l->title;?>
						</option>
					<?php endforeach;?>
				</select>
				<label for="display_type">
					 New Venue (to add to database)
				</label>
				<input type="text" value="" class="span4" name="other_location" id="other_location">
				<label for="venue_street">
					Venue Street Address																	
				</label>
				<input type="text" value="" class="span4 validate[required]" name="venue_street" id="venue_street">
				<label for="zip_code">
					Venue Zip Code																
				</label>
				<input type="text" value="" class="span4 validate[required]" name="zip_code" id="zip_code">
				<label for="venue_category">
					Venue Category																
				</label>
				<select id="venue_category" name="venue_category" class="span4">
					<?php foreach($venue_category as $cat):?>
						<option value="<?=$cat->id;?>">
							<?=$cat->name;?>
						</option>
					<?php endforeach;?>
				</select>
				<label for="website">
					Event Website																	
				</label>
				<input type="text" value="<?php if(isset($info)) echo $info->event_website;?>" class="span4 validate[required]" name="website" id="website">
				<label for="extra_directions">
					Venue Extra Directions														
				</label>
				<textarea class="span4 text_editor_small validate[required]" name="extra_directions" id="extra_directions"><?php if(isset($info)) echo $info->extra_directions;?></textarea>
			</div>
			<div class="detail_title span4 pull-right pink">Additional Event Categories</div>
			<div class="span4 pull-right" style="background: url(<?=base_url();?>img/back_arrows.jpg) no-repeat scroll 0px 0px transparent; width: 305px; margin-right:-5px;">
				<select multiple="multiple" class="span4" name="category_select[]" id="category_select">
					<?php if($categories) 
							foreach($categories as $parent):?>
							<option <?php if(isset($categories_[$parent->id])) echo "selected";?> value="<?=$parent->id;?>"><?=$parent->name;?></option>
					<?php endforeach;?>
				</select>
				<input type="hidden" id="v4" name="v4" value="1" />
				<div class="span4 pull-right description_text" style="width:300px;margin-top:15px">
					Click on the arrows to include or remove aditional event categories categories for the event listing
				</div>
			</div>
		</div>			
<!-----------------------Event Dates-->
		<h3 class="span9">
			Event Date(s)
		</h3>
		<div class="span9 instructions">
			<?=$description['add_dates'];?>
		</div>
		<div>
			<div class="radio_container">
				<input type="radio" id="recurance" name="recurance" value="1" class="radio_" <?php if((isset($info) && $info->recurance==1) || !isset($info)) echo 'checked="checked"';?> />
			</div>
			<div class="span pink">
				Single Date Event
			</div>
		</div>
		<div class="span2">
			<div class="radio_container">
				<input type="radio" id="recurance" name="recurance" value="2" class="radio_" <?php if(isset($info) && $info->recurance==2) echo 'checked="checked"';?> />
			</div>
			<div class="span pink">
				Multiple Date Event
			</div>
		</div>
		<div class="span2">
			<div class="radio_container">
				<input type="radio" id="recurance" name="recurance" value="3" class="radio_" <?php if(isset($info) && $info->recurance==3) echo 'checked="checked"';?> />
			</div>
			<div class="span pink">
				Repeating Event
			</div>
		</div>
		<div class="op1 row" style="float: left; margin-top: 15px;">
			<label for="event_date" class="span4">
				Date
			</label>
			<label for="start_time" class="span2">
				Start Time
			</label>
			<label for="start_time" class="span2">
				End Time
			</label>
			<div class="span4">
				<div class="input-append date datepicker" data-date="<?php if(isset($dates) && $dates) echo $dates[0]->date; else echo date("Y-m-d");?>" data-date-format="yyyy-mm-dd" style="margin-top: -4px;">
					<input style="margin-bottom: 5px; margin-left: -4px; width: 256px;" readonly="readonly" id="event_date" name="event_date[]" class="span3 event_date" size="16" type="text" value="<?php if(isset($dates) && $dates) echo $dates[0]->date; else echo date("Y-m-d");?>">
					<span class="add-on"><i class="icon-th"></i></span>
				</div>
			</div>
			<div class="span2">
				<input class="timepicker span2 start_time validate[required]" type="text" id="start_time" name="start_time[]" readonly="readonly" value="<?php if(isset($dates) && $dates) echo $dates[0]->start_time;?>">
			</div>
			<div class="span2">
				<input class="timepicker span2 end_time validate[required]" type="text" id="end_time" name="end_time[]" readonly="readonly" value="<?php if(isset($dates) && $dates) echo $dates[0]->end_time;?>">
			</div>
		</div>
		<div class="op3 row" id="repeating_dates">
			<?php for($i=0;$i<count($repeating_dates);$i++):?>
				<div id="repeating_date">
					<label for="event_date" class="span2">
						Start Date
					</label>
					<label for="event_date" class="span2">
						End Date
					</label>
					<label for="start_time" class="span2">
						Start Time
					</label>
					<label for="start_time" class="span2">
						End Time
					</label>
					<div class="span2">
						<div class="input-append date datepicker" data-date="<?php if(isset($repeating_dates[$i]->start_date)) echo $repeating_dates[$i]->start_date; else echo date("Y-m-d");?>" data-date-format="yyyy-mm-dd" style="margin-top: -4px;">
							<input style="margin-bottom: 5px; margin-left: -4px; width: 95px;margin-top:0px;" readonly="readonly" id="start_date" name="start_date[]" class="span3 datepicker" size="16" type="text" value="<?php if(isset($repeating_dates[$i]->start_date)) echo $repeating_dates[$i]->start_date; else echo date("Y-m-d");?>">
							<span class="add-on"><i class="icon-th"></i></span>
						</div>
					</div>
					<div class="span2">
						<div class="input-append date datepicker" data-date="<?php if(isset($repeating_dates[$i]->end_date)) echo $repeating_dates[$i]->end_date; else echo date("Y-m-d");?>" data-date-format="yyyy-mm-dd" style="margin-top: -4px;">
							<input style="margin-bottom: 5px; margin-left: -4px; width: 95px;margin-top:0px;" readonly="readonly" id="end_date" name="end_date[]" class="span3 datepicker" size="16" type="text" value="<?php if(isset($repeating_dates[$i]->end_date)) echo $repeating_dates[$i]->end_date; else echo date("Y-m-d");?>">
							<span class="add-on"><i class="icon-th"></i></span>
						</div>
					</div>
					<div class="span2">
						<input class="timepicker span2 start_time validate[required]" type="text" id="start_time" name="start_time_m[]" readonly="readonly" value="<?php if(isset($repeating_dates[$i]->start_time)) echo $repeating_dates[$i]->start_time;?>">
					</div>
					<div class="span2">
						<input class="timepicker span2 end_time validate[required]" type="text" id="end_time" name="end_time_m[]" readonly="readonly" value="<?php if(isset($repeating_dates[$i]->end_time)) echo $repeating_dates[$i]->end_time;?>">
					</div>
					<button type="button" class="btn btn-danger pull-right btn-small remove_row_multiple">
						<i class="icon-remove icon-white"></i>
					</button>
				</div>
			<?php endfor;?>
		</div>
		<div class="span9 op3" style="margin-top:15px;">
			<button type="button" class="btn btn-info pull-left btn-small" id="add_repeating_date">
				<i class="icon-plus icon-white"></i>
				Add Date
			</button>
		</div>
		<div id="date_space" class="op2">
			<div class="span9 instructions">
				Consecutive Dates
			</div>
			<div class="row row2" id="dates2">
				<label for="start_time" class="span4">
					Start Date
				</label>
				<label for="start_time" class="span2">
					Start Time
				</label>
				<label for="start_time" class="span2">
					End Time
				</label>
				<?php 
					$start_date=$end_date=$start_time=$end_time="";
					if(isset($dates) && $dates && $dates[0]->non_consec==0):
						$start_date = $dates[0]->date;
						$end_date = $dates[count($dates)-1]->date;
						$start_time = $dates[0]->start_time;
						$end_time = $dates[0]->end_time;
					endif;
				?>
				<div class="span4">
					<div class="input-append date datepicker" data-date="<?=date("Y-m-d");?>" data-date-format="yyyy-mm-dd" style="margin-top: -4px;">
						<input style="margin-bottom: 5px; margin-left: -4px; width: 259px;" readonly="readonly" id="start_date_mul" name="start_date_mul" class="span3 event_date" size="16" type="text" value="<?=$start_date;?>">
						<span class="add-on"><i class="icon-th"></i></span>
					</div>
				</div>
				<div class="span2">
					<input class="timepicker span2 start_time" type="text" id="start_mul" name="start_mul" readonly="readonly" value="<?=$start_time;?>">
				</div>
				<div class="span2">
					<input class="timepicker span2 end_time" type="text" id="end_mul" name="end_mul" readonly="readonly" value="<?=$end_time;?>">
				</div>
				<label for="end_date_mul" class="span8">
					End Date
				</label>
				<div class="span4">
					<div class="input-append date datepicker" data-date="<?=date("Y-m-d");?>" data-date-format="yyyy-mm-dd" style="margin-top: -4px;">
						<input style="margin-bottom: 5px; margin-left: -4px; width: 259px;" readonly="readonly" id="end_date_mul" name="end_date_mul" class="span3 event_date" size="16" type="text" value="<?=$end_date;?>">
						<span class="add-on"><i class="icon-th"></i></span>
					</div>
				</div>
			</div>
			<div class="span9 instructions" style="margin-top:30px;">
				Non-Consecutive Dates
			</div>
			<?php if(isset($dates) && $dates && $dates[0]->non_consec==1):?>
				<?php foreach($dates as $date):?>
					<div class="row row2" id="dates">
						<div class="span4" >
								Date
						</div>
						<label for="start_time" class="span2">
							Start Time
						</label>
						<label for="start_time" class="span2">
							End Time
						</label>
						<div class="span4">
							<div class="input-append date datepicker" data-date="<?=$date->date;?>" data-date-format="yyyy-mm-dd" style="margin-top: -4px;">
								<input style="margin-bottom: 5px; margin-left: -4px; width: 259px;" readonly="readonly" id="event_date" name="event_date[]" class="span3 event_date" size="16" type="text" value="<?=$date->date;?>">
								<span class="add-on"><i class="icon-th"></i></span>
							</div>
						</div>
						<div class="span2">
							<input class="timepicker span2 start_time" type="text" id="start_time" name="start_time[]" readonly="readonly" value="<?=$date->start_time;?>">
						</div>
						<div class="span2">
							<input class="timepicker span2 end_time" type="text" id="end_time" name="end_time[]" readonly="readonly" value="<?=$date->end_time;?>">
						</div>
						<button type="button" class="btn btn-danger pull-right btn-small remove_row">
							<i class="icon-remove icon-white"></i>
						</button>
					</div>	
				<?php endforeach;?>
			<?php else:?>
				<div class="row row2" id="dates">
					<div class="span4">
							Date
					</div>
					<label for="start_time" class="span2">
						Start Time
					</label>
					<label for="start_time" class="span2">
						End Time
					</label>
					<div class="span4">
						<div class="input-append date datepicker" data-date="<?php echo date("Y-m-d");?>" data-date-format="yyyy-mm-dd" style="margin-top: -4px;">
							<input style="margin-bottom: 5px; margin-left: -4px; width: 259px;" readonly="readonly" id="event_date" name="event_date[]" class="span3 event_date" size="16" type="text" value="">
							<span class="add-on"><i class="icon-th"></i></span>
						</div>
					</div>
					<div class="span2">
						<input class="timepicker span2 start_time" type="text" id="start_time" name="start_time[]" readonly="readonly" value="">
					</div>
					<div class="span2">
						<input class="timepicker span2 end_time" type="text" id="end_time" name="end_time[]" readonly="readonly" value="">
					</div>
					<button type="button" class="btn btn-danger pull-right btn-small remove_row">
						<i class="icon-remove icon-white"></i>
					</button>
				</div>
			<?php endif;?>
		</div>
		<div class="span9 op2" style="margin-top:15px;">
			<button type="button" class="btn btn-info pull-left btn-small" id="add_date">
				<i class="icon-plus icon-white"></i>
				Add Date
			</button>
		</div>
		<div class="op3">
			<h3 class="span9">
				Recurrence  
			</h3>
			<div class="span4">
				<ul class="span5 recurrence">
					<li>
						<input type="radio" id="recurrence_type" name="recurrence_type" value="1" <?php if(!isset($info) || $info->recurrence_type==1) echo 'checked="checked"';?>>
						Weekly (on the same day)
					</li>
					<li>
						<input type="radio" id="recurrence_type" name="recurrence_type" value="2" <?php if(isset($info) && $info->recurrence_type==2) echo 'checked="checked"';?>>
						Bi-Weekly (on the same day)
					</li>
					<li>
						<input type="radio" id="recurrence_type" name="recurrence_type" value="3" <?php if(isset($info) && $info->recurrence_type==3) echo 'checked="checked"';?>>
						Monthly (on the same day)
					</li>
					<li>
						<input type="radio" id="recurrence_type" name="recurrence_type" value="4" <?php if(isset($info) && $info->recurrence_type==4) echo 'checked="checked"';?>>
						Monthly (on the same date)
					</li>
				</ul>
			</div>
		</div>			
<!-----------------------Images-->
		<h3 class="span9">
			Event Photos  
		</h3>
		<div class="span9 instructions">
			<?=$description['add_photos'];?>
		</div>
		<div class="row row2">
			<div class="span4 pull-right description_text">
				<?=$description["text"];?>
			</div>
			<div class="span4">
				<label for="image1">
					Small Event Thumbnail (for calendar view) <br>(186w x181t) 																	
				</label>
				<div>
					<input type="text" id="image1" name="image1" class="span3 file_text" value="<?php if(isset($info)) echo $info->event_image;?>" readonly>
					<input type="file" id="image1_file" name="image1_file" class="file_upload">
					<div class="clear_file" data-id="image1">
						<i class="icon-remove"></i>
					</div>
				</div>
				<label for="credit1">
					 Photo Credit  						
				</label>
				<input type="text" class="span4" value="<?php if(isset($info)) echo $info->caption1;?>" name="credit1" id="credit1">
				<label for="image3" style="margin-top: 20px;">
					Large Event Image (for detail view) <br>(770 pixels wide maximum)											
				</label>
				<div>
					<input type="text" readonly="" value="<?php if(isset($info)) echo $info->big_image;?>" class="span3 file_text" name="big_image" id="big_image">
					<input width="75" type="file" height="29" class="file_upload" name="big_image_file" id="big_image_file">
					<div class="clear_file" data-id="image1">
						<i class="icon-remove"></i>
					</div>
				</div>
				<label for="credit2">
					 Photo Credit  						
				</label>
				<input type="text" class="span4" value="<?php if(isset($info)) echo $info->caption2;?>" name="credit2" id="credit2">
			</div>
		</div>
		<h3 class="span9">
			Description  
		</h3>
		<div class="span9">
			<label for="where">
				Event Information																	
			</label>
			<textarea class="span9 text_editor_ validate[required]" name="information" id="information"><?php if(isset($info)) echo $info->event_information;?></textarea>
		</div>	
		<div class="span9" style="margin-top:20px">
			<label for="what">
				  Sponsor/Organization Information 																
			</label>
			<textarea class="span9 text_editor_ validate[required]" name="host" id="host"><?php if(isset($info)) echo $info->sponsors; elseif(isset($orga)) echo $orga->information;?></textarea>
		</div>					
		<div class="span9" style="margin-top:15px">
			<input type="hidden" name="approved" id="approved" value="1">
			<button type="button" class="btn btn-success pull-left clear" style="margin-right: 10px;">
				<i class="icon-refresh icon-white"></i>
				Clear
			</button>
			<button type="button" class="btn btn-success pull-left preview">
				<i class="icon-eye-open icon-white"></i>
				Preview
			</button>
			<button type="submit" class="btn btn-info pull-right">
				Submit
				<i class="icon-chevron-right icon-white"></i>
			</button>
			<?php $segment = $this->uri->segment(5);
			if($segment):?>
			<button type="button" class="btn btn-warning pull-right delete" style="margin-right: 10px;">
				<i class="icon-remove-sign icon-white"></i>
				Delete
			</button>
			<?php endif;?>
		</div>		
	</div>
</form>
</div>
<? $class="blue";?>
<div class="modal hide fade modal2" style="margin-left: -500px; width: 1032px; top: 265px;">
	<div class="modal-header" style="border:none;padding:0;">
    	<button type="button" class="btn btn-info pull-right" data-dismiss="modal" aria-hidden="true" style="position: relative; background: none repeat scroll 0% 0% rgb(0, 0, 0); right: 25px; z-index: 99; top: 57px; border: medium none; box-shadow: none;">
    		<i class="icon-remove" style="background-image: url(<?=base_url();?>img/glyphicons-halflings-cus.png);"></i>
    	</button>
   	</div>
	 <div class="modal-body">
	<div class="calendar_text_container" style="width:1000px;">
		<div class="upcoming_events">
			<div class="top" id="headline">
				Headline
			</div>
		</div>
		<div class="left_column">
			<div class="down_arrow <?=$class;?>">
				<div class="time">
					<span id="date_">Date</span>
					<div class="description">
					</div>
					<div class="hours">
					</div>
					<div class="organization">
					</div>
				</div>
			</div>
		</div>
		<div class="right_column detail <?=$class;?>">
			<div class="detail_title">
				WHAT:
			</div>
			<div class="text" id="what">
			</div>
			<div class="detail_title">
				WHO:
			</div>
			<div class="text" id="who">
			</div>
			<div class="detail_title">
				WHERE	:
			</div>
			<div class="text" id="where">
			</div>
			<div class="detail_title">
				WEBSITE:
			</div>
			<div class="text">
				<a href="" id="website_">
					Website
				</a>
			</div>
			<div class="text">
				<img src="" id="big_image_">
			</div>
		</div>
	</div>
	</div>
</div>