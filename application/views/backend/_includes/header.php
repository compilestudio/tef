<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01//EN"
"http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<head>
	<title>
		..::CMS::..
	</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js" type="text/javascript"></script>
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
	<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
	<script src="<?=base_url();?>_js/backend/bootstrapSwitch.js" type="text/javascript"></script>
	<script src="<?=base_url();?>_js/backend/jquery.uploadify.min.js" type="text/javascript"></script>
	<script src="<?=base_url();?>_js/backend/bootstrap-modal.js" type="text/javascript"></script>
	<script src="<?=base_url();?>_js/backend/tiny_mce/jquery.tinymce.js" type="text/javascript"></script>
	<script src="<?=base_url();?>_js/backend/bootstrap-timepicker.min.js" type="text/javascript"></script>
	<script src="<?=base_url();?>_js/backend/bootstrap-datepicker.js" type="text/javascript"></script>
	<script src="<?=base_url();?>_js/backend/jquery.pnotify.min.js" type="text/javascript"></script>
	<script src="<?=base_url();?>_js/backend/jquery.colorbox-min.js" type="text/javascript"></script>
	<script language="JavaScript" src="<?=base_url(); ?>_js/backend/jquery.blockUI.js"></script>
	<?php echo link_tag('_css/backend/backend.css');?>
	<?php echo link_tag('_css/backend/colorbox.css');?>
	<?php echo link_tag('_css/backend/jquery.pnotify.default.css');?>
	<script language="javascript">
	$(document).ready(function(){
		//Aplicamos el tiny mce al editor del caption de las imágenes
		$('textarea.tinymce').tinymce({
			// Location of TinyMCE script
			script_url : '<?=base_url();?>_js/backend/tiny_mce/tiny_mce.js',

			// General options
			theme : "advanced",
			plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist,wordcount",

			// Theme options
			theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,pastetext,pasteword, |, formatselect,fontselect,fontsizeselect",
			theme_advanced_buttons2 : "tablecontrols ",
			theme_advanced_buttons3 : "bullist,numlist,|,code,|,sub,sup,|,forecolor,backcolor,link,unlink",
			theme_advanced_buttons4 : "",
			theme_advanced_text_colors : "000000,006598,BDB400,FF8B00,E5495D",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "bottom",
			theme_advanced_resizing : true,
			force_p_newlines : false,
			force_br_newlines : true,
			forced_root_block : '',
			height:300,
			width:"100%"
		});
		//Aplicamos el tiny mce al corto
		$('textarea.editor_small').tinymce({
			// Location of TinyMCE script
			script_url : '<?=base_url();?>_js/backend/tiny_mce/tiny_mce.js',

			// General options
			theme : "advanced",
			plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist,wordcount",

			// Theme options
			theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,pastetext,pasteword",
			theme_advanced_buttons2 : "formatselect,fontselect,fontsizeselect",
			theme_advanced_buttons3 : "tablecontrols",
			theme_advanced_buttons4 : "bullist,numlist,|,code,|,sub,sup,|,forecolor,backcolor,link,unlink",
			theme_advanced_text_colors : "000000,006598,BDB400,FF8B00,E5495D",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "bottom",
			theme_advanced_resizing : true,
			force_p_newlines : false,
			force_br_newlines : true,
			forced_root_block : '',
			height:300,
			width: 300,
			setup : function(ed) {
			        ed.onKeyUp.add(function(ed, evt) {
			        	//console.log(evt.keyCode);
					//Actualizamos el contador de palabras
					$('.char_count .chars_number',$(ed.getContainer()).parent()).html($(ed.getBody()).text().length+1);
					//console.log($(tinyMCE.get(tinyMCE.activeEditor.id).getElement()).attr('max_chars'));
					//Prevenimos escritura si se llegó al máximo de caracteres
					var max_chars = $(tinyMCE.get(tinyMCE.activeEditor.id).getElement()).attr('max_chars');
					if ( $(ed.getBody()).text().length+1 > max_chars && max_chars!=0 && evt.keyCode!=8 && evt.keyCode!=16 && evt.keyCode!=37 && evt.keyCode!=38 && evt.keyCode!=39 && evt.keyCode!=40 && evt.keyCode!=116){
						evt.preventDefault();
						evt.stopPropagation();
						return false;
					}
			        });
			} 
		});
		//Aplicamos el tiny mce al grande
		$('textarea.editor_big').tinymce({
			// Location of TinyMCE script
			script_url : '<?=base_url();?>_js/backend/tiny_mce/tiny_mce.js',

			// General options
			theme : "advanced",
			plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist,wordcount",
			// Theme options
			theme_advanced_buttons1 : "formatselect,fontselect,fontsizeselect, |, bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,pastetext,pasteword,|,code,",
			theme_advanced_buttons2 : "tablecontrols,|,forecolor,backcolor,link,unlink,|,sub,sup,bullist,numlist",
			theme_advanced_buttons3 : "",
			theme_advanced_buttons4 : "",
			theme_advanced_text_colors : "000000,006598,BDB400,FF8B00,E5495D",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "bottom",
			theme_advanced_resizing : true,
			force_p_newlines : false,
			force_br_newlines : true,
			forced_root_block : '',
			height:300,
			width: "100%",
			setup : function(ed) {
			        ed.onKeyUp.add(function(ed, evt) {
			        	//console.log(evt.keyCode);
					//Actualizamos el contador de palabras
					$('.char_count .chars_number',$(ed.getContainer()).parent()).html($(ed.getBody()).text().length+1);
					//console.log($(tinyMCE.get(tinyMCE.activeEditor.id).getElement()).attr('max_chars'));
					//Prevenimos escritura si se llegó al máximo de caracteres
					var max_chars = $(tinyMCE.get(tinyMCE.activeEditor.id).getElement()).attr('max_chars');
					if ( $(ed.getBody()).text().length+1 > max_chars && max_chars!=0 && evt.keyCode!=8 && evt.keyCode!=16 && evt.keyCode!=37 && evt.keyCode!=38 && evt.keyCode!=39 && evt.keyCode!=40 && evt.keyCode!=116){
						evt.preventDefault();
						evt.stopPropagation();
						return false;
					}
			        });
			} 
		});
		//Armamos el uploadify para los campos pequeños
		$('.file_small').each(function(){
			var parent = $(this).parent();
			$(this).uploadify({
				'buttonImage' : '<?=base_url();?>_img/backend/browse.png',
				'width'    : 75,
				'height'   : 30,
				'swf'      : '<?=base_url();?>swf/uploadify.swf',
				'uploader' : '<?=site_url("backend/content/upload_file_fields");?>' ,
				'onUploadSuccess' : function(file, data, response) {
					$('input:text',$(parent)).val(data);
				}	
			});
		});
		$('.file_big').each(function(){
			var parent = $(this).parent();
			$(this).uploadify({
				'buttonImage' : '<?=base_url();?>_img/backend/browse.png',
				'width'    : 75,
				'height'   : 30,
				'swf'      : '<?=base_url();?>swf/uploadify.swf',
				'uploader' : '<?=site_url("backend/content/upload_file_fields");?>' ,
				'onUploadSuccess' : function(file, data, response) {
					$('input:text',$(parent)).val(data);
				}	
			});
		});
	});
	</script>
</head>
<body>
<div class="container">
	<div class="header">
		<a href="<?=site_url("backend");?>" class="logo">
			<img src="<?=base_url();?>img/logo_back.jpg">
		</a>
		<?php 
		$user=$this->session->userdata('user_id');
		if($user):?>
			<a href="<?=site_url('backend/session/end');?>" class="log_off">
				Sign out
			</a>
		<?php endif;?>
		<div class="backend_name">
			content management tool
		</div>
		
	</div> 	