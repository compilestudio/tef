</div>
</div>
<script>
function adjustModalMaxHeightAndPosition() {
    $('.modal').each(function () {
        if ($(this).hasClass('in') === false) {
            $(this).show();
        }
        var contentHeight = $(window).height() - 60;
        var headerHeight = $(this).find('.modal-header').outerHeight() || 2;
        var footerHeight = $(this).find('.modal-footer').outerHeight() || 2;
        $(this).find('.modal-content').css({
            'max-height': function () {
                return contentHeight;
            }
        });
        $(this).find('.modal-body').css({
            'max-height': function () {
                return contentHeight - (headerHeight + footerHeight);
            }
        });
        $(this).find('.modal-dialog').addClass('modal-dialog-center').css({
            'margin-top': function () {
                return -($(this).outerHeight() / 2);
            },
            'margin-left': function () {
                return -($(this).outerWidth() / 2);
            }
        });
        if ($(this).hasClass('in') === false) {
            $(this).hide();
        }
    });
}
if ($(window).height() >= 320) {
    $(window).resize(adjustModalMaxHeightAndPosition).trigger('resize');
}
$(document).ready(function(){
    if ('ontouchstart' in document) {
        $('.menu_container .menu ul.menu li ul li a').removeClass('no-touch');
    }
});
</script>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<script src="<?=base_url();?>_js/jquery.hoverIntent.minified.js"></script>
<script src="<?=base_url();?>_js/owl.carousel.min.js"></script>
<script src="<?=base_url();?>_js/slick.min.js"></script>
<script src="<?=base_url();?>_js/jquery.slimscroll.min.js"></script>
<script src="<?=base_url();?>_js/site.js"></script>
<script src="<?=base_url();?>_js/modal.js"></script>
</body>
</html>