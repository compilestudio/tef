<?php
function addhttp($url) {
	if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
		$url = "http://" . $url;
	}
	return $url;
}
function cmp($a, $b){
	if ($a['c_order'] == $b['c_order']) {
		return 0;
	}
	return ($a['c_order'] < $b['c_order']) ? -1 : 1;
}
?>
<!doctype html>
<html lang="en">
<head>
	<link rel="icon" type="image/png" href="<?=base_url();?>_img/ico.ico">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>
		TEF
	</title>
	<!-- Bootstrap -->
	<?php echo link_tag('_css/front/style.css?v=0.21');?>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="http://getbootstrap.com/dist/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="<?=site_url("_css/front/owl.carousel.css")?>">
    <link rel="stylesheet" href="<?=site_url("_css/front/owl.theme.css")?>">
</head>
<body class="bg<?=$class;?> bg<?=$method;?>">
<script>
    var isMobile = false; //initiate as false
    // device detection
    if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
        || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) isMobile = true;
    function debouncer_header( func , timeout ) {
        var timeoutID , timeout = timeout || 400;
        return function () {
            var scope = this , args = arguments;
            clearTimeout( timeoutID );
            timeoutID = setTimeout( function () {
                func.apply( scope , Array.prototype.slice.call( args ) );
            } , timeout );
        }
    }
    $( window ).load( debouncer_header( function ( e ) {
        $(".all-content").fadeOut();
        $("html").css('overflow-y', "auto");

    }, 300));

    $(function() {
        if(isMobile){
            $('.menu_container .menu ul.menu > li.active > ul').removeClass("submenu");
        }
        $(".dropdown.active").on("hide.bs.dropdown",function(e) {
            e.preventDefault();
            return false;
        });
        var closed = false;
        $(".all-content").css('height',$(document).height());
        $("html").css('overflow-y', "auto");



        if($(".menu_container").css("display") != "none"){//desktop
            if($( "ul.menu > li").hasClass("active")){
                //Si esta activo
            }
        }
        $('.navbar-collapse').on('show.bs.collapse', function ( e) {
            //$(".navbar-collapse").hide();
            e.preventDefault();
            $(".navbar-collapse").css("max-height", "initial");
            $(".navbar-collapse").addClass("in");
            $(".navbar-collapse").height($("body").height());
            $(".navbar-collapse").attr("aria-expanded", "true");
            $("#open_menu").hide();
            $("#close_menu").show();
        });

        $('.navbar-collapse').on('hide.bs.collapse', function ( e) {
            $("#close_menu").hide();
            $("#open_menu").show();
        });

        /*$( "span.title" ).hoverIntent(function (  ) {
            if($(this).parent().find("ul").length > 0 && !$(".menu").find("li").hasClass("active")){
                if(closed == false){
                    $(this).parent().parent().find("li").removeClass("active");
                    $(this).parent().addClass("active");
                }else{
                    closed = false;
                }
            }
        });*/ 

        $( "span.title" ).click(function() {
            if( $(this).parent().hasClass("active")){ //si esta abierta entonces cerrar
                $(this).parent().removeClass("active");
                closed = true;
            }else{ //sino esta abierta entoncesa brir y cerrar las demas
                $(this).parent().parent().find("li").removeClass("active");
                $(this).parent().addClass("active");
            }
        });
        /*

        $('.menu li').on('click',function(){
            $(this).toggleClass("active");
        });
        $('.menu li span').on('click',function(){
            $(this).parent().toggleClass("active");
        });
        */
    });
    /*
    $(document).mouseup(function (e) {
        var container = $("span.title");
        var container2 = $("ul.menu ul li a");

        if ((!container.is(e.target) && container.has(e.target).length === 0) && (!container2.is(e.target) && container2.has(e.target).length === 0) )  // ... nor a descendant of the container
        {
            $(".menu").find("li").removeClass("active");
        }
    });
    */

</script>
<!-- Static navbar -->
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid" style="padding-left: 15px !important; padding-right: 15px !important;">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <img alt="Menu" src="<?=base_url("_img/open_menu.png")?>" style="max-width: 50px" id="open_menu">
                <img alt="Menu" src="<?=base_url("_img/close_menu.png")?>" style="max-width: 50px; display: none" id="close_menu" >
            </button>
            <a class="navbar-brand" href="<?=site_url()?>">
                <img alt="Brand" src="<?=base_url("_img/logo_mobile.png")?>" style="max-width: 50px">
            </a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <?php if($contents[2]->front_display==1):?>
                    <li class="dropdown <?php if($uri1_=="projects") echo "active open";?>">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            WORK
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                            <?php foreach($categories as $cats):?>
                                <li <?php if($uri2_==$cats['category']) echo "class='active'";?>>
                                    <a href="<?=site_url("projects/".$cats['category']);?>" title="<?=$cats['category'];?>">
                                        <?=$cats['category'];?>
                                    </a>
                                </li>
                            <?php endforeach;?>
                            <li <?php if($uri1_=="projects" && empty($uri2_)) echo "class='active'";?>>
                                <a href="<?=site_url("projects");?>" title="projects">
                                    ALL
                                </a>
                            </li>
                        </ul>
                    </li>
                <?php endif;?>
                <li class="dropdown <?php if($uri1_=="people") echo "active open";?>">
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        PEOPLE
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        <?php if($contents[3]->front_display==1):?>
                            <li <?php if($uri1_=="people" && empty($uri2_)) echo "class='active'";?>>
                                <a href="<?=site_url("people");?>" title="people">
                                    ALL
                                </a>
                            </li>
                            <li <?php if($uri1_=="people" && $uri2_=="leadership") echo "class='active'";?>>
                                <a href="<?=site_url("people/leadership");?>" title="people">
                                    LEADERSHIP
                                </a>
                            </li>
                        <?php endif;?>
                        <?php if($contents[5]->front_display==1):?>
                            <li <?php if($uri1_=="about" ) echo "class='active'";?>>
                                <a href="<?=site_url("about");?>" title="about">
                                    ABOUT
                                </a>
                            </li>
                        <?php endif;?>
                        <?php if($contents[7]->front_display==1):?>
                            <li <?php if($uri1_=="clients" ) echo "class='active'";?>>
                                <a href="<?=site_url("clients");?>" title="Clients">
                                    CLIENTS
                                </a>
                            </li>
                        <?php endif;?>
                    </ul>
                </li>
                <?php if($contents[6]->front_display==1):?>
                    <li class="dropdown <?php if($uri1_=="world") echo "active open";?>">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            WORLD
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li <?php if(empty($uri2_)) echo "class='active'";?>>
                                <a href="<?=site_url("world/");?>" title="World">
                                    ALL
                                </a>
                            </li>
                            <li <?php if($uri2_=="dialog" ) echo "class='active'";?>>
                                <a href="<?=site_url("world/dialog");?>" title="World">
                                    DIALOG
                                </a>
                            </li>
                            <li <?php if($uri2_=="inspiration" ) echo "class='active'";?>>
                                <a href="<?=site_url("world/inspiration");?>" title="World">
                                    INSPIRATION
                                </a>
                            </li>
                            <li <?php if($uri2_=="other" ) echo "class='active'";?>>
                                <a href="<?=site_url("world/other");?>" title="World">
                                    OTHER
                                </a>
                            </li>
                        </ul>
                    </li>
                <?php endif;?>
                <li class="dropdown <?php if($uri1_=="contact") echo "active open";?>">
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        CONTACT
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        <?php if($contents[8]->front_display==1):?>
                            <li <?php if($uri1_=="contact" ) echo "class='active'";?>>
                                <a href="<?=site_url("contact");?>" title="Contact">
                                    MAP
                                </a>
                            </li>
                        <?php endif;?>
                        <?php if($contents[9]->front_display==1):?>
                            <li <?php if($uri1_=="careers" ) echo "class='active'";?>>
                                <a href="<?=site_url("careers");?>" title="Careers">
                                    CAREERS
                                </a>
                            </li>
                        <?php endif;?>
                    </ul>
                </li>
            </ul>
        </div><!--/.nav-collapse -->
    </div><!--/.container-fluid -->
</nav>
<div class="all-content" style=""></div>
<div class="menu_container">
	<div class="menu">
        <a href="<?=site_url("")?>">
            <img src="<?=base_url("_img/logo_landing.png")?>" alt="TEF" class="logo">
        </a>
		<ul class="menu">
            <?php if($contents[2]->front_display==1):?>
                <li <?php if($uri1_=="projects") echo "class='active'";?>>
                    <a href="<?=site_url("projects");?>" class="title">
                        WORK
                    </a>
                    <ul class="submenu">
                        <?php foreach($categories as $cats):?>
                            <li <?php if($uri2_==$cats['category']) echo "class='active'";?>>
                                <a href="<?=site_url("projects/".$cats['category']);?>" title="<?=$cats['category'];?>">
                                    <?=$cats['category'];?>
                                </a>
                            </li>
                        <?php endforeach;?>
                        <li <?php if($uri1_=="projects" && empty($uri2_)) echo "class='active'";?>>
                            <a href="<?=site_url("projects");?>" title="projects">
                                ALL
                            </a>
                        </li>
                    </ul>
                </li>
            <?php endif;?>
			<li <?php if($uri1_=="people" || $uri1_ == "about" || $uri1_ == "clients") echo "class='active'";?>>
                <a href="<?=site_url("people");?>" class="title">
					PEOPLE
				</a>
                <ul class="submenu">
                    <?php if($contents[3]->front_display==1):?>
                        <li <?php if($uri1_=="people" && empty($uri2_)) echo "class='active'";?>>
                            <a href="<?=site_url("people");?>" title="people">
                                ALL
                            </a>
                        </li>
                        <li <?php if($uri1_=="people" && $uri2_=="leadership") echo "class='active'";?>>
                            <a href="<?=site_url("people/leadership");?>" title="people">
                                LEADERSHIP
                            </a>
                        </li>
                    <?php endif;?>
                    <?php if($contents[5]->front_display==1):?>
                        <li <?php if($uri1_=="about" ) echo "class='active'";?>>
                            <a href="<?=site_url("about");?>" title="about">
                                ABOUT
                            </a>
                        </li>
                    <?php endif;?>
                    <?php if($contents[7]->front_display==1):?>
                        <li <?php if($uri1_=="clients" ) echo "class='active'";?>>
                            <a href="<?=site_url("clients");?>" title="Clients">
                                CLIENTS
                            </a>
                        </li>
                    <?php endif;?>
				</ul>
			</li>
            <?php if($contents[6]->front_display==1):?>
                <li <?php if($uri1_=="world") echo "class='active'";?>>
                    <a href="<?=site_url("world");?>" class="title">
                        WORLD
                    </a>
                    <ul class="submenu">
                        <li <?php if(empty($uri2_)) echo "class='active'";?>>
                            <a href="<?=site_url("world/");?>" title="World">
                                ALL
                            </a>
                        </li>
                        <?php foreach($categories_w as $cats):?>
                            <li <?php if(strtoupper($uri2_)==$cats['name']) echo "class='active'";?>>
                                <a href="<?=site_url("world/".$cats['name']);?>" title="<?=$cats['name'];?>">
                                    <?=$cats['name'];?>
                                </a>
                            </li>
                        <?php endforeach;?>
                    </ul>
                </li>
            <?php endif;?>
            <li <?php if($uri1_=="contact" || $uri1_ == "careers") echo "class='active'";?>>
				<a href="<?=site_url("contact");?>" class="title">
					CONTACT
				</a>
                <ul class="submenu">
                    <?php if($contents[8]->front_display==1):?>
                        <li <?php if($uri1_=="contact" ) echo "class='active'";?>>
                            <a href="<?=site_url("contact");?>" title="Contact">
                                MAP
                            </a>
                        </li>
                    <?php endif;?>
                    <?php if($contents[9]->front_display==1):?>
                        <li <?php if($uri1_=="careers" ) echo "class='active'";?>>
                            <a href="<?=site_url("careers");?>" title="Careers">
                                CAREERS
                            </a>
                        </li>
                    <?php endif;?>
                </ul>
            </li>
		</ul>
		<div class="footer">
			<ul class="social">
				<?php foreach($social as $s):?>
					<li>
						<a href="<?=addhttp($s['link']);?>" target="_blank" title="<?=$s['name'];?>">
							<img src="<?=base_url();?>uploads/<?=$s['icon'];?>" alt="<?=$s['name'];?>"/>
						</a>
					</li>
				<?php endforeach;?>
                <li>
                    <a href="javascript:void(0);" title="Search">
                        <img src="<?=base_url();?>_img/search.png" alt="Search"/>
                    </a>
                </li>
			</ul>
		</div>
	</div>
</div>

<div class="wrap">
<div class="container-fluid content">