<link rel="stylesheet" href="<?=site_url("_css/front/superslides.css")?>">
<style>
    html{
        overflow-y: auto !important;
    }
</style>

<div class="fullscreen">
    <div class="fullscreen-header">
        <h1 class="projects-title">
            CLIENTS
        </h1>
    </div>
    <div class="our-firm">
        <ul class="clients">
            <?php $cat = "NA";
            if(!empty($info))
            foreach($info as $in):?>
                <?php
                if(!empty($in['client_category']))
                if($cat!=$in['client_category']['category']):
                    $cat = $in['client_category']['category'];
                ?>
                    <li class="long">
                        <h4>
                            <?=$cat;?>
                        </h4>
                    </li>
                <?php endif;?>
                <li>
                    <div class="name <?php if(!empty($in['projects'])) echo "has_more";?>" id="client_<?=$in['row'];?>">
                        <?=$in['name'];?>
                    </div>
                    <div class="project_list">
                        <?php
                        if(!empty($in['projects']))
                        foreach($in['projects'] as $pr):?>
                            <a href="<?=site_url("projects/detail/".$pr->row_id);?>">
                                <?=$pr->fields['p_name'];?>
                            </a>
                        <?php endforeach;?>
                    </div>
                </li>
            <?php endforeach;?>
        </ul>
    </div>
</div>


<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="<?=base_url("_js/superslides/jquery.easing.1.3.js")?>"></script>
<script src="<?=base_url("_js/superslides/jquery.animate-enhanced.min.js")?>"></script>
<script src="<?=base_url("_js/superslides/hammer.min.js")?>"></script>
<script src="<?=base_url("_js/superslides/jquery.superslides.js")?>" type="text/javascript" charset="utf-8"></script>
<script>
$(function() {
    $('img').on('dragstart', function(event) { event.preventDefault(); });

    $(".more-btn").click(function() {
        $('html, body').animate({
            scrollTop: $(".our-firm").offset().top
        }, 1000);
    });

    $('.has_more').on('click',function(){
        $('.project_list',$(this).parent()).slideToggle();
    });
});
</script>