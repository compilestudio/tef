<link rel="stylesheet" href="<?=site_url("_css/front/superslides.css")?>">
<style>
    .content{
        background: none;
        padding: 0;
        //padding-left: 40px;
    }
    html{
        overflow-y: hidden !important;
    }
    .slider-content .more{
        z-index: 1000;
    }
</style>

<div class="fullscreen" style="margin-top:0px;">
    <div class="project">
        <div class="project-name">
            <span> <?=$project["p_name"]?> </span>


            <div class="project-more">
                <img src="<?=base_url("_img/more.png")?>" alt="more" id="btn-more" action="more"/>
            </div>
        </div>
        <div class="project-info" style="display: none">
            <div class="project-text">
                <?php
                    if(!empty($project["text"])){
                        echo $project["text"];
                    }
                ?>
            </div>
            <?php if(!empty($project["client_info"])):?>
                <div class="project-detail">
                    CLIENT:
                    <span>
                        <a href="<?=site_url('clients/#client_'.$project['client']);?>">
                            <?=$project["client_info"]['name']?>
                        </a>
                    </span>
                </div>
            <?php endif;?>
            <?php if(!empty($project["p_size"])):?>
                <div class="project-detail">
                    SIZE:
                    <span><?=$project["p_size"]?></span>
                </div>
            <?php endif;?>
            <?php if(!empty($project["p_location"])):?>
                <div class="project-detail">
                    LOCATION:
                    <span><?=$project["p_location"]?></span>
                </div>
            <?php endif;?>

            <?php if(!empty($project["awards"])):?>
                <div class="project-detail">
                    AWARDS:
                    <span><?=$project["awards"]?></span>
                </div>
            <?php endif;?>

            <?php if(!empty($project['quote'])):?>
                <div class="project-detail">
                    <?=$project['quote'];?>
                </div>
            <?php endif;?>
            <?php if(!empty($awards)):?>
                <div class="project-detail">
                    AWARDS:
                    <?php if(isset($awards) && count($awards) >0):
                        foreach($awards as $item): if($item["a_project"] == $id):?>
                            <a href="<?=site_url("firm/awards/".$item["row"])?>"><span><?=$item["a_name"]?>.</span></a>
                        <?php
                        endif; endforeach;
                    endif; ?>
                </div>
            <?php endif;?>
            <?php if(!empty($press)):?>
                <div class="project-detail">
                    PRESS:
                    <?php if(isset($press) && count($press) >0):
                        foreach($press as $item): if($item["p_project"] == $id):?>
                            <a href="<?=site_url("firm/press/".$item["row"])?>"><span><?=$item["p_title"]?>.</span></a>
                        <?php
                        endif; endforeach;
                    endif; ?>
                </div>
            <?php endif;?>
        </div>
    </div>
    <?php
        $related_projects = array();
        if(isset($project["related_projects"])){
            $related_projects = explode(",", $project["related_projects"]);
        }
    ?>

    <div class="slider-content">
        <div id="slides">
            <div class="slides-container">
                <?php if(isset($images) && count($images) >0):
                    foreach($images as $image):?>
                        <img src="<?=base_url("uploads/".$image->img)?>" alt="Project">
                    <?php
                    endforeach;
                    else:?>
                        <img src="<?=base_url("uploads/".$project["pimg1"])?>" alt="Project">
                    <?php endif; ?>
            </div>
            <nav class="slides-navigation">
                <a href="javascript:void(0)" class="next">
                    <img src="<?=base_url("_img/related_next.png")?>" alt="next" />
                </a>
                <a href="javascript:void(0)" class="prev">
                    <img src="<?=base_url("_img/related_prev.png")?>" alt="prev" />
                </a>
            </nav>
        </div>
        <?php if($related_projects[0] > 0):?>
            <div class="more">
                <div class="more-btn">
                    <img src="<?=site_url("_img/more.png")?>" alt="more">
                    <span class="image-more" style="display: block;">RELATED PROJECTS</span>
                    <span class="image-only" style="display: none;">MAIN PROJECT</span>
                </div>
            </div>
        <?php endif;?>
    </div>
    <?php if($related_projects[0] > 0):?>
        <div class="owl-content">
            <div id="owl-carousel" >
                <?php
                    foreach($projects as $item):
                    if(!in_array($item["row"],$related_projects )){ continue; }?>
                        <div class="item item-thumbnail">
                            <a href="<?=site_url("projects/detail/".$item["row"])?>">
                                <span class="item-title">
                                    <?=$item["p_name"]?>
                                </span>
                                <img class="img-responsive" src="<?=base_url("uploads/".$item["pimg1"])?>" alt="TEF - Related project">
                            </a>
                        </div>
                    <?php
                    endforeach;?>
            </div>
        </div>
    <?php endif;?>
</div>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="<?=base_url("_js/superslides/jquery.easing.1.3.js")?>"></script>
<script src="<?=base_url("_js/superslides/jquery.animate-enhanced.min.js")?>"></script>
<script src="<?=base_url("_js/superslides/hammer.min.js")?>"></script>
<script src="<?=base_url("_js/superslides/jquery.superslides.js")?>" type="text/javascript" charset="utf-8"></script>
<script>
    var hidden = 0;
    $(function() {

        $('img').on('dragstart', function(event) { event.preventDefault(); });
        var $slides = $('#slides');
        Hammer($slides[0]).on("swipeleft", function(e) {
            $slides.data('superslides').animate('next');
        });
        Hammer($slides[0]).on("swiperight", function(e) {
            $slides.data('superslides').animate('prev');
        });
        $slides.superslides({

        });
        $( "#btn-more" ).click(function() {
            if($(this).attr("action") == "more"){
                graybox("open");
                $('.prev').hide().addClass("hide_");
                $('.next').hide().addClass("hide_");
                //$('.slider-content .more').hide().addClass("hide_");
            }else{
                graybox("close");
                $('.prev').show().removeClass("hide_");
                $('.next').show().removeClass("hide_");
                //$('.slider-content .more').show().removeClass("hide_");
            }
        });

        $("body").on("mousemove",function(event) {


            if (
                (event.pageX <= ($('.prev').offset().left+33) && event.pageX >= ($('.prev').offset().left)) &&
                (event.pageY <= ($('.prev').offset().top+33) && event.pageY >= ($('.prev').offset().top))
                ) {
                //$(".hide-arrow").show();
                $(".project").css('z-index',3);
            }else{
                $(".project").css('z-index',4);
                //$(".hide-arrow").hide();
            }
        });

        $(".next").click(function(e) {
            e.stopPropagation();
            $slides.data('superslides').animate('next');
        });
        $(".prev").click(function(e) {
            e.stopPropagation();
            $slides.data('superslides').animate('prev');
        });

        $("#owl-carousel").owlCarousel({
            autoPlay : false,
            items : 4,
            itemsDesktop : [1199,4],
            itemsDesktopSmall : [979,2],
            itemsTablet: [768,2],
            itemsMobile: [480,2],
            navigation : true,
            pagination: false
            //,afterMove: moved
        });
        $(".owl-prev").text("");
        $(".owl-next").text("");
        $(".slides-pagination a").text("");


        <?php if($related_projects[0] > 0):  ?>
            //$(".more img").hide();
            //$(".more span:nth-child(2)").hide();
        <?php endif;?>


        //hide related projects
        $("#slides").click(function() {
            <?php if($related_projects[0] > 0):?>
                //$(".more img").show();
                //$(".more span").show();
            <?php endif;?>
            related_projects("hide");
        });


        $(".more-btn").click(function() {
            //if($(".slider-content").css("margin-top").replace('px', '') < 0){
            if(hidden==1){
                related_projects("hide");
                //$(".more span").show();
            }else{
                related_projects("show");
                //$(".more span").hide();

            }
        });
        /*
        $( ".more-btn" )
            .on( "mouseenter", function() {
                if($(".slider-content").css("margin-top").replace('px', '') < 0){
                    $(".more .image-only").fadeIn();
                }else{
                    $(".more .image-more").fadeIn();
                }
            })
            .on( "mouseleave", function() {
                $(".more span").fadeOut();
            });
        */

    });
    $( window ).load( function () {
        //related_projects("hide");
        //$(".more span").show();
        if(isMobile){
            $('.content .list .item a .name').show();
        }
    });

    $( window ).resize( debouncer( function ( e ) {
        // if is open
        if($(".slider-content").css("margin-top").replace('px', '') < 0){
            related_projects("show");
        }
    }));
    var graybox_open = false;
    function graybox(state){
        if(state == "open"){
            //show
            $(".project-info").slideDown("200");
            var height = $(".slider-content").height();
            if($(".slider-content").css("margin-top").replace('px', '') < 0){
                height = height-$("#owl-carousel").height();
            }
            $(".project").css('height',height);
            $(".project").css('max-height',height);
            $("#btn-more").attr("src", "<?=base_url("_img/close.png")?>");
            $("#btn-more").removeAttr("action").attr("action", "close");
            $(".prev").fadeOut();
            $(".prev").addClass("hide-arrow");

            graybox_open = true;
            //$(".more img").show();
            //related_projects("hide");
        }
        if(state == "close"){
            //hide
            $(".project-info").slideUp("200");
            $(".project").css('height','auto');
            $("#btn-more").attr("src", "<?=base_url("_img/more.png")?>");
            $("#btn-more").removeAttr("action").attr("action", "more");
            $(".prev").fadeIn();
            $(".prev").removeClass("hide-arrow");
        }

    }

    function debouncer( func , timeout ) {
        var timeoutID , timeout = timeout || 400;
        return function () {
            var scope = this , args = arguments;
            clearTimeout( timeoutID );
            timeoutID = setTimeout( function () {
                func.apply( scope , Array.prototype.slice.call( args ) );
            } , timeout );
        }
    }

    function related_projects(){
        //$(".more-btn span").hide();
        if(hidden==0){
            $( ".owl-content" ).animate({
                bottom: $('.owl-content .owl-carousel .owl-item').height(),
                position: "absolute"
            }, 500);
            /*
            $( ".more" ).animate({
                bottom: "+="+$('.owl-content .owl-carousel .owl-item').height()
            }, 500);
            */
            var src = $(".more-btn img").attr("src");
            $(".more-btn img").attr("src", src.replace("close.png", "more.png"));

            $('.image-only').show();
            $('.image-more').hide();
            hidden=1;
        }else{
            var src = $(".more-btn img").attr("src");
            $(".more-btn img").attr("src", src.replace("more.png", "close.png"));
            $( ".owl-content" ).animate({
                bottom: "=0",
                //bottom: 0,
                position: "relative"
            }, 500);

            /*
             $( ".more" ).animate({
             bottom: "20px"+($('.owl-content .owl-carousel .owl-item').height()-300)
             }, 500);*/
            //$('.more').css("bottom","10px");
            $('.image-only').hide();
            $('.image-more').show();
            hidden=0;
        }
    }
</script>