<link rel="stylesheet" href="<?=site_url("_css/front/superslides.css")?>">
<style>
    html{
        overflow-y: auto !important;
    }
</style>

<div class="fullscreen">
    <div class="fullscreen-header">
        <h1 class="projects-title">
            <?=$info["main_title"]?>
        </h1>
    </div>
    <div class="our-firm">
        <div class="address">
            <?=$info["title"]?><br><br>
            <?=$info["address"]?><br><br>
            T <?=$info["phone"]?><br>
            F <?=$info["fax"]?><br>
        </div>
        <div class="map">
            <iframe width="100%" height="450" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q=<?=strip_tags($info["address"]);?>&key=AIzaSyDfT7j5pYCBawU9EC2Azbrkcufwof2qu9Y" allowfullscreen></iframe>
        </div>
    </div>
</div>


<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="<?=base_url("_js/superslides/jquery.easing.1.3.js")?>"></script>
<script src="<?=base_url("_js/superslides/jquery.animate-enhanced.min.js")?>"></script>
<script src="<?=base_url("_js/superslides/hammer.min.js")?>"></script>
<script src="<?=base_url("_js/superslides/jquery.superslides.js")?>" type="text/javascript" charset="utf-8"></script>
<script>

    $(function() {
        $('img').on('dragstart', function(event) { event.preventDefault(); });

        $(".more-btn").click(function() {
            $('html, body').animate({
                scrollTop: $(".our-firm").offset().top
            }, 1000);
        });
    });
</script>
