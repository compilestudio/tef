<?php if(!empty($categories) && $cat==0):?>
<h1>
	Seleccione una categoría
</h1>
<ul class="categories">
	<?php	foreach($categories as $cat):?>
		<li>
			<a href="<?=site_url("landing/test/".$cat['row']);?>">
				<?=$cat['c_nombre'];?>
			</a>
		</li>
	<?php endforeach;?>
</ul>
<?php elseif($cat!=0 && $sub==0):?>
	<h1>
		<a class="back" href="javascript:history.back();" style="width: 25px; display: inline-block;">
			<img src="<?=base_url();?>_img/back.png" alt="" class="img-responsive"/>
		</a>
		<?=$categories[$cat-1]['c_nombre'];?>
	</h1>
	<?php if(!empty($subcategories)):?>
		<ul class="subcategories">
			<?php foreach($subcategories as $s):?>
				<li>
					<a href="<?=site_url("landing/test/".$cat."/".$s->row_id."/".$s->fields['s_nombre']);?>">
						<?=$s->fields['s_nombre'];?>
					</a>
				</li>
			<?php endforeach;?>
		</ul>
	<?php endif;?>
<?php elseif($cat!=0 && $sub!=0):?>
	<h1>
		<a class="back" href="javascript:history.back();" style="width: 25px; display: inline-block;">
			<img src="<?=base_url();?>_img/back.png" alt="" class="img-responsive"/>
		</a>
		<?=urldecode($s_name);?>
	</h1>
	<?php if(!empty($subcategories)):?>
		<ul class="files">
			<?php if(!empty($files))
			foreach($files as $f):?>
				<li>
					<a href="<?=site_url("landing/download/".$f->fields['archivo']);?>">
						<img src="<?=base_url();?>_img/file.png" alt="Descarga de archivo"/>
						<?=$f->fields['a_nombre'];?>
					</a>
				</li>
			<?php endforeach;?>
		</ul>
	<?php endif;?>
<?php endif;?>

