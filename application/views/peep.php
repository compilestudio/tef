<div class="fullscreen">
    <div class="peep-navigation">
        <span>
            MORE LEADERS
        </span>
        <?php
        $left = 0;
        $right = 0;
        $id = $peep['order'];
        $id2 = $this->uri->segment(3);
        if(isset($team2)){
            foreach($team2 as $item){
            if($id2!=$item['row']):
                if($item["order"] <$id && $left==0){
                    $left = $item['row'];
                    break;
                }
            endif;
        }
        }
        if(isset($team)){
            foreach($team as $item){
                if($id2!=$item['row']):
                    if($item["order"] >= $id && $right==0){
                        $right = $item['row'];
                        break;
                    }
                endif;
            }
        }
        ?>
        <div class="peep-controls">
            <?php if($left != 0):?>
                <a href="<?=site_url("people/detail/".$left)?>" class="peep-prev"><img src="<?=base_url("_img/peep_prev.png")?>" alt="prev"/></a>
            <?php endif;?>
            <?php if($right != 0):?>
                <a href="<?=site_url("people/detail/".$right)?>" class="peep-next"><img src="<?=base_url("_img/peep_next.png")?>" alt="next"/></a>
            <?php endif;?>
        </div>
    </div>
    <div class="peep-container">
        <h1 class="peep">
            <?=$peep['name'];?>
        </h1>
        <h5 class="peep">
            <?=$peep['title'];?>
        </h5>
        <div class="row">
            <div class="left_column">
                <div class="description">
                    <?php
                    if(!empty($peep['description'])):
                        echo $peep['description'];
                    endif;?>
                </div>
                <div class="awards">
                    <h5>
                        <?=$peep['c_title'];?>
                    </h5>
                    <div class="name">
                        <?=$peep['c_text'];?>
                    </div>
                </div>
                <div class="contact_info">
                    <h5>
                        CONTACT
                    </h5>
                    <div class="phone">
                        <?=$peep['phone'];?>
                    </div>
                    <?php
                    $atts = array(
                        'class'    => 'email'
                    );
                    echo safe_mailto($peep['email'], 'Email me', $atts);?>
                </div>
            </div>
            <div class="right_column">
                <?php if(!empty($peep['in_image'])){?>
                    <img src="<?=base_url();?>uploads/<?=$peep['in_image'];?>" alt="<?=$peep['name'];?>"/>
                    <!--<div class="bubble">
                        <?="";//$peep['speech'];?>
                    </div>-->
                <?php }?>
            </div>
        </div>
    </div>
    <div class="m_container" style="float: left; width: 100%; position: relative; bottom: -500px;">
        <div class="more_peep">
            <div class="more-btn">
                <img src="<?=site_url("_img/moreb.png")?>" alt="more">
                <span class="image-more" style="display: block;">SIGNATURE PROJECTS</span>
                <span class="image-only" style="display: none;">LEADER PROFILE</span>
            </div>
            <div class="projects">
                <div class="owl-content" >
                    <div id="owl-carousel" >
                        <?php $exist = 4;
                        for($i=1;$i<5;$i++):
                            if(empty($peep['pr'.$i]['pimg1']) && empty($peep['pn'.$i])):
                                $exist--;
                            endif;
                            ?>
                            <div class="item item-thumbnail">
                                <?php if(!empty($peep['project'.$i]) && $peep['project'.$i] > 0){?>
                                    <a href="<?=site_url("projects/detail/".$peep['project'.$i]);?>">
                                <span class="item-title">
                                    <?=$peep['pr'.$i]['p_name'];?>
                                </span>
                                        <img src="<?=base_url();?>uploads/<?=$peep['pr'.$i]['pimg1'];?>" alt="<?=$peep['pr'.$i]['p_name'];?>"/>
                                    </a>
                                <?php }else{ ?>
                                    <a href="javascript:open_modal('#Project<?=$i;?>');" >
                                <span class="item-title">
                                    <?=$peep['pn'.$i];?>
                                </span>
                                        <img src="<?=base_url();?>uploads/<?=$peep['pi'.$i];?>" alt="<?=$peep['pn'.$i];?>"/>
                                    </a>
                                <?php }?>
                            </div>
                        <?php endfor;?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Projects Modal -->
<?php for($i=1;$i<5;$i++):
    if($peep['project'.$i]==-1):
?>
        <div class="modal fade bs-example-modal-lg project_" id="Project<?=$i;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="modal-img">
                            <img src="<?=base_url();?>uploads/<?=$peep['pi'.$i];?>" alt="<?=$peep['pn'.$i];?>"/>
                            <div class="close_" data-dismiss="modal" aria-label="Close">
                                <img src="<?=base_url();?>_img/close_.png" alt="Close"/>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="s_container">
                            <div class="desc_container">
                                <div class="modal-title">
                                    <?=$peep['pn'.$i];?>
                                </div>
                                <div class="modal-text">
                                    <?=$peep['pd'.$i];?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<?php endif;
endfor;?>

<!-- Awards MOdal -->
<div class="modal fade" id="_awards" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="awards_container">
                    <div class="close_" data-dismiss="modal" aria-label="Close">
                        <img src="<?=base_url();?>_img/close_.png" alt="Close"/>
                    </div>
                    <div class="aw_cont">
                        <?php if(!empty($awards))
                            foreach($awards as $aw):?>
                                <div class="aw">
                                    <a href="<?=site_url("projects/detail/".$aw->fields['a_project']);?>" class="title">
                                        <?=$aw->fields['project_info']['p_name'];?>
                                    </a>
                                    <div class="text">
                                        <?=$aw->fields['a_name'];?>
                                    </div>
                                </div>
                            <?php endforeach;?>
                    </div>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- Press MOdal -->
<div class="modal fade" id="_press" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="awards_container">
                    <div class="close_" data-dismiss="modal" aria-label="Close">
                        <img src="<?=base_url();?>_img/close_.png" alt="Close"/>
                    </div>
                    <div class="aw_cont">
                        <?php if(!empty($press))
                            foreach($press as $aw):?>
                                <div class="aw">
                                    <a href="<?=site_url("projects/detail/".$aw->fields['p_project']);?>" class="title">
                                        <?=$aw->fields['project_info']['p_name'];?>
                                    </a>
                                    <div class="text">
                                        <?=$aw->fields['p_title'];?>
                                    </div>
                                </div>
                            <?php endforeach;?>
                    </div>
                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
    var hidden = 0;
    $(document).ready(function(){
        /*
        $('.description').slimScroll({
            position: 'left',
            railVisible: true,
            alwaysVisible: false,
            height: '527px'
        });
       /* $('.right_column').hoverIntent(
            function(){
                $('.bubble').fadeIn();
            },
            function(){
                $('.bubble').fadeOut();
            }
        );*/
        $('.desc_container').slimScroll({
            position: 'left',
            railVisible: true,
            alwaysVisible: false,
            height: '95px'
        });
        $('.proj').hoverIntent(
            function(){
                $('.name',$(this)).fadeIn();
            },
            function(){
                $('.name',$(this)).fadeOut();
            }
        );
        $('.aw_cont').slimScroll({
            position: 'left',
            railVisible: true,
            alwaysVisible: false,
            height: '230px'
        });


        $("#owl-carousel").owlCarousel({
            autoPlay : false,
            items : 4,
            itemsDesktop : [1199,4],
            itemsDesktopSmall : [979,4],
            itemsTablet: [768,4],
            itemsMobile: [480,2],
            navigation : true,
            pagination: false,
            beforeUpdate: function(){
                $('.m_container').css('position','relative');
            },
            afterUpdate: function(){
                $('.m_container').css('position','fixed');
                if(hidden==0){
                    $('.m_container').css('bottom',-$('.m_container .projects').height());
                }else{
                    $('.m_container').css('bottom',0);
                }
            }
        });
        $(".owl-prev").text("");
        $(".owl-next").text("");

        $(".more-btn").click(function() {
            //if($(".slider-content").css("margin-top").replace('px', '') < 0){
            if(hidden==1){
                related_projects("hide");
                //$(".more span").show();
            }else{
                related_projects("show");
                //$(".more span").hide();

            }
        });
        //$('.projects').hide();
    });

    $( window ).load(function () {
        var height = "auto";
        if(window.innerWidth > 768){
            height = ($(".right_column").height()/2);
        }
        $(".left_column >  .slimScrollDiv").css('height',height);
        //$(".description").css('height',height);

        //Actualizamos el bottom para cuando carga
        $('.m_container').css('bottom',-$('.m_container .projects').height()).css('position','fixed');
    });
    $( window ).resize( debouncer( function ( e ) {
        var height = "auto";
        if(window.innerWidth > 768){
            height = ($(".right_column").height()/2);
        }
        $(".left_column >  .slimScrollDiv").css('height',height);
    }));
    function debouncer( func , timeout ) {
        var timeoutID , timeout = timeout || 100;
        return function () {
            var scope = this , args = arguments;
            clearTimeout( timeoutID );
            timeoutID = setTimeout( function () {
                func.apply( scope , Array.prototype.slice.call( args ) );
            } , timeout );
        }
    }
    function open_modal(modal){
        adjustModalMaxHeightAndPosition();
        $(modal).modal("show");
    }
    function related_projects(){
        //$(".more-btn span").hide();
        if(hidden==0){
            $( ".m_container").show().animate({
                bottom: 0
            }, 500);
            /*
            $( ".more" ).animate({
                bottom: "+="+($('.projects').height()-5)
            }, 500);
            */
            var src = $(".more-btn img").attr("src");
            $(".more-btn img").attr("src", src.replace("closeb.png", "moreb.png"));
            $('.image-only').show();
            $('.image-more').hide();
            hidden=1;
        }else{
            $( ".m_container" ).animate({
                bottom: -$('.m_container .projects').height(),
            }, 500);

            var src = $(".more-btn img").attr("src");
            $(".more-btn img").attr("src", src.replace("moreb.png", "closeb.png"));
            $('.image-only').hide();
            $('.image-more').show();
            hidden=0;
        }
        /*
         var top = 0;
         if(state == "show"){
         top = "-"+$("#owl-carousel").height()+"px";
         var src = $(".more-btn img").attr("src");
         $(".more-btn img").attr("src", src.replace("more.png", "close.png"));

         }else{
         var src = $(".more-btn img").attr("src");
         $(".more-btn img").attr("src", src.replace("close.png", "more.png"));
         }
         $(".slider-content").animate({
         marginTop:top
         },
         250
         );
         */
    }
</script>
<?php if($exist==0):?>
    <style>
    .more{
        display:none !important;
    }
    </style>
<?php endif;?>