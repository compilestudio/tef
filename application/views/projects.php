<?php
if(!empty($projects)):
    if($type==1):
        usort($projects,'cmp_p');
    else:
        usort($projects,'cmp_p2');
        #var_dump($projects);
    endif;
endif;

?>
<div class="fullscreen">
    <div class="list"> 
        <?php if(isset($projects) && $category != "all"){
            foreach($projects as $f):
                ?>
                <div class="item col-xs-6 col-sm-3 col-md-3 col-lg-3">
                    <a href="<?=site_url("projects/detail/".$f["row"]);?>" title="View <?=$f['p_name'];?> detail">
                        <div class="image">
                            <img src="<?=base_url()."uploads/".$f['pimg1'];?>" alt="<?=$f['p_name'];?>"/>
                        </div>
                        <div class="name">
                            <?=$f['p_name'];?>
                        </div>
                    </a>
                </div>
            <?php endforeach;
        }
        else if(isset($all_projects)){
            foreach($all_projects as $f): ?>
            <div class="item col-xs-6 col-sm-3 col-md-3 col-lg-3">
                <a href="<?=site_url("projects/detail/".$f["row"]);?>" title="View <?=$f["p_name"];?> detail">
                    <div class="image">
                        <img src="<?=base_url()."uploads/".$f['pimg1'];?>" alt="<?=$f['p_name'];?>"/>
                    </div>
                    <div class="name">
                        <?=$f['p_name'];?>
                    </div>
                </a>
            </div>
            <?php endforeach;}?>
    </div>
</div>
<script>
    //When the images have finished loading
    $(window).load(function() {
        var h = $('.item:first').outerHeight();
        $('.item').outerHeight(h);
        load = 1;
        if(isMobile){
            $('.content .list .item a .name').show();
        }
    });
    $( window ).resize(function() {
        if(load==1) {
            $('.item:first').removeAttr("style");
            var h = $('.item:first').outerHeight();
            $('.item').outerHeight(h);
        }
    });
</script>
<?php
function cmp_p($a, $b){
    if ($a['p_order'] == $b['p_order']) {
        if ($a['p_name'] == $b['p_name']) {
            return 0;
        }
        return ($a['p_name'] < $b['p_name']) ? -1 : 1;
    }
    return ($a['p_order'] < $b['p_order']) ? -1 : 1;
}
function cmp_p2($a, $b){
    if ($a['p_order'] == $b['p_order']) {
        if ($a['p_name'] == $b['p_name']) {
            return 0;
        }
        return ($a['p_name'] < $b['p_name']) ? -1 : 1;
    }
    return ($a['p_order'] < $b['p_order']) ? -1 : 1;
}
?>