<div class="fullscreen">
    <!--
    <div class="fullscreen-header">
        <h1 class="people-title">
            PEOPLE
        </h1>
    </div>
    -->
    <?php if(!empty($peeps)):?>
        <div class="list">
            <?php foreach($peeps as $p):
                $url = "javascript:void(0)";
                $class = "";
                if($p['leader']=="0"):
                    $url = "javascript:void(0)";
                    $class = "full";
                else:
                    $url = site_url("people/detail/".$p["row"]);
                    $class = "full";
                endif;
                if(($id=="leadership" && $p['leader']=="on") || ($id===0)):
            ?>
                <div class="item col-xs-6 col-sm-3 col-md-3 col-lg-3">
                    <a href="<?=$url;?>" title="View <?=$p["name"];?> detail">
                        <div class="image">
                            <img src="<?=base_url()."uploads/".$p['image1'];?>" alt="<?=$p['name'];?>"/>
                        </div>
                        <div class="name <?=$class;?>">
                            <?=strip_tags($p['name']);?>
                            <br>
                            <?=strip_tags($p['title']);?>
                            <?php if($p['leader']=="0"):
                                echo "<br>".substr(strip_tags(strip_slashes($p['quote'])),0,100);
                            else:
                                echo "<div class='more'>more <img src='".base_url()."_img/related_next.png'></div>";
                            endif;?>
                        </div>
                    </a>
                </div>
            <?php endif;
            endforeach;?>
        </div>
    <?php endif;?>
</div>
<script>
    //When the images have finished loading
    $(window).load(function() {
        if(isMobile){
            $('.content .list .item a .name').show().css("background","rgba(0, 0, 0, 0.5) none repeat scroll 0px 0px");
        }
    });
</script>