<?php

	/*
	 * Fecha de Creación: 25-feb-2012
	 * Autor: Pedro Izaguirre
	 * Fecha Última Modificación: 25-feb-2012
	 * Modificado por: Pedro Izaguirre
	 * Descripción: Modelo con funciones del login
	 * 
	 */ 

class Landing_model extends MY_Model{
	function __construct(){
        parent::__construct();
		$this->set_table("user");
    }
#********************************************************************************************************
	public function landing_info(){
		$this->lang->load('front', $this->idioma);

		$this->load->model('menu_model','menu');
		$array_end['menu']=$this->menu->show_menu();
		
		return $array_end;	
		
	}
}

?>