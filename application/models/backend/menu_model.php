<?php

	/*
	 * Fecha de Creación: 28-mar-2012
	 * Autor: Pedro Izaguirre
	 * Fecha Última Modificación: 
	 * Modificado por: 
	 * Descripción: Modelo con funciones para el Menu
	 * 
	 */ 
class Menu_model extends MY_Model{
	function __construct(){
        parent::__construct();
		$this->set_table("home");
    }
#********************************************************************************************************
	public function show_menu(){
		$this->lang->load('backend', $this->idioma);

		$array_end['menu'] = $this->build_menu(1,"", "-1");
		$array_end['current_father'] = $this->get_father($this->uri->segment(3));
		$array_end['default_options'] = $this->get_default_options();
		
		$return = $this->load->view('backend/_includes/menu',$array_end,true);
		
		return $return;	
	}
#********************************************************************************************************
	#We build the menu
	public function build_menu($level=1,$return="",$father,$show=0){
		$user_type = $this->session->userdata('user_type');
		$this->lang->load('backend', $this->idioma);
		$condition = "";
		if($show==1):
			$condition = " and front_display=1 ";
		endif;
		#We get the information
		$sql = "select	c.id,c.title,'".$level."' level,c.father,c.pending_approval
				from	content c
				left join content c2 on c.father=c2.id
				where	c.father=".$father." 
				".$condition."
				and 	c.deleted=0
				order by c.content_order";
		$query_level = $this->db->query($sql);
		$return = "";
		
		if ($query_level->num_rows() > 0):
			$return .= "<ul class='nav nav-list menu";
			if($father==-1)
				$return .= " nav_top";
			$return .="'>";
			foreach ($query_level->result() as $row):
				$return .= "<li id='men".$row->id."'>";
					$return .= '<a href="'.site_url("backend/content/".$row->id).'"';
					if($row->id==$this->uri->segment(3)) $return .= "class='active'";
					$return .= '> '.$row->title;
					if($row->pending_approval==1 && $user_type==4):
						$return .= ' <span class="badge badge-important">*</span> ';
					endif;
					$return .='</a>';
					#we call the funciton for the childs
					$return .= $this->build_menu(($level+1),$return, $row->id);
				$return .= "</li>";
			endforeach;		

			$return .= "</ul>";
		endif;
		
		return $return;	
	}
#********************************************************************************************************
	public function get_father($id=0){
		if(!is_numeric($id))
			$id='';
		if($id!='' && $id!=0):
			#Obtenemos los top level
			$sql = "select	father
					from	content
					where	id=".$id;
			$query = $this->db->query($sql);
			$row = $query->result();
			return $row[0]->father;
		else:
			return -1;
		endif;
	}
#********************************************************************************************************
	public function get_default_options(){
		/*
		$return = "<ul class='nav nav-list menu'>";
		#We add the default options
		$return .= "<li>&nbsp;</li>";
		$return .= "<li>
					<a href='".site_url("backend/header")."'>
						".$this->lang->line('header_menu_legend')."
					</a>
				</li>";
		$return .= "<li>
					<a href='".site_url("backend/footer")."'>
						".$this->lang->line('footer_menu_legend')."
					</a>
				</li>";
		$return .= "<li>
					<a href='".site_url("backend/")."'>
						".$this->lang->line('login_menu_legend')."
					</a>
				</li>";
		$return .= "<li>
					<a href='".site_url("backend/directions")."'>
						".$this->lang->line('directions_menu_legend')."
					</a>
				</li>";
		$return .= "</ul>";
		
		return $return;*/
	}
}

?>