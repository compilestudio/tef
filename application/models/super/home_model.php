<?php

	/*
	 * Fecha de Creación: 16-mar-2012
	 * Autor: Pedro Izaguirre
	 * Fecha Última Modificación: 
	 * Modificado por: 
	 * Descripción: Modelo con funciones para el Home
	 * 
	 */ 

class Home_model extends MY_Model{
	function __construct(){
        parent::__construct();
		$this->set_table("home");
    }
#********************************************************************************************************
	public function home_info(){
		$this->lang->load('super', $this->idioma);

		$array_end['home_welcome'] = $this->lang->line('home_welcome');
		$array_end['home_explanation'] = $this->lang->line('home_explanation');
		$array_end['home_login_title'] = $this->lang->line('home_login_title');
		$array_end['home_login_explanation'] = $this->lang->line('home_login_explanation');
		
		#Info del formulario
		$array_end['log_in_title'] = $this->lang->line('log_in_title');
		$array_end['user_login'] = $this->lang->line('user_login');
		$array_end['user_pass'] = $this->lang->line('user_pass');
		$array_end['update_button'] = $this->lang->line('update_button');
		
		$this->load->model('super/menu_model','menu');
		
		$array_end['menu']=$this->menu->show_menu();		
		
		$this->set_table('user');
		$user_name = $this->get($this->session->userdata('super_user_id'));
		$array_end['user_name'] = $user_name->user;
		
			
		return $array_end;	
		
	}
#********************************************************************************************************
	#Cambiamos el usuario y password
	public function update_user($user_name,$password,$id){
		$data ['user']= $user_name;
		if($password!='')
			$data['password'] = $password;
		
		$this->set_table('super');
		$this->update($data, $id);
	}
}

?>