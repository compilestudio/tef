<?php

	/*
	 * Fecha de Creación: 16-mar-2012
	 * Autor: Pedro Izaguirre
	 * Fecha Última Modificación: 
	 * Modificado por: 
	 * Descripción: Modelo con funciones para el Home
	 * 
	 */ 

class Content_model extends MY_Model{
	function __construct(){
        parent::__construct();
		$this->set_table("content");
    }
#********************************************************************************************************
	public function load_content_language(){
		$this->lang->load('super', $this->idioma);
		
		$array_end = NULL;
		#Obtenemos los títulos para el content del language
		$array_end['content_description'] = $this->lang->line('content_description');
		return $array_end;
	}
#********************************************************************************************************
	public function content_info($id,$father=0){
		#Cargamos las lineas del archivo de lenguaje
		$array_end = $this->load_content_language();
		
		#Content ID
		$array_end['content_id']=$id;
		
		$this->load->model('super/menu_model','menu');
		$array_end['menu']=$this->menu->show_menu();		
		
		$array_end['content_info']=$this->get($id);
		
		#Obtenemos las secciones del contenido
		$sql = "SELECT 	cs.section_id,fs.name,cs.multiple,cs.image,cs.width,cs.height,cs.order,cs.description
			  FROM 	`content_section` cs,field_section fs
			  WHERE 	cs.section_id=fs.id
			  and 	cs.content_id=".$id."
			  order by  cs.order asc";
		$query = $this->db->query($sql);
		$sections = NULL;
		#Recorremos para agregarle los campos a cada sección
		if ($query->num_rows() > 0):
			$sections = $query->result();
			$array_end['sections'] = $sections;
   			foreach ($sections as &$row):
				$row->fields = $this->content_fields($id,$row->section_id);
			endforeach;
		endif;
		
		$array_end['section_data'] = $sections;

		#Obtenemos todas las secciones
		$sql = "SELECT 	distinct fs.id section_id,fs.name
			  FROM 	field_section fs
			  order by fs.name asc";
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0):
			$array_end['sections'] = $query->result();
		else: 
			$array_end['sections'] = NULL;
		endif;
		
		#obtenemos los tipos de campo
		$this->set_table("field_type");
		$array_end['field_types'] = $this->list_rows('*',NULL,'type asc');
		
		
		#Obtenemos el listado de las secciones
		$this->set_table("field_section");
		$array_end['section_list'] = $this->list_rows('*');
		
		
		$array_end['multiple_data'] = $this->get_multiple_data();
		
		return $array_end;	
	}
#******************************************************************************************************************************************************************
	#Funcion para obtener las opciones para seleccionar un multiple
	function get_multiple_data(){
		$sql = 'SELECT 	concat(cs.content_id,",",cs.section_id) id,concat(c.title,"/",fs.name) value
			  FROM 	`content_section` cs, content c, field_section fs
			  WHERE 	cs.content_id = c.id
			  AND 	cs.section_id = fs.id';
		$query = $this->db->query($sql);

		if ($query->num_rows() > 0):
			return $query->result();
		else:
			return NULL;
		endif;
	}
#********************************************************************************************************
	#Obtenemos los fields 
	public function content_fields($content_id,$section_id){
		$sql = 	"SELECT 	f.*,fd.id data_id,fd.fields_id,fd.content_live,fd.content_draft,fd.row_id  
				 FROM		fields f left join field_data fd
				 on 		f.id=fd.fields_id
				 where	f.content_id=".$content_id."
				 and		f.section_id=".$section_id."
				 group by 	f.id
				 order by 	f.order asc,f.id asc";
		$query = $this->db->query($sql);
		
		return $query->result();
	}
#********************************************************************************************************
	#we get the list of sections
	public function get_sections($id,$val=0){
		if($val==0):
			$sql = "select	id,title
					from	content
					where	father=".$id."
					order by content_order";
		else:
			$sql = "select	id,title
					from	content
					where	id=".$val;
		endif;
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0):
			#obtenemos la info
			$return = $query->result();
		else:
			$return = "";
		endif;
		return $return;
	}
#********************************************************************************************************
	#we get the information of the content with the value of a field
	public function get_info_on_field($val,$content_id){
		$sql = "select	fs.content_id id,f.select_id
				from	fields f,field_content_small fs 
				where	fs.fields_id=f.id
				and		fs.content='".$val."'
				and		f.content_id=".$content_id;
		$query = $this->db->query($sql);
		$category_name = $this->get_sections(0,$val);
		$return['category_name'] = $category_name[0]->title;
		if ($query->num_rows() > 0):
			foreach ($query->result() as $row):
				$content=$this->get($row->id);
				$data = NULL;
				$data['name'] = $content->title;
				$data['id'] = $content->id;
				$data['content']= $this->get_data($content->id);
				$return['content'][]=$data;
			endforeach;
		endif;
		return $return;
	}
#********************************************************************************************************
	#we get the information of the sections
	public function get_section_data($id){
		$sections = $this->get_sections($id);
		$data = "";
		if($sections!="")
			foreach($sections as $sec):
				$info = NULL;
				$info['id']=$sec->id;
				$info['name']=$sec->title;
				$info['content']= $this->get_data($sec->id);
				$data[]=$info;
			endforeach;
		return $data;
	}
	
#********************************************************************************************************
	#obtenemos la infor del contenido
	public function get_data($id){
		$sql = "SELECT 	f.name,fs.content content_small,fl.content content_long,f.select_id
				FROM 	fields f
				LEFT 	JOIN field_content_long fl ON fl.fields_id = f.id AND 	fl.content_id =".$id."
				LEFT 	JOIN field_content_small fs ON fs.fields_id = f.id AND 	fs.content_id =".$id."
				WHERE 	fs.fields_id IS NOT NULL
				OR 		fl.fields_id IS NOT NULL";
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0):
			$return = NULL;
			#obtenemos la info
			$result = $query->result();
			foreach($result as $res):
				$return[$res->name] = " ";
				if($res->select_id>0):
					$sql = "select	title
							from	content
							where	id=".$res->content_small;
					$query_select = $this->db->query($sql);
					if ($query_select->num_rows() > 0):
						$result_select = $query_select->result();
						$return[$res->name] = $result_select[0]->title;
					else:
						$return[$res->name] = "";
					endif;
				elseif($res->content_small!=NULL):
					$return[$res->name] = $res->content_small;
				elseif($res->content_long!=NULL):
					$return[$res->name] = $res->content_long;
				endif;
			endforeach;
		else:
			$return = "";
		endif;
		return $return;
	}
#********************************************************************************************************
	#Guardamos la información del contenido
	public function save_content($data,$id){
		if($id==-1):
			return $this->insert($data);
		else:
			$this->update($data, $id);
			return $id;
		endif;
	}
#********************************************************************************************************
	#Guardamos la información de cada field
	public function insert_field_value($field_id,$field_value,$field_type,$content_id){
		if(strpos($field_type,"text_editor_")!==FALSE || strpos($field_type,"textarea_")!==FALSE):
			#Si el field es del largo
			$this->set_table("field_content_long");
		else:
			#Si el field es del corto
			$this->set_table("field_content_small");
		endif;
		#Vemos si existe el contenido guardado para este campo
		$exists = $this->list_rows(''," fields_id=".$field_id." and content_id=".$content_id);
		$data = array(
			'fields_id' => $field_id,
			'content' => $field_value,
			'content_id' => $content_id
		);
		if($exists==0):
			#Si no existe lo insertamos
			$this->insert($data);
		else:
			#si existe lo actualizamos
			$this->update($data, $exists[0]->id);
		endif;
		
	}
#********************************************************************************************************
	#Actualizamos el orden de los fields
	function update_field_order($fields){
		$this->set_table("fields");
		for($i=1;$i<=count($fields);$i++):
			$array = array(
					'order'=>$i
				);
			$this->update($array,$fields[$i-1]);
		endfor;
	}
	
}

?>