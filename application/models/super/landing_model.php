<?php

	/*
	 * Fecha de Creación: 25-feb-2012
	 * Autor: Pedro Izaguirre
	 * Fecha Última Modificación: 25-feb-2012
	 * Modificado por: Pedro Izaguirre
	 * Descripción: Modelo con funciones del login
	 * 
	 */ 

class Landing_model extends MY_Model{
	function __construct(){
        parent::__construct();
		$this->set_table("user");
    }
#********************************************************************************************************
	public function landing_info(){
		$this->lang->load('super', $this->idioma);

		$array_end['log_in_title'] = $this->lang->line('log_in_title');
		$array_end['user_login'] = $this->lang->line('user_login');
		$array_end['user_pass'] = $this->lang->line('user_pass');
		$array_end['login_button'] = $this->lang->line('login_button');
		$array_end['login_error'] = $this->lang->line('login_error');
		
		return $array_end;	
		
	}
}

?>