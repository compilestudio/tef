<?php

	/*
	 * Fecha de Creación: 28-mar-2012
	 * Autor: Pedro Izaguirre
	 * Fecha Última Modificación: 
	 * Modificado por: 
	 * Descripción: Modelo con funciones para el Menu
	 * 
	 */ 

class Menu_model extends MY_Model{
	function __construct(){
        parent::__construct();
		$this->set_table("home");
    }
#********************************************************************************************************
	public function show_menu(){
		$this->lang->load('backend', $this->idioma);
		
		$array_end['menu'] = $this->build_menu(1,"", "-1");
		
		$array_end['current_father'] = $this->get_father($this->uri->segment(3));
		
		$array_end['add_section'] = $this->lang->line('add_section');
		
		$return = $this->load->view('super/_includes/menu',$array_end,true);
		
		return $return;	
	}
#********************************************************************************************************
	#We build the menu
	public function build_menu($level=1,$return="",$father){
		#We get the information
		$sql = "select	id,title,'".$level."' level,father
				from	content
				where	father=".$father."
				order by content_order";
		$query_level = $this->db->query($sql);
				
		$return = "";
		
		if ($query_level->num_rows() > 0):
			$return .= "<ul class='nav nav-list menu'>";
			foreach ($query_level->result() as $row):
				$return .= "<li id='men".$row->id."'>";
					$return .= '<a href="javascript:void(0);">
									<span class="text">
										'.$row->title.'						
									</span>
									<i data-id="'.$row->id.'" class="icon-edit pull-right"></i>
									<i data-id="'.$row->id.'" class="icon-plus pull-right add_new"></i>
								</a>';
				$return .= $this->build_menu(($level+1),$return, $row->id);
				$return .= "</li>";
			endforeach;
			$return .= "</ul>";
		endif;
		
		return $return;	
	}
#********************************************************************************************************
	public function get_father($id=0){
		if($id!=''):
			#Obtenemos los top level
			$sql = "select	father
					from	content
					where	id=".$id;
			$query = $this->db->query($sql);
			if ($query->num_rows() > 0):
				$row = $query->result();
				return $row[0]->father;
			else:
				return -1;
			endif;
		else:
			return -1;
		endif;
	}
}

?>