<?php

	/*
	 * Fecha de Creación: 28-mar-2012
	 * Autor: Pedro Izaguirre
	 * Fecha Última Modificación: 
	 * Modificado por: 
	 * Descripción: Modelo con funciones para el Menu
	 * 
	 */ 

class Menu_model extends MY_Model  {
	function __construct(){
        parent::__construct();
		$this->set_table("home");
    }
#********************************************************************************************************
	public function show_menu(){
		
		$return = $this->build_menu();
		
		return $return;	
	}
#********************************************************************************************************
	public function show_menu_footer(){
		
		$return = $this->build_menu_footer();
		
		return $return;	
	}	
#********************************************************************************************************
	function echoActiveClassIfRequestMatches($requestUri)
	{
	    $actual_link = "http://"."$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
	    if ($actual_link == $requestUri)
	        return 'active';
	}	

	#We build the menu
	public function build_menu(){
		#We get the information
		$sql = "select	*
			  from	content
			  where	father=-1
			  order	by content_order asc";
		$query_level = $this->db->query($sql);
		
		$return = "";
		$i = 1;
		$num_rows = $query_level->num_rows();
		if ($query_level->num_rows() > 0):
			$return .= "<ul >";
			foreach ($query_level->result() as $row):
				# si esta habilitado el menu
				if($row->front_display==1):
					#Obtenemos el submenu
					$sql = "select	*
						  from	content
						  where	father=".$row->id."
						  order by 	content_order asc";
					$query = $this->db->query($sql);
					#Si trae submenu agregamos el submenu al link
					$add_link = "";
					if ($query->num_rows() > 0):
						$res = $query->result();
						$add_link = "/".strtolower(str_replace(' ','_',$res[0]->title));
					endif;
					#Aramamos el menu
					$section =strtolower(str_replace(' ','_',$row->title));
					$return .= "<li class='padres$i item ' >";

						 $return .= "<a href='".site_url("page/".$row->id)."' ";	
						//Detectamos cual esta activo				 
						if($section == $this->uri->segment(1) || ($section == "news/resources" && $this->uri->segment(1) == "news_resources")):
							switch ($section){
								case 'firm': $return.="class='active'"; break;
								case 'work': $return.="class='active'"; break;
								case 'people': $return.="class='active'"; break;
								case 'news/resources': $return.="class='active'"; break;
								case 'contact': $return.="class='active'"; break;
							}
						endif;
						$return .= ">";
							$return .= "<span class='cam_ '>".$row->title."<span>";
						$return .= "</a>";

						if($section == "work"):

							$return .= "<ul class='sub$i categories' style='display:none'><div>";
							//Se colocan los menús fijos		
							$submenu_active = $this->echoActiveClassIfRequestMatches(site_url($section));
							switch ($section){
								//Si esta en..
								case 'work': 
									$return.= "<li><a href='".site_url($section)."' class='".$submenu_active."'> By Type</a></li>"; 
									//Si By Location esta activo
									$active = $this->echoActiveClassIfRequestMatches(site_url($section."/location"));
									$return.= "<li><a href='".site_url($section."/location")."' class='".$active."'> By Location</a></li>"; 

									break;
							}
							$return .="</div></ul>";
						endif;

						#Armamos el submenu fijo
						if ($query->num_rows() > 0):

							$return .= "<ul class='sub$i categories categories-big' style='display:none; '><div>";

							foreach ($query->result() as $row2):
								if($row2->front_display==1):
									$return .= "<li>";
										$sub_section= strtolower(str_replace(' ','_',$row2->title));
										//Como solo news tiene actualmente seccion

										if (strpos($section,'news') !== false) {

											$active = $this->echoActiveClassIfRequestMatches(site_url("news_resources/".$sub_section));

											if(strpos($this->uri->segment(2), "events") !== false && $sub_section == "events"){
												$active = "active";
											}

										    $return .= "<a href='".site_url("news_resources/".$sub_section)."'  class='".$active."'";

										}else{
											$active = $this->echoActiveClassIfRequestMatches(site_url($section."/".$sub_section));
											$return .= "<a href='".site_url($section."/".$sub_section)."' class='".$active."'";
										}


										if($sub_section==$this->uri->segment(2)):
						endif;
										$return .= ">";
										
										/* poner cursiva la palabra Foursight*/
										if($row2->title =="foursight" ):
											$four = "<span style='font-family: conqueror_sanslight_italic;'>four</span>";
											$sight = "<span style='font-family: conqueror_sansregular;'>sight</span>";
											$return .=$four.$sight;
											
										else:
											$return .= $row2->title;
										endif;	
										
										$return .= "</a>";
									$return .= "</li>";
								endif;
							endforeach;
							$return .="</div></ul>";
						endif;
					$return .= "</li>";
				endif;
				$i++;
			endforeach;
			$return .= "</ul>";
		endif;
		
		return $return;
		
		
	}

	public function build_menu_footer(){
		#We get the information
		$sql = "select	*
			  from	content
			  where	father=-1
			  order	by content_order asc";
		$query_level = $this->db->query($sql);
		
		$return = "";
		$i = 1;
		$num_rows = $query_level->num_rows();
		if ($query_level->num_rows() > 0):
			$return .= "<ul >";
			foreach ($query_level->result() as $row):
				# si esta habilitado el menu
				if($row->front_display==1):
					#Obtenemos el submenu
					$sql = "select	*
						  from	content
						  where	father=".$row->id."
						  order by 	content_order asc";
					$query = $this->db->query($sql);
					#Si trae submenu agregamos el submenu al link
					$add_link = "";
					if ($query->num_rows() > 0):
						$res = $query->result();
						$add_link = "/".strtolower(str_replace(' ','_',$res[0]->title));
					endif;
					#Aramamos el menu
					$section =strtolower(str_replace(' ','_',$row->title));
					$return .= "<li class='padres$i item ' >";

						//Solo mostrar
						switch ($section){
							case 'our_projects': $return .= "<a href='".site_url("page/".$row->id)."' ";	 break;
							case 'our_people':  $return .= "<a href='".site_url("page/".$row->id)."' ";	 break;
							case 'our_clients':  $return .= "<a href='".site_url("page/".$row->id)."' ";	 break;
							case 'our_services':  $return .= "<a href='".site_url("page/".$row->id)."' ";	 break;
							default:  $return .= "<a href='#'";	 break;
						}


						//Detectamos cual esta activo
						if($section == $this->uri->segment(1)):
							switch ($section){
								case 'our_projects': $return.="class='active'"; break;
								case 'our_people': $return.="class='active'"; break;
								case 'our_clients': $return.="class='active'"; break;
								case 'our_services': $return.="class='active'"; break;
								case 'our_news': $return.="class='active'"; break;
								case 'contact_us': $return.="class='active'"; break;
							}
						endif;
						$return .= ">";
							$return .= "<span class='cam_ '>".$row->title."<span>";
						$return .= "</a>";

						/*Query submenu dinamico*/
						//Solo mostrar
						switch ($section){
							case 'our_projects': $query2 =  $this->get_section_multiple($row->id, 2); break;
							case 'our_people': $query2 =  $this->get_section_multiple($row->id, 2); break;
							case 'our_clients': $query2 =  $this->get_section_multiple($row->id, 2); break;
							case 'our_services': $query2 =  $this->get_section_multiple($row->id, 2); break;
							default:  $query2 = null;	 break;
						}						

						if(isset($query2) && is_array($query2)) usort($query2,"order_submenu");

						#Armamos el submenu dinamico
						if (isset($query2)):
							$return .= "<ul class='sub$i categories' style='display:none'>";							
							foreach ($query2 as $row2):
									//Si esta activo el submenu
									$submenu_active = $this->echoActiveClassIfRequestMatches(site_url($section."/category/".$row2["row"]));
									
									if($section."/category" == $this->uri->segment(1)."/".$this->uri->segment(2)):
										if($row2["row"] == $this->uri->segment(3)):
											$submenu_active = "active";
										endif;
										
									endif;

									$return .= "<li>";
										$sub_section= strtolower(str_replace(' ','_',$row2["c_label"]));

										$return .= "<a href='".site_url($section."/category/".$row2["row"])."' class='".$submenu_active."'> ";

										$return .= $row2["c_label"];
											$return .= "</a>";
									$return .= "</li>";
							endforeach;


							//Menús fijos		
							$submenu_active = $this->echoActiveClassIfRequestMatches(site_url($section));					
							switch ($section){
								case 'our_projects': 
									$return.= "<li><a href='".site_url($section)."' class='".$submenu_active."'> featured</a></li>"; 
									//si awards esta activo
									$active_awards = $this->echoActiveClassIfRequestMatches(site_url($section."/awards"));
									$return.= "<li><a href='".site_url($section."/awards")."' class='".$active_awards."'> awards</a></li>"; 
									break;
								case 'our_people': $return.= "<li><a href='".site_url($section)."' class='".$submenu_active."'> all management staff</a></li>"; break;								
							}

							$return .="</ul>";
						endif;
						#Armamos el submenu fijo
						if ($query->num_rows() > 0):
							$return .= "<ul class='sub$i'>";
							foreach ($query->result() as $row2):
								if($row2->front_display==1):
									$return .= "<li>";
										$sub_section= strtolower(str_replace(' ','_',$row2->title));
										$return .= "<a href='".site_url($section."/".$sub_section)."' ";
										if($sub_section==$this->uri->segment(2)):
						endif;
										$return .= ">";
										
										/* poner cursiva la palabra Foursight*/
										if($row2->title =="foursight" ):
											$four = "<span style='font-family: conqueror_sanslight_italic;'>four</span>";
											$sight = "<span style='font-family: conqueror_sansregular;'>sight</span>";
											$return .=$four.$sight;
											
										else:
											$return .= $row2->title;
										endif;	
										
										$return .= "</a>";
									$return .= "</li>";
								endif;
							endforeach;
							$return .="</ul>";
						endif;
					$return .= "</li>";
				endif;
				$i++;
			endforeach;
			$return .= "</ul>";
		endif;
		
		return $return;
		
		
	}	
#********************************************************************************************************
	#Obtenemos la información de las secciones multiples
	function get_section_multiple($content_id,$section_id){
		$this->set_table("content");
		$this->draft = $this->session->userdata('draft');
		$field = "content_live";
		if($this->draft)
			$field = "content_draft";

		$sql = "SELECT 	dr.id value, fd.".$field." text,dr.id 
		        FROM 	data_row dr,fields f,field_data fd 
		        WHERE 	dr.content_id=f.content_id 
		        and 	dr.section_id=f.section_id 
		        and 	dr.content_id=".$content_id." 
		        and 	dr.section_id=".$section_id." 
		        and 	f.id=fd.fields_id 
		        and 	dr.id=fd.row_id 
		        and 	order_by=1
			  group 	by dr.id
		        order by fd.content_draft asc,dr.id asc";
		$query = $this->db->query($sql);
		$return = NULL;
		if ($query->num_rows() > 0):
			foreach ($query->result() as $row):
				$data = $this->get_fields_data_front($content_id,$section_id,$row->id);
				
				$result = NULL;
				foreach($data as $dat):
					if($this->draft):
						$result[$dat->name] = $dat->content_draft;
					else:
						$result[$dat->name] = $dat->content_live;
					endif;
				endforeach;
				$result["row"] = $row->id;
				$return[] =  $result;		
			endforeach;
		else:
			return NULL;
		endif;
		
		return $return;
	}	
	#Obtenemos los campos con su data par front
	public function get_fields_data_front($content_id,$section_id,$row_id=1){
		$sql = 	"SELECT 	f.*,fd.id data_id,fd.fields_id,fd.content_live,fd.content_draft,fd.row_id,f.max_chars,f.type_id
				 FROM		fields f left join field_data fd
				 on 		f.id=fd.fields_id
				 where	f.content_id=".$content_id."
				 and		f.section_id=".$section_id."
				 and		fd.row_id=".$row_id."
				 order by 	f.order asc,f.id asc";
				 
		$query = $this->db->query($sql);
		
		$return = $query->result();
		
		#html_cut
		foreach($return as &$item):
			if(($item->type_id==4 || $item->type_id==8 || $item->type_id==12) && $item->max_chars>0):
				if($this->draft)
					$item->content_draft = $this->html_cut($item->content_draft, $item->max_chars);
				else
					$item->content_draft = $this->html_cut($item->content_live, $item->max_chars);
			endif;
		endforeach;
			
		return $return;
	}

}
	 function order_submenu($a,$b){
		if($a['c_order']==$b['c_order']){
			return 0;
		}else{
			return ($a['c_order']<$b['c_order'])? -1 : 1;
		}
	}		
?>